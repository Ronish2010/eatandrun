//
//  EatAndRunNOtificationEditViewController.swift
//  EatAndRun
//
//  Created by Ronish on 3/24/17.
//  Copyright © 2017 Mawaqaa. All rights reserved.
//

import UIKit

class EatAndRunNOtificationEditViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var restuarentNotificationEditListTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    // MARK: - TABLEVIEW DELEGATES
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.restuarentNotificationEditListTable.dequeueReusableCell(withIdentifier: "RestaurentNotificationEditListCell", for: indexPath) as! RestaurentNotificationEditListCell
        cell.imageBackView.layer.cornerRadius = cell.imageBackView.frame.size.width/2
        cell.imageBackView.layer.borderColor = THEME_BLUE_COLOUR.cgColor
        cell.imageBackView.layer.borderWidth = 1
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.restuarentNotificationEditListTable.frame.size.width * 0.272445820433437
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //self.restuarentMenuCategoryListTable.reloadData()
        performSegue(withIdentifier: "OfferCategorydetails", sender: self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

class RestaurentNotificationEditListCell: UITableViewCell {
    @IBOutlet weak var imageBackView: UIView!
    @IBOutlet weak var menuNotificationItemImage: UIImageView!
    @IBOutlet weak var notificationDescriptionTxt: UITextView!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
}
