//
//  RestaurentProfileManager.swift
//  EatAndRun
//
//  Created by Ronish on 5/12/17.
//  Copyright © 2017 Mawaqaa. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
import AlamofireObjectMapper

class RestaurentProfileManager: NSObject {
    var customerID: Int?
    var userName: String?
    var password: String?
    var firstName: String?
    var lastName: String?
    var customerEmail: String?
    var customerMobile: String?
    var commissionPerOrder: Int?
    var customerTip: Int?
    var message: String?
    var success: String?
    
    var manager = Alamofire.SessionManager ()
    
    func fetchRestaurentProfileFromServer(url: String, withParameters: NSDictionary, completion: @escaping ( Bool) -> Void)
    {
        let configuration = URLSessionConfiguration.default
        self.manager = Alamofire.SessionManager(configuration: configuration)
        manager.request(url, method: .post, parameters: withParameters as? Parameters, encoding: JSONEncoding.default).responseObject (keyPath: "") { (response: DataResponse<RestaurentProfileMapper>) in
            
            let responsedata = response.result.value
            //print(responsedata?.totalAmount)
            
            if(responsedata?.customerID != nil)
            {
                self.customerID = (responsedata?.customerID)!
            }
            
            if(responsedata?.userName != nil)
            {
                self.userName = (responsedata?.userName)!
            }
            
            if(responsedata?.password != nil)
            {
                self.password = (responsedata?.password)!
            }
            
            if(responsedata?.firstName != nil)
            {
                self.firstName = (responsedata?.firstName)!
            }
            
            if(responsedata?.lastName != nil)
            {
                self.lastName = (responsedata?.lastName)!
            }
            
            if(responsedata?.customerEmail != nil)
            {
                self.customerEmail = (responsedata?.customerEmail)!
            }
            
            if(responsedata?.customerMobile != nil)
            {
                self.customerMobile = (responsedata?.customerMobile)!
            }
            
            if(responsedata?.commissionPerOrder != nil)
            {
                self.commissionPerOrder = (responsedata?.commissionPerOrder)!
            }
            
            if(responsedata?.customerTip != nil)
            {
                self.customerTip = (responsedata?.customerTip)!
            }
            
            if(responsedata?.message != nil)
            {
                self.message = (responsedata?.message)!
            }
            
            if(responsedata?.success != nil)
            {
                self.success = (responsedata?.success)!
            }
            
            completion(true)
        }
    }
}

class RestaurentProfileMapper: Mappable {
    
    var customerID: Int?
    var userName: String?
    var password: String?
    var firstName: String?
    var lastName: String?
    var customerEmail: String?
    var customerMobile: String?
    var commissionPerOrder: Int?
    var customerTip: Int?
    var message: String?
    var success: String?
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    func mapping(map: Map)
    {
        customerID <- map["Cus_Id"]
        userName <- map["Cus_Username"]
        password <- map["Cus_Password"]
        firstName <- map["Cus_FirstName"]
        lastName <- map["Cus_LastName"]
        customerEmail <- map["Cus_Email"]
        customerMobile <- map["Cus_Mobile"]
        commissionPerOrder <- map["CommissionPerOrder"]
        customerTip <- map["Cus_Tip"]
        message <- map["Message"]
        success <- map["Success"]
    }
}
