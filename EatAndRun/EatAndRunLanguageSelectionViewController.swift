//
//  EatAndRunLanguageSelectionViewController.swift
//  EatAndRun
//
//  Created by Ronish on 3/13/17.
//  Copyright © 2017 Mawaqaa. All rights reserved.
//

import UIKit

class EatAndRunLanguageSelectionViewController: UIViewController {
    @IBOutlet weak var englishButton: UIButton!
    @IBOutlet weak var arabicButton: UIButton!
    
    var globalData: RestaurentGlobalDataMapper?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ROOT_NAVIGATION_POINTER = self.navigationController!
        getGlobalValues()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func englishButtonAction(_ sender: UIButton) {
        LANGUAGE = "1"
        EatAndRun_DEFAULTS.setValue("1", forKey: "Language")
        EatAndRun_DEFAULTS.synchronize()
        //performSegue(withIdentifier: "LOGINVIEW", sender: self)
        if let _ = EatAndRun_DEFAULTS.value(forKey: "KEEP_ME_SIGNED_IN"){
            if EatAndRun_DEFAULTS.value(forKey: "KEEP_ME_SIGNED_IN") as! Bool {
                performSegue(withIdentifier: "HomeView", sender: self)
            } else {
                performSegue(withIdentifier: "LOGINVIEW", sender: self)
            }
        } else{
            performSegue(withIdentifier: "LOGINVIEW", sender: self)
        }
    }
    
    @IBAction func arabicButtonAction(_ sender: UIButton) {
        LANGUAGE = "2"
        EatAndRun_DEFAULTS.setValue("2", forKey: "Language")
        EatAndRun_DEFAULTS.synchronize()
        performSegue(withIdentifier: "LOGINVIEW", sender: self)
    }
    
    // MARK: - Web Services
    
    // MARK:  GET ORDER HISTORY DETAILS
    
    func getGlobalValues() {
        //EatAndRun_DELEGATE.showActivity(myView: self.view, myTitle: "Loading..".localized(lang: LANGUAGE!))
        if !(REACHIBILITY.statusNetworkCheck())
        {
            EatAndRun_DELEGATE.removeActivity(myView: self.view)
            EatAndRun_DELEGATE.showAlert(title: "Sorry", message: "No network available", buttonTitle: "ok")
        }
        else
        {
            let params = ["WebnaSecurityKey": "W3BN@53CUR6ITY8"]
            print(params)
            RESTAURENT_GLOBAL_DATA_MANAGER.fetchRestaurentGlobalDataFromServer(url: BASE_URL + "General/ApiGetGlobalParams", withParameters: params as NSDictionary, completion: { (result) -> Void in
                EatAndRun_DELEGATE.removeActivity(myView: self.view)
                //self.globalData = RESTAURENT_GLOBAL_DATA_MANAGER.globalValue!
                //print(self.globalData!.globalParamId!)
                //print(RESTAURENT_GLOBAL_DATA_MANAGER.globalParamId!)
                //print(RESTAURENT_GLOBAL_DATA_MANAGER.globalValue?.globalParamId!)
                
                
            })
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
