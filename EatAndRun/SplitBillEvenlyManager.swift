//
//  SplitBillEvenlyManager.swift
//  EatAndRun
//
//  Created by Sreeraj VR on 17/05/2017.
//  Copyright © 2017 Mawaqaa. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
import AlamofireObjectMapper

class SplitBillEvenlyManager: NSObject {
    
    var perHeadAmount: Int?
    var orderId: Int?
    var totalAmount: Int?
    var tip : Int?
    var manager = Alamofire.SessionManager ()
    
    func SubmitSplitBillEvenlyToServer(url: String, withParameters: NSDictionary, completion: @escaping ( Bool) -> Void)
    {
        let configuration = URLSessionConfiguration.default
        self.manager = Alamofire.SessionManager(configuration: configuration)
        manager.request(url, method: .post, parameters: withParameters as? Parameters, encoding: JSONEncoding.default).responseObject  { (response: DataResponse<SplitBillEvenlyObjectMapper>) in
            let responsedata = response.result.value
            if(responsedata?.perHeadAmount != nil)
            {
                self.perHeadAmount =  (responsedata?.perHeadAmount)!
            }
            
            if(responsedata?.orderId != nil)
            {
                self.orderId =  (responsedata?.orderId)!
            }
            if(responsedata?.totalAmount != nil)
            {
                self.totalAmount =  (responsedata?.totalAmount)!
            }
            if(responsedata?.tip != nil)
            {
                self.tip =  (responsedata?.tip)!
            }
           
            
            completion(true)
        }
    }

}


class SplitBillEvenlyObjectMapper: Mappable {
    var perHeadAmount: Int?
    var orderId: Int?
    var totalAmount: Int?
    var tip : Int?
    required init?(map: Map) {
        mapping(map: map)
    }
    
    func mapping(map: Map)
    {
        perHeadAmount <- map["PerHeadAmount"]
        orderId <- map["OrderId"]
        totalAmount <- map["TotalAmount"]
        tip <- map["Cus_Tip"]
        
    }
}






