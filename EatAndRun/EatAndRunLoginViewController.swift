//
//  EatAndRunLoginViewController.swift
//  EatAndRun
//
//  Created by Ronish on 3/13/17.
//  Copyright © 2017 Mawaqaa. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import TwitterKit
import Alamofire


class EatAndRunLoginViewController: UIViewController, UITextFieldDelegate,UIWebViewDelegate {
    
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var registrationPopUpBackView: UIView!
    @IBOutlet weak var registrationPopUpView: UIView!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var registerAsRestaurantButton: UIButton!
    @IBOutlet weak var registerAsUserButton: UIButton!
    @IBOutlet weak var userNameWhiteBackView: UIView!
    @IBOutlet weak var passwordWhiteBackView: UIView!
    @IBOutlet weak var blueBackView: UIView!
    @IBOutlet var textBackView: [UIView]!
    @IBOutlet weak var instagramWebView: UIWebView!
    @IBOutlet weak var userNameTextView: UITextField!
    @IBOutlet weak var passwordTextView: UITextField!
    @IBOutlet weak var forgetPasswordButton: UIButton!
    @IBOutlet weak var forgotPasswordPopUpBackView: UIView!
    @IBOutlet weak var forgotPasswordPopUpView: UIView!
    @IBOutlet weak var forgotPasswordCloseButton: UIButton!
    @IBOutlet weak var forgotPasswordSubmitButton: UIButton!
    @IBOutlet weak var emailWhiteBackView: UIView!
    @IBOutlet weak var emailTextView: UITextField!
    @IBOutlet weak var keepMeSignedInButton: UIButton!
    
    var customerID: Int?
    var customerEmail: String?
    var message: String?
    var success: String?
    
    var cusName = ""
    var cusFirstName = ""
    var cusLastName = ""
    var cusEmail = ""
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
        self.registrationPopUpBackView.isHidden = true
        self.forgotPasswordPopUpBackView.isHidden = true
        cusName = ""
        cusFirstName = ""
        cusLastName = ""
        cusEmail = ""
    }
    
    override func viewDidLayoutSubviews() {
        self.userNameWhiteBackView.layer.cornerRadius = self.userNameWhiteBackView.frame.size.height/2
        self.userNameWhiteBackView.clipsToBounds = true
        self.passwordWhiteBackView.layer.cornerRadius = self.passwordWhiteBackView.frame.size.height/2
        self.passwordWhiteBackView.clipsToBounds = true
        self.blueBackView.layer.cornerRadius = 3
        self.blueBackView.clipsToBounds = true
        
        self.forgotPasswordPopUpView.layer.borderWidth = 4
        self.forgotPasswordPopUpView.layer.borderColor = THEME_ORANGE_COLOUR.cgColor
        self.forgotPasswordPopUpView.layer.cornerRadius = 10
        self.forgotPasswordCloseButton.layer.cornerRadius = self.forgotPasswordCloseButton.frame.size.height/2
        self.forgotPasswordCloseButton.layer.borderColor = UIColor.white.cgColor
        self.forgotPasswordCloseButton.layer.borderWidth = 2
        self.emailWhiteBackView.layer.cornerRadius = self.emailWhiteBackView.frame.size.height/2
        self.emailWhiteBackView.clipsToBounds = true
        self.forgotPasswordSubmitButton.layer.cornerRadius = self.forgotPasswordSubmitButton.frame.size.height/2
        self.forgotPasswordSubmitButton.clipsToBounds = true
        
        self.keepMeSignedInButton.layer.borderColor = UIColor.white.cgColor
        self.keepMeSignedInButton.layer.borderWidth = 3
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.registrationPopUpView.layer.borderWidth = 4
        self.registrationPopUpView.layer.borderColor = THEME_ORANGE_COLOUR.cgColor
        self.registrationPopUpView.layer.cornerRadius = 10
        self.closeButton.layer.cornerRadius = self.closeButton.frame.size.height/2
        self.closeButton.layer.borderColor = UIColor.white.cgColor
        self.closeButton.layer.borderWidth = 2
        self.registerAsRestaurantButton.layer.cornerRadius = 4
        self.registerAsRestaurantButton.layer.borderColor =  UIColor.white.cgColor
        self.registerAsRestaurantButton.layer.borderWidth = 1
        self.registerAsUserButton.layer.cornerRadius = 4
        self.registerAsUserButton.layer.borderColor =  UIColor.white.cgColor
        self.registerAsUserButton.layer.borderWidth = 1
        
        self.keepMeSignedInButton.isSelected = false
        self.keepMeSignedInButton.backgroundColor = UIColor.clear
        
        // Do any additional setup after loading the view.
    }
    
    // MARK: - LOGIN/REGISTER Button Action
    
    @IBAction func loginButtonAction(_ sender: UIButton) {
        self.view.endEditing(true)
        if self.userNameTextView.text == "" || self.passwordTextView.text == "" {
            if self.userNameTextView.text == "" {
                EatAndRun_DELEGATE.showAlert(title: "Message".localized(lang: LANGUAGE!), message: "Enter username".localized(lang: LANGUAGE!), buttonTitle: "OK".localized(lang: LANGUAGE!))
            }else {
                EatAndRun_DELEGATE.showAlert(title: "Message".localized(lang: LANGUAGE!), message: "Enter password".localized(lang: LANGUAGE!), buttonTitle: "OK".localized(lang: LANGUAGE!))
            }
        }else {
            EatAndRun_DELEGATE.showActivity(myView: self.view, myTitle: "Loading..".localized(lang: LANGUAGE!))
            postLogin()
        }
    }
    
    @IBAction func registerButtonAction(_ sender: UIButton) {
        self.registrationPopUpBackView.isHidden = false
    }
    
    // MARK: - Keep Me Signed In Button Action
    
    @IBAction func keepMeSignedInButtonAction(_ sender: UIButton) {
        if keepMeSignedInButton.isSelected {
            self.keepMeSignedInButton.isSelected = false
            self.keepMeSignedInButton.backgroundColor = UIColor.clear
        } else {
            self.keepMeSignedInButton.isSelected = true
            self.keepMeSignedInButton.backgroundColor = THEME_ORANGE_COLOUR
        }
    }
    
    // MARK: - Fogot password Button Action
    
    @IBAction func forgetPasswordButtonAction(_ sender: UIButton) {
        self.forgotPasswordPopUpBackView.isHidden = false
    }
    
    // MARK: - Fogot password Submit Button Action
    
    @IBAction func forgetPasswordSubmitButtonAction(_ sender: UIButton) {
        self.view.endEditing(true)
        if self.emailTextView.text == "" {
            EatAndRun_DELEGATE.showAlert(title: "Message".localized(lang: LANGUAGE!), message: "Enter email".localized(lang: LANGUAGE!), buttonTitle: "OK".localized(lang: LANGUAGE!))
        }else{
            self.forgotPasswordPopUpBackView.isHidden = true
            EatAndRun_DELEGATE.showActivity(myView: self.view, myTitle: "Loading..".localized(lang: LANGUAGE!))
            postForgotPassword()
        }
    }
    
    // MARK: - PopUp Close Button Action
    
    @IBAction func popUpCloseButtonAction(_ sender: UIButton) {
        if sender.tag == 0 {
            self.registrationPopUpBackView.isHidden = true
        }else {
            self.forgotPasswordPopUpBackView.isHidden = true
        }
    }
    
    // MARK: - TEXTFIELD DELEGATE
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print(textField.tag)
        for txtViews in textBackView {
            if txtViews.tag ==  textField.tag{
                txtViews.backgroundColor = THEME_ORANGE_COLOUR
            } else {
                txtViews.backgroundColor = UIColor.white
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        for txtViews in textBackView {
            txtViews.backgroundColor = UIColor.white
        }
        return false
    }
    
    // MARK: - Facebook Button Action
    
    
    @IBAction func loginFacebookAction(_ sender: UIButton) {
        EatAndRun_DELEGATE.showActivity(myView: self.view, myTitle: "Loading..".localized(lang: LANGUAGE!))
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: ["email"], from: self) { (result, error) -> Void in
            if (error == nil){
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                if ((fbloginresult.grantedPermissions) != nil)
                {
                    if(fbloginresult.grantedPermissions.contains("email"))
                    {
                        self.getFBUserData()
                    }
                }
                else
                {
                   EatAndRun_DELEGATE.removeActivity(myView: self.view)
                }
            } else {
                EatAndRun_DELEGATE.removeActivity(myView: self.view)
            }
        }
    }
    
    func getFBUserData(){
        if((FBSDKAccessToken.current()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    //everything works print the user data
                    EatAndRun_DELEGATE.removeActivity(myView: self.view)
                    print(result!)
                    if let fbData = result as? [String:Any] {
                        self.cusName = fbData["id"] as! String
                        self.cusFirstName = fbData["first_name"] as! String
                        self.cusLastName = fbData["last_name"] as! String
                        self.cusEmail = fbData["email"] as! String
                    }
                    self.postSocialMediaLogin(socialUrl: "SocialMedia/LoginWithFacebook")
                }
            })
        }
    }
    
    // MARK: - Twitter Button Action
    
    @IBAction func twitterButtonAction(_ sender: UIButton) {
        EatAndRun_DELEGATE.showActivity(myView: self.view, myTitle: "Loading..".localized(lang: LANGUAGE!))
        Twitter.sharedInstance().logIn { session, error in
            if (session != nil) {
                EatAndRun_DELEGATE.removeActivity(myView: self.view)
                print("signed in as \(session?.userID)");
                self.cusName = (session?.userID)!
                self.cusFirstName = (session?.userName)!
                
                self.postSocialMediaLogin(socialUrl: "SocialMedia/LoginWithTwitter")
            } else {
                EatAndRun_DELEGATE.removeActivity(myView: self.view)
                print("error: \(error?.localizedDescription)");
            }
        }
    }
    
    // MARK: - Instagram Button Action
    
    @IBAction func instagramButtonAction(_ sender: UIButton) {
        EatAndRun_DELEGATE.showActivity(myView: self.view, myTitle: "Loading..".localized(lang: LANGUAGE!))
        self.instagramWebView.isHidden = false
        let authURL = String(format: "%@?client_id=%@&redirect_uri=%@&response_type=token&scope=%@&DEBUG=True", arguments: [INSTAGRAM_IDS.INSTAGRAM_AUTHURL,INSTAGRAM_IDS.INSTAGRAM_CLIENT_ID,INSTAGRAM_IDS.INSTAGRAM_REDIRECT_URI, INSTAGRAM_IDS.INSTAGRAM_SCOPE ])
        let urlRequest =  URLRequest.init(url: URL.init(string: authURL)!)
        self.instagramWebView.loadRequest(urlRequest)
    }
    
    func checkRequestForCallbackURL(request: URLRequest) -> Bool {
        
        let requestURLString = (request.url?.absoluteString)! as String
        
        if requestURLString.hasPrefix(INSTAGRAM_IDS.INSTAGRAM_REDIRECT_URI) {
            let range: Range<String.Index> = requestURLString.range(of: "#access_token=")!
            handleAuth(authToken: requestURLString.substring(from: range.upperBound))
            return false;
        }else {
            EatAndRun_DELEGATE.removeActivity(myView: self.view)
        }
        return true
    }
    
    func handleAuth(authToken: String)  {
        self.instagramWebView.isHidden = true
        EatAndRun_DELEGATE.removeActivity(myView: self.view)
        print("Instagram authentication token ==", authToken)
        self.cusName = authToken
        //self.cusFirstName = (session?.userName)!
        
        self.postSocialMediaLogin(socialUrl: "SocialMedia/LoginWithInstagram")
    }
    
    // MARK: - UIWebViewDelegate
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        
        return checkRequestForCallbackURL(request: request)
    }
    
    func nullToNil(value : AnyObject?) -> AnyObject? {
        if value is NSNull {
            return nil
        } else {
            return value
        }
    }
    
    // MARK: - Web Services
    func postLogin() {
        if !(REACHIBILITY.statusNetworkCheck())
        {
            EatAndRun_DELEGATE.removeActivity(myView: self.view)
            EatAndRun_DELEGATE.showAlert(title: "Sorry", message: "No network available", buttonTitle: "ok")
        }
        else
        {
            let params = ["Cus_Username": self.userNameTextView.text!, "Cus_Password": self.passwordTextView.text!]
            print(params)
            LOGIN_MANAGER.fetchLoginDetailsFromServer(url: BASE_URL + "General/Login", withParameters: params as NSDictionary, completion: { (result) -> Void in
                EatAndRun_DELEGATE.removeActivity(myView: self.view)
                self.customerID = LOGIN_MANAGER.customerID
                self.customerEmail = LOGIN_MANAGER.customerEmail
                self.message = LOGIN_MANAGER.message
                self.success = LOGIN_MANAGER.success
                EatAndRun_DEFAULTS.setValue(self.customerID, forKey: "CUSTOMER_ID")
                if self.success == "Success"{
                    if self.keepMeSignedInButton.isSelected {
                        EatAndRun_DEFAULTS.set(true, forKey: "KEEP_ME_SIGNED_IN")
                        EatAndRun_DEFAULTS.setValue(self.userNameTextView.text, forKey: "USERNAME")
                        EatAndRun_DEFAULTS.setValue(self.passwordTextView.text, forKey: "PASSWORD")
                    } else {
                        EatAndRun_DEFAULTS.set(false, forKey: "KEEP_ME_SIGNED_IN")
                        EatAndRun_DEFAULTS.setValue("", forKey: "USERNAME")
                        EatAndRun_DEFAULTS.setValue("", forKey: "PASSWORD")
                    }
                    self.performSegue(withIdentifier: "HomePage", sender: self)
                    EatAndRun_DELEGATE.showAlert(title: "", message: self.message!, buttonTitle: "ok")
                } else {
                    EatAndRun_DELEGATE.showAlert(title: "", message: self.message!, buttonTitle: "ok")
                }
            })
        }
        self.userNameTextView.text = ""
        self.passwordTextView.text = ""
    }
    
    func postForgotPassword() {
        if !(REACHIBILITY.statusNetworkCheck())
        {
            EatAndRun_DELEGATE.removeActivity(myView: self.view)
            EatAndRun_DELEGATE.showAlert(title: "Sorry", message: "No network available", buttonTitle: "ok")
        }
        else
        {
            let params = ["Cus_Email": self.emailTextView.text!, "WebnaSecurityKey": "W3BN@53CUR6ITY8"]
            print(params)
            FORGOT_PASSWORD_MANAGER.fetchForgotPasswordDetailsFromServer(url: BASE_URL + "General/ForgotPassword", withParameters: params as NSDictionary, completion: { (result) -> Void in
                EatAndRun_DELEGATE.removeActivity(myView: self.view)
                self.message = FORGOT_PASSWORD_MANAGER.message
                self.success = FORGOT_PASSWORD_MANAGER.success
                if self.success == "Success"{
                    //self.performSegue(withIdentifier: "HomePage", sender: self)
                    EatAndRun_DELEGATE.showAlert(title: "", message: self.message!, buttonTitle: "ok")
                } else {
                    EatAndRun_DELEGATE.showAlert(title: "", message: self.message!, buttonTitle: "ok")
                }
            })
        }
        self.emailTextView.text = ""
    }
    
    func postSocialMediaLogin(socialUrl: String) {
        if !(REACHIBILITY.statusNetworkCheck())
        {
            EatAndRun_DELEGATE.removeActivity(myView: self.view)
            EatAndRun_DELEGATE.showAlert(title: "Sorry", message: "No network available", buttonTitle: "ok")
        }
        else
        {
            let params = ["Cus_Username": self.cusName, "Cus_FirstName": self.cusFirstName, "Cus_LastName": self.cusLastName, "Cus_Email": self.cusEmail, "WebnaSecurityKey": "W3BN@53CUR6ITY8"]
            print(params)
            SOCIALMEDIA_LOGIN_MANAGER.fetchSocialMediaLoginDetailsFromServer(url: BASE_URL + socialUrl, withParameters: params as NSDictionary, completion: { (result) -> Void in
                EatAndRun_DELEGATE.removeActivity(myView: self.view)
                self.customerID = SOCIALMEDIA_LOGIN_MANAGER.customerID
                self.customerEmail = SOCIALMEDIA_LOGIN_MANAGER.customerEmail
                self.message = SOCIALMEDIA_LOGIN_MANAGER.message
                self.success = SOCIALMEDIA_LOGIN_MANAGER.success
                EatAndRun_DEFAULTS.setValue(self.customerID, forKey: "CUSTOMER_ID")
                if self.success == "Success"{
                    if self.keepMeSignedInButton.isSelected {
                        EatAndRun_DEFAULTS.set(true, forKey: "KEEP_ME_SIGNED_IN")
                    } else {
                        EatAndRun_DEFAULTS.set(false, forKey: "KEEP_ME_SIGNED_IN")
                    }
                    self.performSegue(withIdentifier: "HomePage", sender: self)
                    EatAndRun_DELEGATE.showAlert(title: "", message: self.message!, buttonTitle: "ok")
                } else {
                    EatAndRun_DELEGATE.showAlert(title: "", message: self.message!, buttonTitle: "ok")
                }
            })
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
