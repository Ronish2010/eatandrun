//
//  ForgotPasswordManager.swift
//  EatAndRun
//
//  Created by Ronish on 4/26/17.
//  Copyright © 2017 Mawaqaa. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
import AlamofireObjectMapper

class ForgotPasswordManager: NSObject {
    var message: String?
    var success: String?
    var manager = Alamofire.SessionManager ()
    
    func fetchForgotPasswordDetailsFromServer(url: String, withParameters: NSDictionary, completion: @escaping ( Bool) -> Void)
    {
        let configuration = URLSessionConfiguration.default
        self.manager = Alamofire.SessionManager(configuration: configuration)
        manager.request(url, method: .post, parameters: withParameters as? Parameters, encoding: JSONEncoding.default).responseObject (keyPath: "") { (response: DataResponse<ForgotPasswordObjectMapper>) in
            
            let responsedata = response.result.value
            print(responsedata!)
            
            if(responsedata?.message != nil)
            {
                self.message = (responsedata?.message)!
            }
            
            if(responsedata?.success != nil)
            {
                self.success = (responsedata?.success)!
            }
            
            completion(true)
        }
    }
}

class ForgotPasswordObjectMapper: Mappable {
    var message: String?
    var success: String?
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    func mapping(map: Map)
    {
        message <- map["Message"]
        success <- map["Success"]
    }
}
