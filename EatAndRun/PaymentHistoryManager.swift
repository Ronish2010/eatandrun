//
//  PaymentHistoryManager.swift
//  EatAndRun
//
//  Created by Sreeraj VR on 17/05/2017.
//  Copyright © 2017 Mawaqaa. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
import AlamofireObjectMapper

class PaymentHistoryManager: NSObject {
    
    var paymentHistoryArray: NSArray = []
    var message: String?
    var success: String?
    var manager = Alamofire.SessionManager ()
    
    func fetchpaymentHistoryFromServer(url: String, withParameters: NSDictionary, completion: @escaping ( Bool) -> Void)
    {
        let configuration = URLSessionConfiguration.default
        self.manager = Alamofire.SessionManager(configuration: configuration)
        manager.request(url, method: .post, parameters: withParameters as? Parameters, encoding: JSONEncoding.default).responseObject  { (response: DataResponse<PaymentHistoryObjectMapper>) in
            
            let responsedata = response.result.value
            print(responsedata!)
            if(responsedata?.paymentHistoryArray != nil)
            {
                self.paymentHistoryArray =  (responsedata?.paymentHistoryArray)!
            }
            
            if(responsedata?.message != nil)
            {
                self.message = (responsedata?.message)!
            }
            
            if(responsedata?.success != nil)
            {
                self.success = (responsedata?.success)!
            }
            
            completion(true)
        }
    }
    
    
}



class PaymentHistoryObjectMapper: Mappable {
    var paymentHistoryArray: NSArray = []
    var message: String?
    var success: String?
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    func mapping(map: Map)
    {
        paymentHistoryArray <- map["billpaymentmodelList"]
        message <- map["Message"]
        success <- map["Success"]
    }
}





