//
//  EatAndRunContactUsViewController.swift
//  EatAndRun
//
//  Created by Ronish on 3/28/17.
//  Copyright © 2017 Mawaqaa. All rights reserved.
//

import UIKit

class EatAndRunContactUsViewController: UIViewController {
    @IBOutlet weak var addressLine1: UILabel!
    @IBOutlet weak var addressLine2: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var webSiteLabel: UILabel!
    @IBOutlet weak var instagramLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        getProfileData()
        // Do any additional setup after loading the view.
    }
    
    // MARK: - Web Services
    
    // MARK:  GET OPEN BILL DETAILS
    
    func getProfileData() {
        EatAndRun_DELEGATE.showActivity(myView: self.view, myTitle: "Loading..".localized(lang: LANGUAGE!))
        if !(REACHIBILITY.statusNetworkCheck())
        {
            EatAndRun_DELEGATE.removeActivity(myView: self.view)
            EatAndRun_DELEGATE.showAlert(title: "Sorry", message: "No network available", buttonTitle: "ok")
        }
        else
        {
            let params = ["Cus_Id": EatAndRun_DEFAULTS .value(forKey: "CUSTOMER_ID")!, "LanguageId": LANGUAGE!,"WebnaSecurityKey": "W3BN@53CUR6ITY8"]
            print(params)
            RESTAURENT_CONTACTUS_MANAGER.fetchRestaurentContactUsFromServer(url: BASE_URL + "General/ContactUs", withParameters: params as NSDictionary, completion: { (result) -> Void in
                EatAndRun_DELEGATE.removeActivity(myView: self.view)
                self.addressLine1.text = RESTAURENT_CONTACTUS_MANAGER.address
                self.phoneNumberLabel.text = RESTAURENT_CONTACTUS_MANAGER.phoneNumber
                self.emailLabel.text = RESTAURENT_CONTACTUS_MANAGER.email
                self.webSiteLabel.text = RESTAURENT_CONTACTUS_MANAGER.website
                self.instagramLabel.text = RESTAURENT_CONTACTUS_MANAGER.instagram
            })
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
