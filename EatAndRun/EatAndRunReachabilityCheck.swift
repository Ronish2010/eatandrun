//
//  EatAndRunReachabilityCheck.swift
//  EatAndRun
//
//  Created by Ronish on 4/12/17.
//  Copyright © 2017 Mawaqaa. All rights reserved.
//

import UIKit
import Alamofire

class EatAndRunReachabilityCheck: NSObject {
    func statusNetworkCheck() -> Bool
    {
        let reachabilityManager = Alamofire.NetworkReachabilityManager(host: "www.google.com")
        
        if (reachabilityManager?.isReachable)!
        {
            return true
        }
        else
        {
            return false
        }
    }
}
