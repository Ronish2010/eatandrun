//
//  EatAndRunSplitBillViewController.swift
//  EatAndRun
//
//  Created by Sreeraj VR on 17/05/2017.
//  Copyright © 2017 Mawaqaa. All rights reserved.
//

import UIKit

class EatAndRunSplitBillViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func splitBillEvenly(_ sender: Any) {
        
        performSegue(withIdentifier: "SplitBillEvenly", sender: self)
    }
    
    
    @IBAction func chooseItemsToSplit(_ sender: Any) {
        
        performSegue(withIdentifier: "ChooseItemToSplit", sender: self)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
