//
//  GetCountryManager.swift
//  EatAndRun
//
//  Created by Ronish on 4/12/17.
//  Copyright © 2017 Mawaqaa. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
import AlamofireObjectMapper

class GetCountryManager: NSObject {
    var countryArray: [CountryDetails]?
    var manager = Alamofire.SessionManager ()
    
    func getCountryDetailsFromServer(url: String, withParameters: NSDictionary, completion: @escaping ( Bool) -> Void)
    {
        let configuration = URLSessionConfiguration.default
        self.manager = Alamofire.SessionManager(configuration: configuration)
        manager.request(url, method: .post, parameters: withParameters as? Parameters, encoding: JSONEncoding.default).responseArray (keyPath: "countryList") { (response: DataResponse<[CountryDetails]>) in
            let responsedata = response.result.value
            if(responsedata != nil)
            {
                self.countryArray = responsedata 
            }
            completion(true)
        }
    }
}

class CountryDetails: Mappable {
    var countryId: Int?
    var countryName: String?
    var countryCode: String?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        countryId <- map["CountryId"]
        countryName <- map["CountryName"]
        countryCode <- map["CountryCode"]
    }
}
