//
//  EatAndRunOfferListViewController.swift
//  EatAndRun
//
//  Created by Ronish on 3/22/17.
//  Copyright © 2017 Mawaqaa. All rights reserved.
//

import UIKit

class EatAndRunOfferListViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var searchBackView: UIView!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var searchTextView: UITextField!
    @IBOutlet weak var restuarentOfferCategoryListTable: UITableView!
    
    var restaurantID = ""
    var menuItemId = ""
    var restaurentOfferListDetails: [RestaurentOfferListDetails]?
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
        self.restuarentOfferCategoryListTable.reloadData()
    }
    
    override func viewDidLayoutSubviews() {
        self.searchBackView.layer.cornerRadius = self.searchBackView.frame.size.height/2
        self.searchBackView.clipsToBounds = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getRestaurentOfferListDetails()
        // Do any additional setup after loading the view.
    }
    
    // MARK: - TABLEVIEW DELEGATES
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if restaurentOfferListDetails != nil {
            if (restaurentOfferListDetails?.count)! > 0 {
                return restaurentOfferListDetails!.count
            }else {
                return 0
            }
        }else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.restuarentOfferCategoryListTable.dequeueReusableCell(withIdentifier: "RestaurentOfferListCell", for: indexPath) as! RestaurentOfferListCell
        cell.imageBackView.layer.cornerRadius = cell.imageBackView.frame.size.width/2
        cell.imageBackView.layer.borderColor = THEME_BLUE_COLOUR.cgColor
        cell.imageBackView.layer.borderWidth = 1
        
        cell.itemImage.sd_setImage(with: URL(string: "http://homepluskw.com/Renderers/ShowMedia.ashx?id=MediaArchive:0b0bdc54-64a1-4f69-ad30-c37767269c9f"), placeholderImage: nil)
        cell.titleLbl.text = self.restaurentOfferListDetails?[indexPath.row].menuItemName!
        cell.itemPriceLbl.text = String(describing: self.restaurentOfferListDetails![indexPath.row].discountPrice!)
        cell.oldPriceLbl.text = String(describing: self.restaurentOfferListDetails![indexPath.row].menuItemPrice!)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.restuarentOfferCategoryListTable.frame.size.width * 0.244897959183673
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //self.restuarentMenuCategoryListTable.reloadData()
        self.menuItemId = String(describing: self.restaurentOfferListDetails![indexPath.row].menuItemId!)
        performSegue(withIdentifier: "OfferCategorydetails", sender: self)
    }
    
    // MARK: - Web Services
    
    // MARK:  GET MENU LIST DETAILS
    
    func getRestaurentOfferListDetails() {
        EatAndRun_DELEGATE.showActivity(myView: self.view, myTitle: "Loading..".localized(lang: LANGUAGE!))
        if !(REACHIBILITY.statusNetworkCheck())
        {
            EatAndRun_DELEGATE.removeActivity(myView: self.view)
            EatAndRun_DELEGATE.showAlert(title: "Sorry", message: "No network available", buttonTitle: "ok")
        }
        else
        {
            let params = ["Cus_Id": EatAndRun_DEFAULTS .value(forKey: "CUSTOMER_ID")!, "LanguageId": LANGUAGE!,"WebnaSecurityKey": "W3BN@53CUR6ITY8", "Res_Id": self.restaurantID]
            print(params)
            RESTAURENT_OFFER_LIST_MANAGER.fetchRestaurentOfferListDetailsFromServer(url: BASE_URL + "Restaurant/GetRestaurantOffers", withParameters: params as NSDictionary, completion: { (result) -> Void in
                EatAndRun_DELEGATE.removeActivity(myView: self.view)
                self.restaurentOfferListDetails = RESTAURENT_OFFER_LIST_MANAGER.restaurentOfferListDetails!
                self.restuarentOfferCategoryListTable.reloadData()
                self.viewWillAppear(false)
            })
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "OfferCategorydetails" {
            let restaurentOfferItemDetailsViewController = segue.destination as! EatAndRunOfferDetailsViewController
            restaurentOfferItemDetailsViewController.menuItemId = self.menuItemId
        }
    }

}


class RestaurentOfferListCell: UITableViewCell {
    @IBOutlet weak var imageBackView: UIView!
    @IBOutlet weak var itemPriceLbl: UILabel!
    @IBOutlet weak var oldPriceLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var itemImage: UIImageView!
}
