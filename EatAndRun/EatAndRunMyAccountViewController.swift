//
//  EatAndRunMyAccountViewController.swift
//  EatAndRun
//
//  Created by Ronish on 3/28/17.
//  Copyright © 2017 Mawaqaa. All rights reserved.
//

import UIKit

class EatAndRunMyAccountViewController: UIViewController {
    @IBOutlet weak var profilePicBackGround: UIView!
    @IBOutlet weak var profilePicImageView: UIImageView!

    override func viewWillLayoutSubviews() {
        self.profilePicBackGround.layer.cornerRadius = self.profilePicBackGround.frame.size.width/2
        self.profilePicBackGround.layer.borderColor = THEME_BLUE_COLOUR.cgColor
        self.profilePicBackGround.layer.borderWidth = 4
        print(self.profilePicBackGround.frame)
    }
    
    override func viewDidLayoutSubviews() {
        //print(self.profilePicBackGround.frame)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    // MARK: - Button Actions
    
    @IBAction func paymentHistoryButtonAction(_ sender: UIButton) {
        performSegue(withIdentifier: "PaymentHistory", sender: self)
    }
    
    @IBAction func paymentMethodButtonAction(_ sender: UIButton) {
        performSegue(withIdentifier: "PaymentMethod", sender: self)
    }

    @IBAction func settingsButtonAction(_ sender: UIButton) {
        performSegue(withIdentifier: "SettingsPage", sender: self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
