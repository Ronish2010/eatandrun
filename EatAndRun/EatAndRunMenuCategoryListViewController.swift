//
//  EatAndRunMenuCategoryListViewController.swift
//  EatAndRun
//
//  Created by Ronish on 3/20/17.
//  Copyright © 2017 Mawaqaa. All rights reserved.
//

import UIKit
import SDWebImage

class EatAndRunMenuCategoryListViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var searchBackView: UIView!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var searchTextView: UITextField!
    @IBOutlet weak var restuarentMenuCategoryListTable: UITableView!
    @IBOutlet weak var headingLabel: UILabel!
    
    var restaurantID = ""
    var menuSectionID = ""
    var heading = ""
    var menuItemId = ""
    var isMenu = Bool()
    var restaurentMenuListDetails: [RestaurentMenuListDetails]?
    var restaurentCuisinesListDetails: [RestaurentCuisinesListDetails]?
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
        self.restuarentMenuCategoryListTable.reloadData()
    }
    
    override func viewDidLayoutSubviews() {
        self.searchBackView.layer.cornerRadius = self.searchBackView.frame.size.height/2
        self.searchBackView.clipsToBounds = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.headingLabel.text = self.heading
        if isMenu{
            self.getRestaurentMenuListDetails()
        } else {
            self.getRestaurentCuisineListDetails()
        }
        // Do any additional setup after loading the view.
    }

    // MARK: - TABLEVIEW DELEGATES
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isMenu{
            if restaurentMenuListDetails != nil {
                if (restaurentMenuListDetails?.count)! > 0 {
                    return restaurentMenuListDetails!.count
                }else {
                    return 0
                }
            }else {
                return 0
            }
        } else {
            if restaurentCuisinesListDetails != nil {
                if (restaurentCuisinesListDetails?.count)! > 0 {
                    return restaurentCuisinesListDetails!.count
                }else {
                    return 0
                }
            }else {
                return 0
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.restuarentMenuCategoryListTable.dequeueReusableCell(withIdentifier: "RestaurentMenuCategoryListCell", for: indexPath) as! RestaurentMenuCategoryListCell
        cell.imageBackView.layer.cornerRadius = cell.imageBackView.frame.size.width/2
        cell.imageBackView.layer.borderColor = THEME_BLUE_COLOUR.cgColor
        cell.imageBackView.layer.borderWidth = 1
        if isMenu{
            cell.menuCategoryItemImage.sd_setImage(with: URL(string: "http://homepluskw.com/Renderers/ShowMedia.ashx?id=MediaArchive:0b0bdc54-64a1-4f69-ad30-c37767269c9f"), placeholderImage: nil)
            cell.titleLbl.text = self.restaurentMenuListDetails?[indexPath.row].menuItemName!
            cell.itemPriceLbl.text = String(describing: self.restaurentMenuListDetails![indexPath.row].menuItemPrice!)
        } else {
            cell.menuCategoryItemImage.sd_setImage(with: URL(string: "http://homepluskw.com/Renderers/ShowMedia.ashx?id=MediaArchive:0b0bdc54-64a1-4f69-ad30-c37767269c9f"), placeholderImage: nil)
            cell.titleLbl.text = self.restaurentCuisinesListDetails?[indexPath.row].cuisineName!
            cell.itemPriceLbl.text = String(describing: self.restaurentCuisinesListDetails![indexPath.row].cuisineItemCount!)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.restuarentMenuCategoryListTable.frame.size.width * 0.244897959183673
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //self.restuarentMenuCategoryListTable.reloadData()
        if isMenu{
            self.menuItemId = String(describing: self.restaurentMenuListDetails![indexPath.row].menuItemId!)
        } else {
            self.menuItemId = String(describing: self.restaurentCuisinesListDetails![indexPath.row].cuisineId!)
        }
        performSegue(withIdentifier: "ItemDetails", sender: self)
    }
    
    // MARK: - Web Services
    
    // MARK:  GET MENU LIST DETAILS
    
    func getRestaurentMenuListDetails() {
        EatAndRun_DELEGATE.showActivity(myView: self.view, myTitle: "Loading..".localized(lang: LANGUAGE!))
        if !(REACHIBILITY.statusNetworkCheck())
        {
            EatAndRun_DELEGATE.removeActivity(myView: self.view)
            EatAndRun_DELEGATE.showAlert(title: "Sorry", message: "No network available", buttonTitle: "ok")
        }
        else
        {
            let params = ["Cus_Id": EatAndRun_DEFAULTS .value(forKey: "CUSTOMER_ID")!, "LanguageId": LANGUAGE!,"WebnaSecurityKey": "W3BN@53CUR6ITY8", "Res_Id": self.restaurantID, "MenuSectionId": self.menuSectionID]
            print(params)
            RESTAURENT_MENU_LIST_MANAGER.fetchRestaurentMenuListDetailsFromServer(url: BASE_URL + "Restaurant/GetRestaurantMenuItemByMenuSectionId", withParameters: params as NSDictionary, completion: { (result) -> Void in
                EatAndRun_DELEGATE.removeActivity(myView: self.view)
                self.restaurentMenuListDetails = RESTAURENT_MENU_LIST_MANAGER.restaurentMenuListDetails!
                self.restuarentMenuCategoryListTable.reloadData()
                self.viewWillAppear(false)
            })
        }
    }
    
    // MARK:  GET CUISINE LIST DETAILS
    
    func getRestaurentCuisineListDetails() {
        EatAndRun_DELEGATE.showActivity(myView: self.view, myTitle: "Loading..".localized(lang: LANGUAGE!))
        if !(REACHIBILITY.statusNetworkCheck())
        {
            EatAndRun_DELEGATE.removeActivity(myView: self.view)
            EatAndRun_DELEGATE.showAlert(title: "Sorry", message: "No network available", buttonTitle: "ok")
        }
        else
        {
            let params = ["Cus_Id": EatAndRun_DEFAULTS .value(forKey: "CUSTOMER_ID")!, "LanguageId": LANGUAGE!,"WebnaSecurityKey": "W3BN@53CUR6ITY8", "Res_Id": self.restaurantID, "MenuSectionId": self.menuSectionID]
            print(params)
            RESTAURENT_CUISINES_LIST_MANAGER.fetchRestaurentCuisinesListDetailsFromServer(url: BASE_URL + "Restaurant/GetRestaurantCuisines", withParameters: params as NSDictionary, completion: { (result) -> Void in
                EatAndRun_DELEGATE.removeActivity(myView: self.view)
                self.restaurentCuisinesListDetails = RESTAURENT_CUISINES_LIST_MANAGER.restaurentCuisinesListDetails!
                self.restuarentMenuCategoryListTable.reloadData()
                self.viewWillAppear(false)
            })
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "ItemDetails" {
            if isMenu{
                let restaurentMenuItemDetailsViewController = segue.destination as! EatAndRunMenuItemDetailsViewController
                restaurentMenuItemDetailsViewController.menuItemId = self.menuItemId
            } else {
                let restaurentMenuItemDetailsViewController = segue.destination as! EatAndRunMenuItemDetailsViewController
                restaurentMenuItemDetailsViewController.menuItemId = self.menuItemId
            }
        }
    }

}

class RestaurentMenuCategoryListCell: UITableViewCell {
    @IBOutlet weak var imageBackView: UIView!
    @IBOutlet weak var itemPriceLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var menuCategoryItemImage: UIImageView!
}
