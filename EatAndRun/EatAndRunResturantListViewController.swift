//
//  EatAndRunResturantListViewController.swift
//  EatAndRun
//
//  Created by Ronish on 3/15/17.
//  Copyright © 2017 Mawaqaa. All rights reserved.
//

import UIKit
import CoreLocation
import SDWebImage

class EatAndRunResturantListViewController: UIViewController,UITableViewDelegate,UITableViewDataSource, CLLocationManagerDelegate {

    @IBOutlet weak var restuarentListTableView: UITableView!
    @IBOutlet weak var searchBackView: UIView!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var searchTextView: UITextField!
    @IBOutlet weak var registeredResturantsCount: UILabel!
    @IBOutlet weak var registeredCustomerCount: UILabel!
    
    var countryID = ""
    var areaID = ""
    var restaurantID = ""
    var userLattitude : Double = 0.0
    var userLongitude : Double = 0.0
    var restaurentList: [RestaurentList]?
    var locationManager = CLLocationManager()
    //var locationStatus = false
    var statusCount = 1
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
        self.restuarentListTableView.reloadData()
    }
    
    override func viewDidLayoutSubviews() {
        self.searchBackView.layer.cornerRadius = self.searchBackView.frame.size.height/2
        self.searchBackView.clipsToBounds = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.restuarentListTableView.allowsSelection = true
        // Ask for Authorisation from the User.
        //self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
            print("loc1\(locationManager.location?.coordinate.latitude as Any)")
            print("loc2\(locationManager.location?.coordinate.longitude as Any)")
            //locationManager.pausesLocationUpdatesAutomatically = true
        }
        //getRestaurentList()
        // Do any additional setup after loading the view.
    }
    
    // MARK: - TABLEVIEW DELEGATES
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if restaurentList != nil {
            if (restaurentList?.count)! > 0 {
                return restaurentList!.count
            }else {
                return 0
            }
        }else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.restuarentListTableView.dequeueReusableCell(withIdentifier: "RestautantList", for: indexPath) as! RestaurentListCell
        print(cell.restuarentImage.frame.size.width)
        print(cell.restuarentImage.frame.size.height)
        print(self.restuarentListTableView.frame.size.width)
        cell.restuarentImage.layer.cornerRadius = cell.restuarentImage.frame.width/2
        cell.restuarentImage.layer.borderWidth = 1
        cell.restuarentImage.layer.borderColor = UIColor.lightGray.cgColor
        
        //cell.restuarentImage.sd_setImageWithURL(NSURL(string: "loading"))
        //cell.restuarentImage.sd_setImageWithURL(NSURL(string: "http://homepluskw.com/Renderers/ShowMedia.ashx?id=MediaArchive:0b0bdc54-64a1-4f69-ad30-c37767269c9f"), placeholderImage: UIImage(named: "loading"))
        cell.restuarentImage.sd_setImage(with: URL(string: "http://homepluskw.com/Renderers/ShowMedia.ashx?id=MediaArchive:0b0bdc54-64a1-4f69-ad30-c37767269c9f"), placeholderImage: nil)
        cell.restaurentName.text = restaurentList?[indexPath.row].restaurentName
        cell.restaurentDistance.text = restaurentList?[indexPath.row].distance
        cell.restaurentAddress.text = restaurentList?[indexPath.row].restaurentAddress
        cell.starRatingView.rating = (restaurentList?[indexPath.row].ratingStarCount)!
        cell.ratingValueLabel.text = "(\(String(describing: restaurentList![indexPath.row].ratingViewCount!)) Ratings)"
        cell.openButton.tag = indexPath.row
        cell.menuButton.tag = indexPath.row
        cell.offerButton.tag = indexPath.row
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.restuarentListTableView.frame.size.width * 0.583090379008746
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //self.restuarentListTableView.reloadData()
        self.restaurantID = String(describing: restaurentList![indexPath.row].resturantId!)
        performSegue(withIdentifier: "ResturantsDetails", sender: self)
    }
    
    // MARK: - Button Actions
    
    @IBAction func openButtonAction(_ sender: UIButton) {
        self.restaurantID = String(describing: restaurentList![sender.tag].resturantId!)
        performSegue(withIdentifier: "ResturantsDetails", sender: self)
    }
    
    @IBAction func offerButtonAction(_ sender: UIButton) {
        self.restaurantID = String(describing: restaurentList![sender.tag].resturantId!)
        performSegue(withIdentifier: "OfferDetails", sender: self)
    }

    @IBAction func menuButtonAction(_ sender: UIButton) {
        self.restaurantID = String(describing: restaurentList![sender.tag].resturantId!)
        performSegue(withIdentifier: "MenuDetails", sender: self)
    }
    
    // MARK: - Web Services
    
    // MARK:  GET RESTAURENT LIST
    
    func getRestaurentList() {
        locationManager.stopUpdatingLocation()
        EatAndRun_DELEGATE.showActivity(myView: self.view, myTitle: "Loading..".localized(lang: LANGUAGE!))
        if !(REACHIBILITY.statusNetworkCheck())
        {
            EatAndRun_DELEGATE.removeActivity(myView: self.view)
            EatAndRun_DELEGATE.showAlert(title: "Sorry", message: "No network available", buttonTitle: "ok")
        }
        else
        {
            let params = ["Cus_Id": EatAndRun_DEFAULTS .value(forKey: "CUSTOMER_ID")!, "LanguageId": LANGUAGE!,"WebnaSecurityKey": "W3BN@53CUR6ITY8", "CountryId": self.countryID, "AreaId": self.areaID, "Cus_Latitude": self.userLattitude, "Cus_Longitude": self.userLongitude]
            print(params)
            RESTAURENT_LIST_MANAGER.fetchRestaurentListFromServer(url: BASE_URL + "Restaurant/GetRestaurantList", withParameters: params as NSDictionary, completion: { (result) -> Void in
                EatAndRun_DELEGATE.removeActivity(myView: self.view)
                self.restaurentList = RESTAURENT_LIST_MANAGER.restaurentList
                print(self.restaurentList?.count as Any)
                self.restuarentListTableView.reloadData()
                self.viewWillAppear(false)
                self.getHomeDetails()
            })
        }
    }
    
    // MARK:  GET HOME DETAILS
    func getHomeDetails() {
        EatAndRun_DELEGATE.showActivity(myView: self.view, myTitle: "Loading..".localized(lang: LANGUAGE!))
        if !(REACHIBILITY.statusNetworkCheck())
        {
            EatAndRun_DELEGATE.removeActivity(myView: self.view)
            EatAndRun_DELEGATE.showAlert(title: "Sorry", message: "No network available", buttonTitle: "ok")
        }
        else
        {
            let params = ["Cus_Id": EatAndRun_DEFAULTS .value(forKey: "CUSTOMER_ID")!, "LanguageId": LANGUAGE!,"WebnaSecurityKey": "W3BN@53CUR6ITY8"]
            print(params)
            HOME_SERVICE_MANAGER.fetchHomeDetailsFromServer(url: BASE_URL + "MobileHome/MobileHome", withParameters: params as NSDictionary, completion: { (result) -> Void in
                EatAndRun_DELEGATE.removeActivity(myView: self.view)
                self.registeredCustomerCount.text = String(describing: HOME_SERVICE_MANAGER.registeredCustomerCount!)
                self.registeredResturantsCount.text = String(describing: HOME_SERVICE_MANAGER.registeredRestaurentCount!)
            })
        }
    }
    
    // MARK: - LOCATION DELEGATE
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        self.userLattitude = Double(locValue.latitude)
        self.userLongitude = Double(locValue.longitude)
        //locationManager.stopUpdatingLocation()
        if statusCount > 0 {
            //self.locationStatus = false
            statusCount = statusCount - 1
        } else if statusCount == 0{
            statusCount = -1
            getRestaurentList()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "ResturantsDetails"{
            let restaurentDetailViewController = segue.destination as! EatAndRunResturantDetailViewController
            restaurentDetailViewController.restaurantID = self.restaurantID
        }else if segue.identifier == "MenuDetails" {
            let restaurentMenuViewController = segue.destination as! EatAndRunResturantMenuViewController
            restaurentMenuViewController.restaurantID = self.restaurantID
            restaurentMenuViewController.isMenu = true
        }else if segue.identifier == "OfferDetails" {
            let restaurentOfferListViewController = segue.destination as! EatAndRunOfferListViewController
            restaurentOfferListViewController.restaurantID = self.restaurantID
        }
    }

}

class RestaurentListCell: UITableViewCell {
    @IBOutlet weak var restuarentImage: UIImageView!
    @IBOutlet weak var restaurentName: UILabel!
    @IBOutlet weak var restaurentDistance: UILabel!
    @IBOutlet weak var restaurentAddress: UITextView!
    @IBOutlet weak var openButton: UIButton!
    @IBOutlet weak var offerButton: UIButton!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var starRatingView: FloatRatingView!
    @IBOutlet weak var ratingValueLabel: UILabel!
}
