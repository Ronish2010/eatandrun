//
//  EatAndRunSplitBillEvenlyViewController.swift
//  EatAndRun
//
//  Created by Sreeraj VR on 17/05/2017.
//  Copyright © 2017 Mawaqaa. All rights reserved.
//

import UIKit

class EatAndRunSplitBillEvenlyViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var splitCountTextField: UITextField!
    
    @IBOutlet weak var countTableView: UITableView!
    @IBOutlet weak var btnShowHideTable: UIButton!
    @IBOutlet weak var viewTableView: UIView!
    
    var buttonStatus = "off"
    
    var valuesCount = ["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","40"]
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        viewTableView.isHidden = true
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func submitAction(_ sender: Any) {
        
        if (self.splitCountTextField.text! as String == "")
        {
            
            print ("Please add count")
            
        }
            
        else
        {
            performSegue(withIdentifier: "ConfirmBill", sender: self)
            
        }
        
    }
    
    //    func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    //
    //        if let ConfirmBillVC = segue.destination as? EatAndRunConfirmBillViewController {
    //            ConfirmBillVC.selectedNumber = self.splitCountTextField.text! as String
    //
    //        }
    //    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ConfirmBill"{
            
            
            let ConfirmBillVC = segue.destination as! EatAndRunConfirmBillViewController
            ConfirmBillVC.selectedNumber = self.splitCountTextField.text! as String
            
        }
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    
    override func viewDidLayoutSubviews()
    {
        // Assumption is we're supporting a small maximum number of entries
        // so will set height constraint to content size
        // Alternatively can set to another size, such as using row heights and setting frame
        // heightConstraint.constant = countTableView.contentSize.height
    }
    
    // MARK: UITableViewDataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return valuesCount.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        // Set text from the data model
        
        let detailCell : BBCountListTableViewCell  = tableView.dequeueReusableCell(withIdentifier: "BBCountListTableViewCell") as! BBCountListTableViewCell
        
        detailCell.lblDisplayCount.text = valuesCount[indexPath.row]
        return detailCell
    }
    
    // MARK: UITableViewDelegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        // Row selected, so set textField to relevant value, hide tableView
        // endEditing can trigger some other action according to requirements
        buttonStatus = "off"
        
        splitCountTextField.text = valuesCount[indexPath.row]
        self.viewTableView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.viewTableView.alpha = 0.0
        viewTableView.isHidden = true
        
        UIView.animate(withDuration: 0.25, animations:
            {
                self.viewTableView.alpha = 1.0
                self.viewTableView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        })
    }
    
    @IBAction func btnShowHideTable(_ sender: Any)
    {
        if buttonStatus == "off"
        {
            buttonStatus == "on"
            
            self.viewTableView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.viewTableView.alpha = 0.0
            viewTableView.isHidden = false
            
            UIView.animate(withDuration: 0.25, animations:
                {
                    self.viewTableView.alpha = 1.0
                    self.viewTableView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            })
        }
        else
        {
            self.viewTableView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.viewTableView.alpha = 0.0
            viewTableView.isHidden = true
            
            UIView.animate(withDuration: 0.25, animations:
                {
                    self.viewTableView.alpha = 1.0
                    self.viewTableView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            })
        }
    }
    
}


class BBCountListTableViewCell: UITableViewCell
{
    
    @IBOutlet weak var lblDisplayCount: UILabel!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
    }
    
    func loadUIData( dataObject : AnyObject)
    {
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}


/*
 // MARK: - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
 // Get the new view controller using segue.destinationViewController.
 // Pass the selected object to the new view controller.
 }
 */


