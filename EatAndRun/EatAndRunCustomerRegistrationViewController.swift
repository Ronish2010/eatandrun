//
//  EatAndRunCustomerRegistrationViewController.swift
//  EatAndRun
//
//  Created by Ronish on 3/13/17.
//  Copyright © 2017 Mawaqaa. All rights reserved.
//

import UIKit

class EatAndRunCustomerRegistrationViewController: UIViewController, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet var textBackView: [UIView]!
    @IBOutlet var registrationTextViews: [UITextField]!
    @IBOutlet weak var addPaymentMethodDetailsButton: UIButton!
    @IBOutlet weak var termsAndConditionAcceptButton: UIButton!
    @IBOutlet weak var termsAndConditionButton: UIButton!
    @IBOutlet weak var patentInformationButton: UIButton!
    @IBOutlet weak var countryButton: UIButton!
    @IBOutlet weak var selectCountryDropDownView: UIView!
    @IBOutlet weak var selectCountryTableView: UITableView!
    @IBOutlet weak var countryTableHeight: NSLayoutConstraint!
    @IBOutlet weak var registerButton: UIButton!
    
    var activeTextField : UITextField = UITextField()
    var attrs = [
        NSFontAttributeName : UIFont.systemFont(ofSize: 15.0),
        NSForegroundColorAttributeName : UIColor.darkGray,
        NSUnderlineStyleAttributeName : 1] as [String : Any]
    var attributedString = NSMutableAttributedString(string:"")
    var dropDownCountryArray: NSArray!
    var is_CountryDropDownShow = false
    var countryArray: [CountryDetails]?
    let placeHolderArray: [String] = ["Create Name".localized(lang: LANGUAGE!), "Create Password".localized(lang:LANGUAGE!), "First Name".localized(lang:LANGUAGE!), "Last Name".localized(lang:LANGUAGE!), "Email".localized(lang:LANGUAGE!), "Country".localized(lang:LANGUAGE!), "Mobile Number".localized(lang:LANGUAGE!), "190 Fills per order".localized(lang:LANGUAGE!)]
    var registrationArray = [String](repeating: "", count: 8)
    
    var customerID: Int?
    var message: String?
    var success: String?
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
        self.termsAndConditionAcceptButton.isSelected = false
        self.termsAndConditionAcceptButton.backgroundColor = UIColor.white
        getCountry()
    }
    override func viewDidLayoutSubviews() {
        var radius : CGFloat = 0
        for textbackView in self.textBackView {
            textbackView.layer.cornerRadius = textbackView.frame.size.height/2
            textbackView.layer.borderWidth = 1
            textbackView.layer.borderColor = UIColor.lightGray.cgColor
            radius = textbackView.frame.size.height/2
        }
        self.addPaymentMethodDetailsButton.layer.cornerRadius = self.addPaymentMethodDetailsButton.frame.size.height/2
        self.termsAndConditionAcceptButton.layer.borderColor = UIColor.black.cgColor
        self.termsAndConditionAcceptButton.layer.borderWidth = 4
        self.selectCountryDropDownView.layer.borderWidth = 1
        self.selectCountryDropDownView.layer.cornerRadius = radius
        self.selectCountryDropDownView.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let termsAndConditionButtonTitleStr = NSMutableAttributedString(string:"Terms and Conditions", attributes:attrs)
        attributedString.append(termsAndConditionButtonTitleStr)
        self.termsAndConditionButton.setAttributedTitle(attributedString, for: .normal)
        attributedString = NSMutableAttributedString(string:"")
        let patentInformationButtonTitleStr = NSMutableAttributedString(string:"Patent Information", attributes:attrs)
        attributedString.append(patentInformationButtonTitleStr)
        self.patentInformationButton.setAttributedTitle(attributedString, for: .normal)
        // Do any additional setup after loading the view.

    }
    
    // MARK: - TEXTFIELD DELEGATE
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print(textField.tag)
        activeTextField = textField
        showDropDownTable(buttonTag: 0)
        for txtViews in textBackView {
            if txtViews.tag ==  textField.tag{
                txtViews.backgroundColor = THEME_ORANGE_COLOUR
            } else {
                txtViews.backgroundColor = UIColor.white
            }
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        //activeTextField = nil
        registrationArray[textField.tag] = EatAndRun_DELEGATE.TrimString(trimString: textField.text!)
        print(registrationArray)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        for txtViews in textBackView {
            txtViews.backgroundColor = UIColor.white
        }
        return false
    }
    
    // MARK: - BACK BUTTON ACTION
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    // MARK: - COUNTRY BUTTON ACTION
    
    @IBAction func countryButtonAction(_ sender: UIButton) {
        showDropDownTable(buttonTag: 1)
        for txtViews in textBackView {
            if txtViews.tag ==  5{
                txtViews.backgroundColor = THEME_ORANGE_COLOUR
            } else {
                txtViews.backgroundColor = UIColor.white
            }
        }
    }
    
    // MARK: - TERMS AND CONDITION ACCEPT BUTTON ACTION
    
    @IBAction func termsAndConditionAcceptBtnActn(_ sender: UIButton) {
        if self.termsAndConditionAcceptButton.isSelected{
            self.termsAndConditionAcceptButton.isSelected = false
            self.termsAndConditionAcceptButton.backgroundColor = UIColor.white
        } else {
            self.termsAndConditionAcceptButton.isSelected = true
            self.termsAndConditionAcceptButton.backgroundColor = THEME_ORANGE_COLOUR
        }
    }
    
    // MARK: - REGESTER BUTTON ACTION
    
    @IBAction func registerButtonAction(_ sender: UIButton) {
        self.view.endEditing(true)
        
        var message: String!
        
        for i in 0..<registrationArray.count {
            if EatAndRun_DELEGATE.TrimString(trimString: registrationArray[i]) == "" {
                message = "\(placeHolderArray[i].capitalized) \("cannot be empty".localized(lang: LANGUAGE!))"
                break
            }
        }
        
        if message == nil {
            if EatAndRun_DELEGATE.isValidEmail(testStr: registrationArray[4]) == false {
                message = "Please enter a valid E-mail".localized(lang: LANGUAGE!)
            }else if EatAndRun_DELEGATE.TrimNumbers(trimString: registrationArray[6]) == false {
                message = "Phone Number cannot enter characters".localized(lang: LANGUAGE!)
            }else if registrationArray[6].characters.count < 8 {
                message = "Please enter a valid Phone Number".localized(lang: LANGUAGE!)
            }else if !self.termsAndConditionAcceptButton.isSelected {
                message = "Please accept the Terms and Condition".localized(lang: LANGUAGE!)
            }
        }
        
        if message == nil {
            postRegistration()
        }else {
            EatAndRun_DELEGATE.showAlert(title: "Message".localized(lang: LANGUAGE!), message: message, buttonTitle: "OK".localized(lang: LANGUAGE!))
        }
    }
    
    func showDropDownTable(buttonTag : Int) {
        
        if buttonTag == 0{
            UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseInOut, animations: { () -> Void in
                self.selectCountryDropDownView.frame.size.height = 0
            }) { (completed: Bool) -> Void in
                self.is_CountryDropDownShow = false
            }
        } else if buttonTag == 1 {
            if self.countryArray != nil {
                if (self.countryArray?.count)! > 0 {
                    UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseInOut, animations: { () -> Void in
                        if self.is_CountryDropDownShow == false {
                            if (self.countryArray?.count)! < 4 {
                                self.selectCountryDropDownView.frame.size.height = CGFloat((self.countryArray?.count)! * 30)
                                self.countryTableHeight.constant = self.selectCountryDropDownView.frame.size.height
                            }else {
                                self.selectCountryDropDownView.frame.size.height = CGFloat(4 * 30)
                                self.countryTableHeight.constant = self.selectCountryDropDownView.frame.size.height
                            }
                        }else {
                            self.selectCountryDropDownView.frame.size.height = 0
                        }
                    }) { (completed: Bool) -> Void in
                        self.view.setNeedsLayout()
                        self.view.layoutIfNeeded()
                        if self.is_CountryDropDownShow == false {
                            self.is_CountryDropDownShow = true
                        }else {
                            self.is_CountryDropDownShow = false
                        }
                    }
                }
            }
        }
    }
    
    // MARK: - Tableview Delegate Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if countryArray != nil {
            if (countryArray?.count)! > 0 {
                return countryArray!.count
            }else {
                return 0
            }
        }else {
            return 0
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = selectCountryTableView.dequeueReusableCell(withIdentifier: "DROPDOWN", for: indexPath as IndexPath) as! DropDownCell
        cell.valueLbl.text = self.countryArray?[indexPath.row].countryName
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        showDropDownTable(buttonTag: 0)
        print(self.registrationTextViews)
        self.registrationTextViews[5].text = self.countryArray?[indexPath.row].countryName
        let countryId = self.countryArray?[indexPath.row].countryId
        registrationArray[5] = String(describing: countryId!)
    }
    
    // MARK: - Web Services
    
    // MARK:  GET COUNTRY
    func getCountry() {
        EatAndRun_DELEGATE.showActivity(myView: self.view, myTitle: "Loading..".localized(lang: LANGUAGE!))
        if !(REACHIBILITY.statusNetworkCheck())
        {
            EatAndRun_DELEGATE.removeActivity(myView: self.view)
            EatAndRun_DELEGATE.showAlert(title: "Sorry", message: "No network available", buttonTitle: "ok")
        }
        else
        {
            let params = ["LanguageId": LANGUAGE!,"WebnaSecurityKey": "W3BN@53CUR6ITY8"]
            GET_COUNTRY_MANAGER.getCountryDetailsFromServer(url: BASE_URL + "General/GetAllCountry", withParameters: params as NSDictionary, completion: { (result) -> Void in
                EatAndRun_DELEGATE.removeActivity(myView: self.view)
                self.countryArray = GET_COUNTRY_MANAGER.countryArray 
                print(self.countryArray!)
                self.selectCountryTableView.reloadData()
            })
        }
    }
    
    // MARK:  POST REGISTRATION
    
    func postRegistration() {
        EatAndRun_DELEGATE.showActivity(myView: self.view, myTitle: "Loading..".localized(lang: LANGUAGE!))
        if !(REACHIBILITY.statusNetworkCheck())
        {
            EatAndRun_DELEGATE.removeActivity(myView: self.view)
            EatAndRun_DELEGATE.showAlert(title: "Sorry", message: "No network available", buttonTitle: "ok")
        }
        else
        {
            let params = ["Cus_Username": self.registrationArray[0], "Cus_Password": self.registrationArray[1], "Cus_FirstName": self.registrationArray[2], "Cus_LastName": self.registrationArray[3], "Cus_Email": self.registrationArray[4], "CountryId": self.registrationArray[5], "Cus_Mobile": self.registrationArray[6], "CommissionPerOrder": self.registrationArray[7],"WebnaSecurityKey":"W3BN@53CUR6ITY8"]
            print(params)
            
            CUSTOMER_REGISTRATION_MANAGER.fetchCustomerRegistrationDetailsFromServer(url: BASE_URL + "General/AddCustomer", withParameters: params as NSDictionary, completion: { (result) -> Void in
                EatAndRun_DELEGATE.removeActivity(myView: self.view)
                self.customerID = CUSTOMER_REGISTRATION_MANAGER.customerID
                self.message = CUSTOMER_REGISTRATION_MANAGER.message
                self.success = CUSTOMER_REGISTRATION_MANAGER.success
                if self.success == "Success"{
            
                    let alert = UIAlertController(title: "", message: self.message!, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler:
                    { (action:UIAlertAction!) in
                        self.performSegue(withIdentifier: "HomePage", sender: self)
                     alert.dismiss(animated: true, completion: nil)
                    }))
                    
                    self.present(alert, animated: true, completion: nil)
                    
                } else {
                    EatAndRun_DELEGATE.showAlert(title: "", message: self.message!, buttonTitle: "ok")
                }
            })

        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
