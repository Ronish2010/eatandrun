//
//  RestaurentGlobalDataManager.swift
//  EatAndRun
//
//  Created by Ronish on 5/17/17.
//  Copyright © 2017 Mawaqaa. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
import AlamofireObjectMapper

class RestaurentGlobalDataManager: NSObject {
    var globalParamId: Int?
    var thresholdAmount: Float?
    var userRegistrationFee: Float?
    var restaurantRegistrationFee: Float?
    var userCommissionFee: Float?
    var restaurantCommissionFee: Float?
    var cardCommissionFee: Float?
    var orderClosingInterval: String?
    var sortOrder: Int?
    var isActive: Bool?
    var lastModifiedDate: String?
    var errorMessage: String?
    var kNET_CardCommissionFee: Float?
    var visa_CardCommissionFee: Float?
    var master_CardCommissionFee: Float?
    var dinersClub_CardCommissionFee: Float?
    var restaurantNameVisibility: Bool?
    var restaurantPictureVisibility: Bool?
    var restaurantAddressVisibility: Bool?
    var restaurantDistanceVisibility: Bool?
    var restaurantDescriptionVisibility: Bool?
    var restaurantRatingStarVisibility: Bool?
    var restaurantRatingCountVisibility: Bool?
    var registeredRestaurantsVisibility: Bool?
    var registeredCustomersVisibility: Bool?
    var technicalIssues: Bool?
    var message: String?
    var success: String?
    
    var globalValue: RestaurentGlobalDataMapper?
    
    var manager = Alamofire.SessionManager ()
    
    func fetchRestaurentGlobalDataFromServer(url: String, withParameters: NSDictionary, completion: @escaping ( Bool) -> Void)
    {
        let configuration = URLSessionConfiguration.default
        self.manager = Alamofire.SessionManager(configuration: configuration)
        manager.request(url, method: .post, parameters: withParameters as? Parameters, encoding: JSONEncoding.default).responseObject (keyPath: "") { (response: DataResponse<RestaurentGlobalDataMapper>) in
            
            let responsedata = response.result.value
            //print(responsedata?.globalParamId!)
            
            self.globalValue = responsedata
            
            if(responsedata?.globalParamId != nil)
            {
                self.globalParamId = (responsedata?.globalParamId)!
            }
            
            if(responsedata?.thresholdAmount != nil)
            {
                self.thresholdAmount = (responsedata?.thresholdAmount)!
            }
            
            if(responsedata?.userRegistrationFee != nil)
            {
                self.userRegistrationFee = (responsedata?.userRegistrationFee)!
            }
            
            if(responsedata?.restaurantRegistrationFee != nil)
            {
                self.restaurantRegistrationFee = (responsedata?.restaurantRegistrationFee)!
            }
            
            if(responsedata?.userCommissionFee != nil)
            {
                self.userCommissionFee = (responsedata?.userCommissionFee)!
            }
            
            if(responsedata?.restaurantCommissionFee != nil)
            {
                self.restaurantCommissionFee = (responsedata?.restaurantCommissionFee)!
            }
            
            if(responsedata?.cardCommissionFee != nil)
            {
                self.cardCommissionFee = (responsedata?.cardCommissionFee)!
            } else {
                self.cardCommissionFee = 0.0
            }
            
            if(responsedata?.orderClosingInterval != nil)
            {
                self.orderClosingInterval = (responsedata?.orderClosingInterval)!
            }
            
            if(responsedata?.sortOrder != nil)
            {
                self.sortOrder = (responsedata?.sortOrder)!
            }
            
            if(responsedata?.isActive != nil)
            {
                self.isActive = (responsedata?.isActive)!
            }
            
            if(responsedata?.lastModifiedDate != nil)
            {
                self.lastModifiedDate = (responsedata?.lastModifiedDate)!
            }
            
            if(responsedata?.errorMessage != nil)
            {
                self.errorMessage = (responsedata?.errorMessage)!
            }
            
            if(responsedata?.kNET_CardCommissionFee != nil)
            {
                self.kNET_CardCommissionFee = (responsedata?.kNET_CardCommissionFee)!
            }
            
            if(responsedata?.visa_CardCommissionFee != nil)
            {
                self.visa_CardCommissionFee = (responsedata?.visa_CardCommissionFee)!
            }
            
            if(responsedata?.master_CardCommissionFee != nil)
            {
                self.master_CardCommissionFee = (responsedata?.master_CardCommissionFee)!
            }
            
            if(responsedata?.dinersClub_CardCommissionFee != nil)
            {
                self.dinersClub_CardCommissionFee = (responsedata?.dinersClub_CardCommissionFee)!
            }
            
            if(responsedata?.restaurantNameVisibility != nil)
            {
                self.restaurantNameVisibility = (responsedata?.restaurantNameVisibility)!
            }
            
            if(responsedata?.restaurantPictureVisibility != nil)
            {
                self.restaurantPictureVisibility = (responsedata?.restaurantPictureVisibility)!
            }
            
            if(responsedata?.restaurantAddressVisibility != nil)
            {
                self.restaurantAddressVisibility = (responsedata?.restaurantAddressVisibility)!
            }
            
            if(responsedata?.restaurantDistanceVisibility != nil)
            {
                self.restaurantDistanceVisibility = (responsedata?.restaurantDistanceVisibility)!
            }
            
            if(responsedata?.restaurantDescriptionVisibility != nil)
            {
                self.restaurantDescriptionVisibility = (responsedata?.restaurantDescriptionVisibility)!
            }
            
            if(responsedata?.restaurantRatingStarVisibility != nil)
            {
                self.restaurantRatingStarVisibility = (responsedata?.restaurantRatingStarVisibility)!
            }
            
            if(responsedata?.restaurantRatingCountVisibility != nil)
            {
                self.restaurantRatingCountVisibility = (responsedata?.restaurantRatingCountVisibility)!
            }
            
            if(responsedata?.registeredRestaurantsVisibility != nil)
            {
                self.registeredRestaurantsVisibility = (responsedata?.registeredRestaurantsVisibility)!
            }
            
            if(responsedata?.registeredCustomersVisibility != nil)
            {
                self.registeredCustomersVisibility = (responsedata?.registeredCustomersVisibility)!
            }
            
            if(responsedata?.technicalIssues != nil)
            {
                self.technicalIssues = (responsedata?.technicalIssues)!
            }
            
            if(responsedata?.message != nil)
            {
                self.message = (responsedata?.message)!
            }
            
            if(responsedata?.success != nil)
            {
                self.success = (responsedata?.success)!
            }
            
            completion(true)
        }
    }
}

class RestaurentGlobalDataMapper: Mappable {
    
    var globalParamId: Int?
    var thresholdAmount: Float?
    var userRegistrationFee: Float?
    var restaurantRegistrationFee: Float?
    var userCommissionFee: Float?
    var restaurantCommissionFee: Float?
    var cardCommissionFee: Float?
    var orderClosingInterval: String?
    var sortOrder: Int?
    var isActive: Bool?
    var lastModifiedDate: String?
    var errorMessage: String?
    var kNET_CardCommissionFee: Float?
    var visa_CardCommissionFee: Float?
    var master_CardCommissionFee: Float?
    var dinersClub_CardCommissionFee: Float?
    var restaurantNameVisibility: Bool?
    var restaurantPictureVisibility: Bool?
    var restaurantAddressVisibility: Bool?
    var restaurantDistanceVisibility: Bool?
    var restaurantDescriptionVisibility: Bool?
    var restaurantRatingStarVisibility: Bool?
    var restaurantRatingCountVisibility: Bool?
    var registeredRestaurantsVisibility: Bool?
    var registeredCustomersVisibility: Bool?
    var technicalIssues: Bool?
    var message: String?
    var success: String?
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    func mapping(map: Map)
    {
        globalParamId <- map["GlobalParamId"]
        thresholdAmount <- map["ThresholdAmount"]
        userRegistrationFee <- map["UserRegistrationFee"]
        restaurantRegistrationFee <- map["RestaurantRegistrationFee"]
        userCommissionFee <- map["UserCommissionFee"]
        restaurantCommissionFee <- map["RestaurantCommissionFee"]
        cardCommissionFee <- map["CardCommissionFee"]
        orderClosingInterval <- map["OrderClosingInterval"]
        sortOrder <- map["SortOrder"]
        isActive <- map["IsActive"]
        lastModifiedDate <- map["LastModifiedDate"]
        errorMessage <- map["ErrorMessage"]
        kNET_CardCommissionFee <- map["KNET_CardCommissionFee"]
        visa_CardCommissionFee <- map["Visa_CardCommissionFee"]
        master_CardCommissionFee <- map["Master_CardCommissionFee"]
        dinersClub_CardCommissionFee <- map["DinersClub_CardCommissionFee"]
        restaurantNameVisibility <- map["RestaurantNameVisibility"]
        restaurantPictureVisibility <- map["RestaurantPictureVisibility"]
        restaurantAddressVisibility <- map["RestaurantAddressVisibility"]
        restaurantDistanceVisibility <- map["RestaurantDistanceVisibility"]
        restaurantDescriptionVisibility <- map["RestaurantDescriptionVisibility"]
        restaurantRatingStarVisibility <- map["RestaurantRatingStarVisibility"]
        restaurantRatingCountVisibility <- map["RestaurantRatingCountVisibility"]
        registeredRestaurantsVisibility <- map["RegisteredRestaurantsVisibility"]
        registeredCustomersVisibility <- map["RegisteredCustomersVisibility"]
        technicalIssues <- map["TechnicalIssues"]
        message <- map["Message"]
        success <- map["Success"]
    }
}
