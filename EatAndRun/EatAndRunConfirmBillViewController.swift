//
//  EatAndRunConfirmBillViewController.swift
//  EatAndRun
//
//  Created by Sreeraj VR on 18/05/2017.
//  Copyright © 2017 Mawaqaa. All rights reserved.
//

import UIKit

class EatAndRunConfirmBillViewController: UIViewController {
    
    var selectedNumber : String?
    
    @IBOutlet weak var billAmount: UILabel!
    
    @IBOutlet weak var tipAmount: UILabel!
    
    @IBOutlet weak var totalAmount: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {

                    let params = ["Cus_Id": "23","OrderId":"10080","SplitBillType":"1","OrderType":"1","Count":selectedNumber! as String,"WebnaSecurityKey": "W3BN@53CUR6ITY8"]
                    print(params)
                    SPLITBILLEVENLY_MANAGER.SubmitSplitBillEvenlyToServer(url: BASE_URL + "SplitBillEvenly/PayBillEvenly", withParameters: params as NSDictionary, completion: { (result) -> Void in
                        self.billAmount.text =  String(SPLITBILLEVENLY_MANAGER.perHeadAmount!)
                        self.totalAmount.text =  String(SPLITBILLEVENLY_MANAGER.totalAmount!)
                        self.tipAmount.text =  String(SPLITBILLEVENLY_MANAGER.tip!)
                        
                    })
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
