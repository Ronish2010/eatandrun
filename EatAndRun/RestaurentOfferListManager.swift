//
//  RestaurentOfferListManager.swift
//  EatAndRun
//
//  Created by Ronish on 5/5/17.
//  Copyright © 2017 Mawaqaa. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
import AlamofireObjectMapper

class RestaurentOfferListManager: NSObject {
    var restaurentOfferListDetails: [RestaurentOfferListDetails]?
    var message: String?
    var success: String?
    
    var manager = Alamofire.SessionManager ()
    
    func fetchRestaurentOfferListDetailsFromServer(url: String, withParameters: NSDictionary, completion: @escaping ( Bool) -> Void)
    {
        let configuration = URLSessionConfiguration.default
        self.manager = Alamofire.SessionManager(configuration: configuration)
        manager.request(url, method: .post, parameters: withParameters as? Parameters, encoding: JSONEncoding.default).responseObject (keyPath: "") { (response: DataResponse<RestaurentOfferListMapper>) in
            
            let responsedata = response.result.value
            //print(responsedata?.restaurentMenuList!)
            
            if(responsedata?.restaurentOfferListDetails != nil)
            {
                self.restaurentOfferListDetails = (responsedata?.restaurentOfferListDetails)!
                for list in self.restaurentOfferListDetails!
                {
                    if list.menuItemId == nil{
                        list.menuItemId = 0
                    }
                    if list.menuItemName == nil{
                        list.menuItemName = ""
                    }
                    if list.menuItemImage == nil{
                        list.menuItemImage = ""
                    }
                    if list.menuItemPrice == nil{
                        list.menuItemPrice = 0
                    }
                    if list.discountPrice == nil{
                        list.discountPrice = 0
                    }
                }
            }else {
                self.restaurentOfferListDetails = []
            }
            
            if(responsedata?.message != nil)
            {
                self.message = (responsedata?.message)!
            }
            
            if(responsedata?.success != nil)
            {
                self.success = (responsedata?.success)!
            }
            
            completion(true)
        }
    }
}

class RestaurentOfferListMapper: Mappable {
    var restaurentOfferListDetails: [RestaurentOfferListDetails]?
    var message: String?
    var success: String?
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    func mapping(map: Map)
    {
        restaurentOfferListDetails <- map["menuItemList"]
        message <- map["Message"]
        success <- map["Success"]
    }
}

class RestaurentOfferListDetails: Mappable {
    var menuItemId: Int?
    var menuItemName: String?
    var menuItemImage: String?
    var menuItemPrice: Float?
    var discountPrice: Float?
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        menuItemId <- map["MenuItemId"]
        menuItemName <- map["MenuItemName"]
        menuItemImage <- map["MenuItemImage"]
        menuItemPrice <- map["Price"]
        discountPrice <- map["Discount"]
    }
}
