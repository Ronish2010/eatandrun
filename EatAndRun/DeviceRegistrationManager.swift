//
//  DeviceRegistrationManager.swift
//  EatAndRun
//
//  Created by Ronish on 5/15/17.
//  Copyright © 2017 Mawaqaa. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
import AlamofireObjectMapper

class DeviceRegistrationManager: NSObject {
    var customerID: Int?
    var message: String?
    var success: String?
    var manager = Alamofire.SessionManager ()
    
    func fetchDeviceRegistrationDetailsFromServer(url: String, withParameters: NSDictionary, completion: @escaping ( Bool) -> Void)
    {
        let configuration = URLSessionConfiguration.default
        self.manager = Alamofire.SessionManager(configuration: configuration)
        manager.request(url, method: .post, parameters: withParameters as? Parameters, encoding: JSONEncoding.default).responseObject (keyPath: "") { (response: DataResponse<DeviceRegistrationObjectMapper>) in
            
            let responsedata = response.result.value
            print(responsedata!)
            
            if(responsedata?.customerID != nil)
            {
                self.customerID = (responsedata?.customerID)!
            }
            
            if(responsedata?.message != nil)
            {
                self.message = (responsedata?.message)!
            }
            
            if(responsedata?.success != nil)
            {
                self.success = (responsedata?.success)!
            }
            
            completion(true)
        }
    }
}

class DeviceRegistrationObjectMapper: Mappable {
    var message: String?
    var success: String?
    var customerID: Int?
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    func mapping(map: Map)
    {
        customerID <- map["Cus_Id"]
        message <- map["Message"]
        success <- map["Success"]
    }
}
