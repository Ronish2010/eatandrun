//
//  EatAndRunPaymentHistoryViewController.swift
//  EatAndRun
//
//  Created by Sreeraj VR on 17/05/2017.
//  Copyright © 2017 Mawaqaa. All rights reserved.
//

import UIKit

class EatAndRunPaymentHistoryViewController: UIViewController ,UITableViewDelegate,UITableViewDataSource{
    
    @IBOutlet weak var paymentHistoryTable: UITableView!
    var paymentHistoryArray: NSArray = []
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let params = ["Cus_Id": "23","WebnaSecurityKey": "W3BN@53CUR6ITY8"]
        print(params)
        PAYMENTHISTORY_MANAGER.fetchpaymentHistoryFromServer(url: BASE_URL + "BillPayment/GetPaymentHistory", withParameters: params as NSDictionary, completion: { (result) -> Void in
            self.paymentHistoryArray =  PAYMENTHISTORY_MANAGER.paymentHistoryArray
            self.paymentHistoryTable.reloadData()
            
        })
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(self.paymentHistoryArray.count != 0)
        {
            return self.paymentHistoryArray.count
        }
            
        else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.paymentHistoryTable.dequeueReusableCell(withIdentifier: "PaymentHistoryCell", for: indexPath) as! PaymentHistoryCell
        cell.setData(paymentHistoryDict: paymentHistoryArray[indexPath.row] as! NSDictionary)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.paymentHistoryTable.frame.size.width * 0.707246376811594
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //self.restuarentMenuCategoryListTable.reloadData()
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

class PaymentHistoryCell: UITableViewCell {
    
    @IBOutlet weak var transactionId: UILabel!
    
    @IBOutlet weak var orderId: UILabel!
    
    @IBOutlet weak var paymentDate: UILabel!
    
    @IBOutlet weak var amount: UILabel!
    @IBOutlet weak var status: UILabel!
    
    
    
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setData(paymentHistoryDict: NSDictionary)
    {
        
        
        
        if paymentHistoryDict["OrderId"] != nil {
            
            self.orderId.text = String(describing: paymentHistoryDict["OrderId"] as! Int)
            
        }
        
        if paymentHistoryDict["TransactionId"] != nil {
            
            self.transactionId.text = paymentHistoryDict["TransactionId"] as? String
            
        }
        
        if paymentHistoryDict["PaymentDate"] != nil {
            
            self.paymentDate.text = paymentHistoryDict["PaymentDate"] as? String
            
        }
        
        if paymentHistoryDict["Amount"] != nil {
            
            self.amount.text = String(describing:paymentHistoryDict["Amount"] as! Int)
            
        }
        
        if paymentHistoryDict["Status"] != nil {
            
            self.status.text = paymentHistoryDict["Status"] as? String
            
        }
        
    }
    
    
}
