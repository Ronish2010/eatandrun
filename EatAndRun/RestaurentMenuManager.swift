//
//  RestaurentMenuManager.swift
//  EatAndRun
//
//  Created by Ronish on 5/4/17.
//  Copyright © 2017 Mawaqaa. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
import AlamofireObjectMapper

class RestaurentMenuManager: NSObject {
    var restaurentMenuList: [RestaurentMenuList]?
    var message: String?
    var success: String?
    
    var manager = Alamofire.SessionManager ()
    
    func fetchRestaurentMenuDetailsFromServer(url: String, withParameters: NSDictionary, completion: @escaping ( Bool) -> Void)
    {
        let configuration = URLSessionConfiguration.default
        self.manager = Alamofire.SessionManager(configuration: configuration)
        manager.request(url, method: .post, parameters: withParameters as? Parameters, encoding: JSONEncoding.default).responseObject (keyPath: "") { (response: DataResponse<RestaurentMenuMapper>) in
            
            let responsedata = response.result.value
            //print(responsedata?.restaurentMenuList!)
            
            if(responsedata?.restaurentMenuList != nil)
            {
                self.restaurentMenuList = (responsedata?.restaurentMenuList)!
                for list in self.restaurentMenuList!
                {
                    if list.menuSectionId == nil{
                        list.menuSectionId = 0
                    }
                    if list.menuSectionName == nil{
                        list.menuSectionName = ""
                    }
                    if list.menuSectionCount == nil{
                        list.menuSectionCount = 0
                    }
                    if list.menuLogo == nil{
                        list.menuLogo = ""
                    }
                }
            }else {
                self.restaurentMenuList = []
            }
            
            if(responsedata?.message != nil)
            {
                self.message = (responsedata?.message)!
            }
            
            if(responsedata?.success != nil)
            {
                self.success = (responsedata?.success)!
            }
            
            completion(true)
        }
    }
}

class RestaurentMenuMapper: Mappable {
    var restaurentMenuList: [RestaurentMenuList]?
    var message: String?
    var success: String?
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    func mapping(map: Map)
    {
        restaurentMenuList <- map["menuSectionList"]
        message <- map["Message"]
        success <- map["Success"]
    }
}

class RestaurentMenuList: Mappable {
    var menuSectionId: Int?
    var menuSectionName: String?
    var menuSectionCount: Int?
    var menuLogo: String?
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        menuSectionId <- map["MenuSectionId"]
        menuSectionName <- map["MenuSectionName"]
        menuSectionCount <- map["MenuSectionCount"]
        menuLogo <- map["Logo"]
    }
}
