//
//  RestaurentOrderHistoryManager.swift
//  EatAndRun
//
//  Created by Ronish on 5/15/17.
//  Copyright © 2017 Mawaqaa. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
import AlamofireObjectMapper

class RestaurentOrderHistoryManager: NSObject {
    
    var orderHistoryList: [RestaurentOrderHistoryListMapper]?
    var message: String?
    var success: String?
    
    var manager = Alamofire.SessionManager ()
    
    func fetchRestaurentOrderHistoryFromServer(url: String, withParameters: NSDictionary, completion: @escaping ( Bool) -> Void)
    {
        let configuration = URLSessionConfiguration.default
        self.manager = Alamofire.SessionManager(configuration: configuration)
        manager.request(url, method: .post, parameters: withParameters as? Parameters, encoding: JSONEncoding.default).responseObject (keyPath: "") { (response: DataResponse<RestaurentOrderHistoryMapper>) in
            
            let responsedata = response.result.value
            //print(responsedata?.phoneNumber!)
            
            if(responsedata?.orderHistoryList != nil)
            {
                self.orderHistoryList = (responsedata?.orderHistoryList)!
                for list in self.orderHistoryList!
                {
                    if list.orderNumber == nil{
                        list.orderNumber = ""
                    }
                    if list.cus_Id == nil{
                        list.cus_Id = 0
                    }
                    if list.res_Id == nil{
                        list.res_Id = 0
                    }
                    if list.totalAmount == nil{
                        list.totalAmount = 0
                    }
                    if list.message == nil{
                        list.message = ""
                    }
                    if list.orderDate == nil{
                        list.orderDate = ""
                    }
                    if list.orderStatus == nil{
                        list.orderStatus = ""
                    }
                }
            }else {
                self.orderHistoryList = []
            }
            
            if(responsedata?.message != nil)
            {
                self.message = (responsedata?.message)!
            }
            
            if(responsedata?.success != nil)
            {
                self.success = (responsedata?.success)!
            }
            
            completion(true)
        }
    }
}

class RestaurentOrderHistoryMapper: Mappable {
    
    var orderHistoryList: [RestaurentOrderHistoryListMapper]?
    var message: String?
    var success: String?
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    func mapping(map: Map)
    {
        orderHistoryList <- map["openBillmodelList"]
        message <- map["Message"]
        success <- map["Success"]
    }
}

class RestaurentOrderHistoryListMapper: Mappable {
    
    var orderNumber: String?
    var cus_Id: Int?
    var res_Id: Int?
    var totalAmount: Int?
    var message: String?
    var orderDate: String?
    var orderStatus: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map)
    {
        orderNumber <- map["OpenOrderNumber"]
        cus_Id <- map["Cus_Id"]
        res_Id <- map["Res_Id"]
        totalAmount <- map["TotalAmount"]
        message <- map["Message"]
        orderDate <- map["OrderDate"]
        orderStatus <- map["OrderStatus"]
    }
}
