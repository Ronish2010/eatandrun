//
//  RestaurentOrderHistoryDetailsManager.swift
//  EatAndRun
//
//  Created by Ronish on 5/16/17.
//  Copyright © 2017 Mawaqaa. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
import AlamofireObjectMapper

class RestaurentOrderHistoryDetailsManager: NSObject {

    var orderNumber: String?
    var cus_Id: Int?
    var res_Id: Int?
    var restaurantName: String?
    var waiterId: String?
    var waiterName: String?
    var noOfCustomers: Int?
    var tableNo: String?
    var totalAmount: Int?
    var LanguageId: Int?
    var message: String?
    var success: String?
    var orderItemList: [RestaurentOrderItemDetailsMapper]?
    var subOrderItemList: [RestaurentOrderItemDetailsMapper]?
    
    var manager = Alamofire.SessionManager ()
    
    func fetchRestaurentOrderHistoryDetailsFromServer(url: String, withParameters: NSDictionary, completion: @escaping ( Bool) -> Void)
    {
        let configuration = URLSessionConfiguration.default
        self.manager = Alamofire.SessionManager(configuration: configuration)
        manager.request(url, method: .post, parameters: withParameters as? Parameters, encoding: JSONEncoding.default).responseObject (keyPath: "") { (response: DataResponse<RestaurentOrderHistoryDetailsMapper>) in
            
            let responsedata = response.result.value
            //print(responsedata?.phoneNumber!)
            
            if(responsedata?.orderNumber != nil)
            {
                self.orderNumber = (responsedata?.orderNumber)!
            }
            
            if(responsedata?.cus_Id != nil)
            {
                self.cus_Id = (responsedata?.cus_Id)!
            }
            
            if(responsedata?.res_Id != nil)
            {
                self.res_Id = (responsedata?.res_Id)!
            }
            
            if(responsedata?.restaurantName != nil)
            {
                self.restaurantName = (responsedata?.restaurantName)!
            }
            
            if(responsedata?.waiterId != nil)
            {
                self.waiterId = (responsedata?.waiterId)!
            }
            
            if(responsedata?.waiterName != nil)
            {
                self.waiterName = (responsedata?.waiterName)!
            }
            
            if(responsedata?.noOfCustomers != nil)
            {
                self.noOfCustomers = (responsedata?.noOfCustomers)!
            }
            
            if(responsedata?.tableNo != nil)
            {
                self.tableNo = (responsedata?.tableNo)!
            }
            
            if(responsedata?.totalAmount != nil)
            {
                self.totalAmount = (responsedata?.totalAmount)!
            }
            
            if(responsedata?.LanguageId != nil)
            {
                self.LanguageId = (responsedata?.LanguageId)!
            }
            
            if(responsedata?.message != nil)
            {
                self.message = (responsedata?.message)!
            }
            
            if(responsedata?.success != nil)
            {
                self.success = (responsedata?.success)!
            }
            
            if(responsedata?.orderItemList != nil)
            {
                self.orderItemList = (responsedata?.orderItemList)!
                for list in self.orderItemList!
                {
                    if list.detailId == nil{
                        list.detailId = 0
                    }
                    if list.orderId == nil{
                        list.orderId = 0
                    }
                    if list.menuItemId == nil{
                        list.menuItemId = 0
                    }
                    if list.menuItemName == nil{
                        list.menuItemName = ""
                    }
                    if list.menuItemImage == nil{
                        list.menuItemImage = ""
                    }
                    if list.price == nil{
                        list.price = 0
                    }
                    if list.quantity == nil{
                        list.quantity = 0
                    }
                    if list.total == nil{
                        list.total = 0
                    }
                }
            }else {
                self.orderItemList = []
            }
            
            if(responsedata?.subOrderItemList != nil)
            {
                self.subOrderItemList = (responsedata?.subOrderItemList)!
                for list in self.subOrderItemList!
                {
                    if list.detailId == nil{
                        list.detailId = 0
                    }
                    if list.orderId == nil{
                        list.orderId = 0
                    }
                    if list.menuItemId == nil{
                        list.menuItemId = 0
                    }
                    if list.menuItemName == nil{
                        list.menuItemName = ""
                    }
                    if list.menuItemImage == nil{
                        list.menuItemImage = ""
                    }
                    if list.price == nil{
                        list.price = 0
                    }
                    if list.quantity == nil{
                        list.quantity = 0
                    }
                    if list.total == nil{
                        list.total = 0
                    }
                }
            }else {
                self.subOrderItemList = []
            }
            
            completion(true)
        }
    }
}

class RestaurentOrderHistoryDetailsMapper: Mappable {
    
    var orderNumber: String?
    var cus_Id: Int?
    var res_Id: Int?
    var restaurantName: String?
    var waiterId: String?
    var waiterName: String?
    var noOfCustomers: Int?
    var tableNo: String?
    var totalAmount: Int?
    var LanguageId: Int?
    var message: String?
    var success: String?
    var orderItemList: [RestaurentOrderItemDetailsMapper]?
    var subOrderItemList: [RestaurentOrderItemDetailsMapper]?
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    func mapping(map: Map)
    {
        orderNumber <- map["OpenOrderNumber"]
        cus_Id <- map["Cus_Id"]
        res_Id <- map["Res_Id"]
        restaurantName <- map["RestaurantName"]
        waiterId <- map["WaiterId"]
        waiterName <- map["WaiterName"]
        noOfCustomers <- map["NoOfCustomers"]
        tableNo <- map["TableNo"]
        totalAmount <- map["TotalAmount"]
        LanguageId <- map["LanguageId"]
        message <- map["Message"]
        success <- map["Success"]
        orderItemList <- map["apimainorderitemlist"]
        subOrderItemList <- map["apisuborderitemlist"]
    }
}

class RestaurentOrderItemDetailsMapper: Mappable {
    
    var detailId: Int?
    var orderId: Int?
    var menuItemId: Int?
    var menuItemName: String?
    var menuItemImage: String?
    var price: Int?
    var quantity: Int?
    var total: Int?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map)
    {
        detailId <- map["DetailId"]
        orderId <- map["OrderId"]
        menuItemId <- map["MenuItemId"]
        menuItemName <- map["MenuItemName"]
        menuItemImage <- map["MenuItemImage"]
        price <- map["Price"]
        quantity <- map["Quantity"]
        total <- map["Total"]
    }
}
