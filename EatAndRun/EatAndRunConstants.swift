//
//  EatAndRunConstants.swift
//  EatAndRun
//
//  Created by Ronish on 3/13/17.
//  Copyright © 2017 Mawaqaa. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

// MARK: - Global constants

let THEME_BLUE_COLOUR = UIColor(red: 0, green: 0, blue: 0.5176, alpha: 1.0) /* 000084 */
let THEME_ORANGE_COLOUR = UIColor(red: 1, green: 0.7098, blue: 0.0392, alpha: 1.0) /* #ffb50a */
var NAVIGATION_POINTER : UINavigationController = UINavigationController()
var ROOT_NAVIGATION_POINTER : UINavigationController = UINavigationController()
var IS_REACHABLE: Bool!
let APP_ID = "fd725621c5e44198a5b8ad3f7a0ffa09"
let EatAndRun_DELEGATE: EatAndRun_CustomDelegates = EatAndRun_CustomDelegates()
let EatAndRun_DEFAULTS = UserDefaults.standard
let REACHIBILITY: EatAndRunReachabilityCheck = EatAndRunReachabilityCheck()
var LANGUAGE = EatAndRun_DEFAULTS.string(forKey: "Language")
let THEME_COLOUR = UIColor(red: 0.0823, green: 0.1568, blue: 0.2901, alpha: 1.0) /* #c82d33 */
var DEVICE_TOKEN = EatAndRun_DEFAULTS.value(forKey: "DeviceToken")

var LOCATION_MANAGER = CLLocationManager()
let BASE_URL = "http://eatrun.mawaqaademo.com/api/Services/"

struct INSTAGRAM_IDS {
    
    static let INSTAGRAM_AUTHURL = "https://api.instagram.com/oauth/authorize/"
    
    static let INSTAGRAM_APIURl  = "https://api.instagram.com/v1/users/"
    
    static let INSTAGRAM_CLIENT_ID  = "bd6ecdfae55b4f409694f610b2934623"
    
    static let INSTAGRAM_CLIENTSERCRET = "de6c3f5ecf56459da6a1faffd2fb6932"
    
    static let INSTAGRAM_REDIRECT_URI = "http://www.clover-studio.com/"
    
    static let INSTAGRAM_ACCESS_TOKEN =  "access_token"
    
    static let INSTAGRAM_SCOPE = "likes+comments+relationships"
    
}

// MARK: - Manager

let LOGIN_MANAGER : LoginManager = LoginManager()
let GET_COUNTRY_MANAGER : GetCountryManager = GetCountryManager()
let GET_AREA_MANAGER : GetAreaManager = GetAreaManager()
let CUSTOMER_REGISTRATION_MANAGER : CustomerRegistrationManager = CustomerRegistrationManager()
let RESTAURANT_REGISTRATION_MANAGER : RestaurantRegistrationManager = RestaurantRegistrationManager()
let FORGOT_PASSWORD_MANAGER : ForgotPasswordManager = ForgotPasswordManager()
let HOME_SERVICE_MANAGER : HomeServiceManager = HomeServiceManager()
let RESTAURENT_LIST_MANAGER : RestaurentListManager = RestaurentListManager()
let RESTAURENT_DETAILS_MANAGER : RestaurentDetailsManager = RestaurentDetailsManager()
let RESTAURENT_MENU_MANAGER : RestaurentMenuManager = RestaurentMenuManager()
let RESTAURENT_CUISINES_MANAGER : RestaurentCuisinesManager = RestaurentCuisinesManager()
let RESTAURENT_MENU_LIST_MANAGER : RestaurentMenuListManager = RestaurentMenuListManager()
let RESTAURENT_CUISINES_LIST_MANAGER : RestaurentCuisinesListManager = RestaurentCuisinesListManager()
let RESTAURENT_MENU_ITEM_MANAGER : RestaurentMenuItemManager = RestaurentMenuItemManager()
let RESTAURENT_OFFER_LIST_MANAGER : RestaurentOfferListManager = RestaurentOfferListManager()
let RESTAURENT_OFFER_ITEM_MANAGER : RestaurentOfferItemManager = RestaurentOfferItemManager()
let RESTAURENT_OPEN_BILL_MANAGER : RestaurentOpenBillManager = RestaurentOpenBillManager()
let RESTAURENT_OPEN_BILL_PROCEED_MANAGER : RestaurentOpenBillProceedManager = RestaurentOpenBillProceedManager()
let RESTAURENT_OPEN_BILL_DETAILS_MANAGER : RestaurentOpenBillDetailsManager = RestaurentOpenBillDetailsManager()
let RESTAURENT_PROFILE_MANAGER : RestaurentProfileManager = RestaurentProfileManager()
let RESTAURENT_EDIT_PROFILE_MANAGER : RestaurentEditProfileManager = RestaurentEditProfileManager()
let CHANGE_PASSWORD_MANAGER : ChangePasswordManager = ChangePasswordManager()
let DEVICE_REGISTRATION_MANAGER : DeviceRegistrationManager = DeviceRegistrationManager()
let RESTAURENT_CONTACTUS_MANAGER : RestaurentContactUsManager = RestaurentContactUsManager()
let RESTAURENT_ORDER_HISTORY_MANAGER : RestaurentOrderHistoryManager = RestaurentOrderHistoryManager()
let RESTAURENT_ORDER_HISTORY_DETAILS_MANAGER : RestaurentOrderHistoryDetailsManager = RestaurentOrderHistoryDetailsManager()
let RESTAURENT_GLOBAL_DATA_MANAGER : RestaurentGlobalDataManager = RestaurentGlobalDataManager()
let PAYMENTHISTORY_MANAGER : PaymentHistoryManager = PaymentHistoryManager()
let SPLITBILLEVENLY_MANAGER :SplitBillEvenlyManager = SplitBillEvenlyManager()
let SOCIALMEDIA_LOGIN_MANAGER : SocialMediaLoginManager = SocialMediaLoginManager()

