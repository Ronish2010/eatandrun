//
//  EatAndRunMyAccountSettingsViewController.swift
//  EatAndRun
//
//  Created by Ronish on 5/12/17.
//  Copyright © 2017 Mawaqaa. All rights reserved.
//

import UIKit

class EatAndRunMyAccountSettingsViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var firstNameText: UITextField!
    @IBOutlet weak var lastNameText: UITextField!
    @IBOutlet weak var mobileNumberText: UITextField!
    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var tipValueText: UITextField!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var logoutButton: UIButton!
    @IBOutlet weak var changePasswordButton: UIButton!
    @IBOutlet weak var changePasswordPopUpBackView: UIView!
    @IBOutlet weak var changePasswordPopUpView: UIView!
    @IBOutlet weak var changePasswordCloseButton: UIButton!
    @IBOutlet weak var changePasswordSubmitButton: UIButton!
    @IBOutlet weak var oldPasswordWhiteBackView: UIView!
    @IBOutlet weak var oldPasswordTextView: UITextField!
    @IBOutlet weak var newPasswordWhiteBackView: UIView!
    @IBOutlet weak var newPasswordTextView: UITextField!
    @IBOutlet var textBackView: [UIView]!
    
    var message: String?
    var success: String?
    var customerID: String?
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
        self.changePasswordPopUpBackView.isHidden = true
    }
    
    override func viewDidLayoutSubviews() {
        self.changePasswordPopUpView.layer.borderWidth = 4
        self.changePasswordPopUpView.layer.borderColor = THEME_ORANGE_COLOUR.cgColor
        self.changePasswordPopUpView.layer.cornerRadius = 10
        self.changePasswordCloseButton.layer.cornerRadius = self.changePasswordCloseButton.frame.size.height/2
        self.changePasswordCloseButton.layer.borderColor = UIColor.white.cgColor
        self.changePasswordCloseButton.layer.borderWidth = 2
        self.oldPasswordWhiteBackView.layer.cornerRadius = self.oldPasswordWhiteBackView.frame.size.height/2
        self.oldPasswordWhiteBackView.clipsToBounds = true
        self.newPasswordWhiteBackView.layer.cornerRadius = self.newPasswordWhiteBackView.frame.size.height/2
        self.newPasswordWhiteBackView.clipsToBounds = true
        self.changePasswordSubmitButton.layer.cornerRadius = self.changePasswordSubmitButton.frame.size.height/2
        self.changePasswordSubmitButton.clipsToBounds = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getProfileData()
        // Do any additional setup after loading the view.
    }
    
    // MARK: - Web Services
    
    // MARK:  GET OPEN BILL DETAILS
    
    func getProfileData() {
        EatAndRun_DELEGATE.showActivity(myView: self.view, myTitle: "Loading..".localized(lang: LANGUAGE!))
        if !(REACHIBILITY.statusNetworkCheck())
        {
            EatAndRun_DELEGATE.removeActivity(myView: self.view)
            EatAndRun_DELEGATE.showAlert(title: "Sorry", message: "No network available", buttonTitle: "ok")
        }
        else
        {
            let params = ["Cus_Id": EatAndRun_DEFAULTS .value(forKey: "CUSTOMER_ID")!, "LanguageId": LANGUAGE!,"WebnaSecurityKey": "W3BN@53CUR6ITY8"]
            print(params)
            RESTAURENT_PROFILE_MANAGER.fetchRestaurentProfileFromServer(url: BASE_URL + "Customer/ViewCustomer", withParameters: params as NSDictionary, completion: { (result) -> Void in
                EatAndRun_DELEGATE.removeActivity(myView: self.view)
                self.firstNameText.text = RESTAURENT_PROFILE_MANAGER.firstName
                self.lastNameText.text = RESTAURENT_PROFILE_MANAGER.lastName
                self.mobileNumberText.text = RESTAURENT_PROFILE_MANAGER.customerMobile
                self.emailText.text = RESTAURENT_PROFILE_MANAGER.customerEmail
                self.tipValueText.text = String(describing:RESTAURENT_PROFILE_MANAGER.customerTip!)
            })
        }
    }
    
    func postEditProfileData() {
        EatAndRun_DELEGATE.showActivity(myView: self.view, myTitle: "Loading..".localized(lang: LANGUAGE!))
        if !(REACHIBILITY.statusNetworkCheck())
        {
            EatAndRun_DELEGATE.removeActivity(myView: self.view)
            EatAndRun_DELEGATE.showAlert(title: "Sorry", message: "No network available", buttonTitle: "ok")
        }
        else
        {
            let params = ["Cus_Id": EatAndRun_DEFAULTS .value(forKey: "CUSTOMER_ID")!,"Cus_FirstName": self.firstNameText.text! ,"Cus_LastName": self.lastNameText.text!,"Cus_Email": self.emailText.text!,"Cus_Mobile": self.mobileNumberText.text!,"Cus_Tip": self.tipValueText.text!, "LanguageId": LANGUAGE!,"WebnaSecurityKey": "W3BN@53CUR6ITY8"]
            print(params)
            RESTAURENT_EDIT_PROFILE_MANAGER.postRestaurentEditProfileToServer(url: BASE_URL + "Customer/EditCustomer", withParameters: params as NSDictionary, completion: { (result) -> Void in
                EatAndRun_DELEGATE.removeActivity(myView: self.view)
                if RESTAURENT_EDIT_PROFILE_MANAGER.success == "Success"{
                    //self.performSegue(withIdentifier: "HomePage", sender: self)
                    EatAndRun_DELEGATE.showAlert(title: "", message: RESTAURENT_EDIT_PROFILE_MANAGER.message!, buttonTitle: "ok")
                } else {
                    EatAndRun_DELEGATE.showAlert(title: "", message: RESTAURENT_EDIT_PROFILE_MANAGER.message!, buttonTitle: "ok")
                }
            })
        }
    }
    
    // MARK: - BUTTON ACTIONS

    @IBAction func editButtonAction(_ sender: UIButton) {
        var message: String!
        
        if EatAndRun_DELEGATE.TrimString(trimString: self.firstNameText.text!) == "" {
            message = "First name cannot be empty".localized(lang: LANGUAGE!)
        }else if EatAndRun_DELEGATE.TrimString(trimString: self.lastNameText.text!) == "" {
            message = "Last name cannot be empty".localized(lang: LANGUAGE!)
        }else if EatAndRun_DELEGATE.TrimString(trimString: self.mobileNumberText.text!) == "" {
            message = "Mobile number cannot be empty".localized(lang: LANGUAGE!)
        }else if EatAndRun_DELEGATE.TrimString(trimString: self.emailText.text!) == "" {
            message = "Email cannot be empty".localized(lang: LANGUAGE!)
        }else if EatAndRun_DELEGATE.isValidEmail(testStr: self.emailText.text!) == false {
            message = "Please enter a valid E-mail".localized(lang: LANGUAGE!)
        }else if EatAndRun_DELEGATE.TrimNumbers(trimString: self.mobileNumberText.text!) == false {
            message = "Phone Number cannot enter characters".localized(lang: LANGUAGE!)
        }else if self.mobileNumberText.text!.characters.count < 8 {
            message = "Please enter a valid Phone Number".localized(lang: LANGUAGE!)
        }
        
        if message == nil {
            postEditProfileData()
        }else {
            EatAndRun_DELEGATE.showAlert(title: "Message".localized(lang: LANGUAGE!), message: message, buttonTitle: "OK".localized(lang: LANGUAGE!))
        }
    }
    
    @IBAction func logoutButtonAction(_ sender: UIButton) {
        let alert = UIAlertController(title: "", message: "Do you want to logout?".localized(lang: LANGUAGE!), preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Continue".localized(lang: LANGUAGE!), style: UIAlertActionStyle.default, handler: { action in
            EatAndRun_DEFAULTS.setValue("", forKey: "CUSTOMER_ID")
            EatAndRun_DEFAULTS.set(false, forKey: "KEEP_ME_SIGNED_IN")
            let storage = HTTPCookieStorage.shared
            if let cookies = storage.cookies
            {
                for cookie in cookies
                {
                    storage.deleteCookie(cookie)
                }
            }
            //let mainView: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let viewcontroller : EatAndRunLoginViewController = self.storyboard!.instantiateViewController(withIdentifier: "EatAndRunLoginViewController") as! EatAndRunLoginViewController
            self.present(viewcontroller, animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Cancel".localized(lang: LANGUAGE!), style: UIAlertActionStyle.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func changePasswordButtonAction(_ sender: UIButton) {
        self.changePasswordPopUpBackView.isHidden = false
    }
    
    // MARK: - Change password Submit Button Action
    
    @IBAction func changePasswordSubmitButtonAction(_ sender: UIButton) {
        self.view.endEditing(true)
        if self.oldPasswordTextView.text == "" {
            EatAndRun_DELEGATE.showAlert(title: "Message".localized(lang: LANGUAGE!), message: "Enter old password".localized(lang: LANGUAGE!), buttonTitle: "OK".localized(lang: LANGUAGE!))
        }else if self.newPasswordTextView.text == ""{
            EatAndRun_DELEGATE.showAlert(title: "Message".localized(lang: LANGUAGE!), message: "Enter new password".localized(lang: LANGUAGE!), buttonTitle: "OK".localized(lang: LANGUAGE!))
        }else{
            self.changePasswordPopUpBackView.isHidden = true
            EatAndRun_DELEGATE.showActivity(myView: self.view, myTitle: "Loading..".localized(lang: LANGUAGE!))
            postChangePassword()
        }
    }
    
    // MARK: - PopUp Close Button Action
    
    @IBAction func popUpCloseButtonAction(_ sender: UIButton) {
        self.view.endEditing(true)
        for txtViews in textBackView {
            txtViews.backgroundColor = UIColor.white
        }
        self.changePasswordPopUpBackView.isHidden = true
    }
    
    // MARK: - TEXTFIELD DELEGATE
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print(textField.tag)
        for txtViews in textBackView {
            if txtViews.tag ==  textField.tag{
                txtViews.backgroundColor = THEME_ORANGE_COLOUR
            } else {
                txtViews.backgroundColor = UIColor.white
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        for txtViews in textBackView {
            txtViews.backgroundColor = UIColor.white
        }
        return false
    }
    
    // MARK: - Web Services
    
    func postChangePassword() {
        if !(REACHIBILITY.statusNetworkCheck())
        {
            EatAndRun_DELEGATE.removeActivity(myView: self.view)
            EatAndRun_DELEGATE.showAlert(title: "Sorry", message: "No network available", buttonTitle: "ok")
        }
        else
        {
            let params = ["Cus_Id": EatAndRun_DEFAULTS .value(forKey: "CUSTOMER_ID")!, "WebnaSecurityKey": "W3BN@53CUR6ITY8", "LanguageId": LANGUAGE!, "OldPassword": self.oldPasswordTextView.text!, "NewPassword": self.newPasswordTextView.text!]
            print(params)
            CHANGE_PASSWORD_MANAGER.fetchChangePasswordDetailsFromServer(url: BASE_URL + "Customer/ChangePassword", withParameters: params as NSDictionary, completion: { (result) -> Void in
                EatAndRun_DELEGATE.removeActivity(myView: self.view)
                self.customerID = String(describing: CHANGE_PASSWORD_MANAGER.customerID!)
                self.message = CHANGE_PASSWORD_MANAGER.message
                self.success = CHANGE_PASSWORD_MANAGER.success
                if self.success == "Success"{
                    //self.performSegue(withIdentifier: "HomePage", sender: self)
                    EatAndRun_DELEGATE.showAlert(title: "", message: self.message!, buttonTitle: "ok")
                } else {
                    EatAndRun_DELEGATE.showAlert(title: "", message: self.message!, buttonTitle: "ok")
                }
            })
            self.oldPasswordTextView.text = ""
            self.newPasswordTextView.text = ""
            self.changePasswordPopUpBackView.isHidden = true
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
