//
//  EatAndRunNotificationListViewController.swift
//  EatAndRun
//
//  Created by Ronish on 3/23/17.
//  Copyright © 2017 Mawaqaa. All rights reserved.
//

import UIKit

class EatAndRunNotificationListViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var restuarentNotificationListTable: UITableView!
    @IBOutlet weak var editButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    // MARK: - TABLEVIEW DELEGATES
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.restuarentNotificationListTable.dequeueReusableCell(withIdentifier: "RestaurentNotificationListCell", for: indexPath) as! RestaurentNotificationListCell
        cell.imageBackView.layer.cornerRadius = cell.imageBackView.frame.size.width/2
        cell.imageBackView.layer.borderColor = THEME_BLUE_COLOUR.cgColor
        cell.imageBackView.layer.borderWidth = 1
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.restuarentNotificationListTable.frame.size.width * 0.272445820433437
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //self.restuarentMenuCategoryListTable.reloadData()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func editButtonAction(_ sender: UIButton) {
        performSegue(withIdentifier: "NotificationDelete", sender: self)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

class RestaurentNotificationListCell: UITableViewCell {
    @IBOutlet weak var imageBackView: UIView!
    @IBOutlet weak var menuNotificationItemImage: UIImageView!
    @IBOutlet weak var notificationDescriptionTxt: UITextView!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
}
