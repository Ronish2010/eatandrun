//
//  RestaurentOfferItemManager.swift
//  EatAndRun
//
//  Created by Ronish on 5/8/17.
//  Copyright © 2017 Mawaqaa. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
import AlamofireObjectMapper

class RestaurentOfferItemManager: NSObject {
    var menuItemId: Int?
    var menuItemName: String?
    var menuItemDescription: String?
    var menuItemImage: String?
    var menuItemPrice: Float?
    var discountPrice: Float?
    var message: String?
    var success: String?
    
    var manager = Alamofire.SessionManager ()
    
    func fetchRestaurentOfferItemDetailsFromServer(url: String, withParameters: NSDictionary, completion: @escaping ( Bool) -> Void)
    {
        let configuration = URLSessionConfiguration.default
        self.manager = Alamofire.SessionManager(configuration: configuration)
        manager.request(url, method: .post, parameters: withParameters as? Parameters, encoding: JSONEncoding.default).responseObject (keyPath: "") { (response: DataResponse<RestaurentOfferItemMapper>) in
            
            let responsedata = response.result.value
            //print(responsedata?.restaurentMenuList!)
            
            if (responsedata?.menuItemId != nil) {
                self.menuItemId = (responsedata?.menuItemId)!
            }
            
            if (responsedata?.menuItemName != nil) {
                self.menuItemName = (responsedata?.menuItemName)!
            }
            
            if (responsedata?.menuItemDescription != nil) {
                self.menuItemDescription = (responsedata?.menuItemDescription)!
            }
            
            if (responsedata?.menuItemImage != nil) {
                self.menuItemImage = (responsedata?.menuItemImage)!
            }
            
            if (responsedata?.menuItemPrice != nil) {
                self.menuItemPrice = (responsedata?.menuItemPrice)!
            }
            
            if (responsedata?.discountPrice != nil) {
                self.discountPrice = (responsedata?.discountPrice)!
            }
            
            if(responsedata?.message != nil)
            {
                self.message = (responsedata?.message)!
            }
            
            if(responsedata?.success != nil)
            {
                self.success = (responsedata?.success)!
            }
            
            completion(true)
        }
    }
}

class RestaurentOfferItemMapper: Mappable {
    var menuItemId: Int?
    var menuItemName: String?
    var menuItemDescription: String?
    var menuItemImage: String?
    var discountPrice: Float?
    var menuItemPrice: Float?
    var message: String?
    var success: String?
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    func mapping(map: Map)
    {
        menuItemId <- map["MenuItemId"]
        menuItemName <- map["MenuItemName"]
        menuItemDescription <- map["MenuItemDescription"]
        menuItemImage <- map["MenuItemImage"]
        menuItemPrice <- map["Price"]
        discountPrice <- map["Discount"]
        message <- map["Message"]
        success <- map["Success"]
    }
}
