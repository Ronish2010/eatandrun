//
//  EatAndRunReataurantRegistrationViewController.swift
//  EatAndRun
//
//  Created by Ronish on 3/13/17.
//  Copyright © 2017 Mawaqaa. All rights reserved.
//

import UIKit


class EatAndRunReataurantRegistrationViewController: UIViewController, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var textBackView: [UIView]!
    @IBOutlet var registrationTextViews: [UITextField]!
    @IBOutlet weak var termsAndConditionAcceptButton: UIButton!
    @IBOutlet weak var termsAndConditionButton: UIButton!
    @IBOutlet weak var patentInformationButton: UIButton!
    @IBOutlet weak var countryButton: UIButton!
    @IBOutlet weak var selectCountryDropDownView: UIView!
    @IBOutlet weak var selectCountryTableView: UITableView!
    @IBOutlet weak var countryTableHeight: NSLayoutConstraint!
    
    var activeTextField = UITextField()
    var attrs = [
        NSFontAttributeName : UIFont.systemFont(ofSize: 15.0),
        NSForegroundColorAttributeName : UIColor.darkGray,
        NSUnderlineStyleAttributeName : 1] as [String : Any]
    var attributedString = NSMutableAttributedString(string:"")
    var is_CountryDropDownShow = false
    var countryArray: [CountryDetails]?
    let placeHolderArray: [String] = ["Create Name".localized(lang: LANGUAGE!), "Create Password".localized(lang:LANGUAGE!), "Restaurant Name".localized(lang:LANGUAGE!), "Company Name".localized(lang:LANGUAGE!), "Person in Charge".localized(lang:LANGUAGE!), "Email".localized(lang:LANGUAGE!), "Country".localized(lang:LANGUAGE!), "Mobile Number".localized(lang:LANGUAGE!), "Telephone Number".localized(lang:LANGUAGE!), "Address".localized(lang:LANGUAGE!), "Company Commision".localized(lang:LANGUAGE!), "KENET Commision".localized(lang:LANGUAGE!), "Credit Card Commision".localized(lang:LANGUAGE!)]
    var registrationArray = [String](repeating: "", count: 13)
    
    var restaurantID: Int?
    var message: String?
    var success: String?
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
        self.termsAndConditionAcceptButton.isSelected = false
        self.termsAndConditionAcceptButton.backgroundColor = UIColor.white
        getCountry()
    }
    
    override func viewDidLayoutSubviews() {
        var radius : CGFloat = 0
        for textbackView in self.textBackView {
            if textbackView.tag == 9 {
                textbackView.layer.cornerRadius = 5
            } else {
                textbackView.layer.cornerRadius = textbackView.frame.size.height/2
                radius = textbackView.frame.size.height/2
            }
            textbackView.layer.borderWidth = 1
            textbackView.layer.borderColor = UIColor.lightGray.cgColor
        }
        self.termsAndConditionAcceptButton.layer.borderColor = UIColor.black.cgColor
        self.termsAndConditionAcceptButton.layer.borderWidth = 4
        self.selectCountryDropDownView.layer.borderWidth = 1
        self.selectCountryDropDownView.layer.cornerRadius = radius
        self.selectCountryDropDownView.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        //activeTextField = nil
        registrationArray[textField.tag] = EatAndRun_DELEGATE.TrimString(trimString: textField.text!)
        print(registrationArray)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let termsAndConditionButtonTitleStr = NSMutableAttributedString(string:"Terms and Conditions", attributes:attrs)
        attributedString.append(termsAndConditionButtonTitleStr)
        self.termsAndConditionButton.setAttributedTitle(attributedString, for: .normal)
        attributedString = NSMutableAttributedString(string:"")
        let patentInformationButtonTitleStr = NSMutableAttributedString(string:"Patent Information", attributes:attrs)
        attributedString.append(patentInformationButtonTitleStr)
        self.patentInformationButton.setAttributedTitle(attributedString, for: .normal)
        // Do any additional setup after loading the view.
    }
    
    // MARK: - TEXTFIELD DELEGATE
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print(textField.tag)
        showDropDownTable(buttonTag: 0)
        for txtViews in textBackView {
            if txtViews.tag ==  textField.tag{
                txtViews.backgroundColor = THEME_ORANGE_COLOUR
            } else {
                txtViews.backgroundColor = UIColor.white
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        for txtViews in textBackView {
            txtViews.backgroundColor = UIColor.white
        }
        return false
    }
    
    // MARK: - BACK BUTTON ACTION
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        _ = navigationController?.popViewController(animated: true)
    }
    
   
    
    // MARK: - COUNTRY BUTTON ACTION
    
    @IBAction func countryButtonAction(_ sender: UIButton) {
        showDropDownTable(buttonTag: 1)
        for txtViews in textBackView {
            if txtViews.tag ==  6{
                txtViews.backgroundColor = THEME_ORANGE_COLOUR
            } else {
                txtViews.backgroundColor = UIColor.white
            }
        }
    }
    
    // MARK: - TERMS AND CONDITION ACCEPT BUTTON ACTION
    
    @IBAction func termsAndConditionAcceptBtnActn(_ sender: UIButton) {
        if self.termsAndConditionAcceptButton.isSelected{
            self.termsAndConditionAcceptButton.isSelected = false
            self.termsAndConditionAcceptButton.backgroundColor = UIColor.white
        } else {
            self.termsAndConditionAcceptButton.isSelected = true
            self.termsAndConditionAcceptButton.backgroundColor = THEME_ORANGE_COLOUR
        }
    }
    
    // MARK: - REGESTER BUTTON ACTION
    
    @IBAction func registerButtonAction(_ sender: UIButton) {
        self.view.endEditing(true)
        
        var message: String!
        
        for i in 0..<registrationArray.count {
            if EatAndRun_DELEGATE.TrimString(trimString: registrationArray[i]) == "" {
                message = "\(placeHolderArray[i].capitalized) \("cannot be empty".localized(lang: LANGUAGE!))"
                break
            }
        }
        

        if message == nil {
            if EatAndRun_DELEGATE.isValidEmail(testStr: registrationArray[5]) == false {
                message = "Please enter a valid E-mail".localized(lang: LANGUAGE!)
            }else if EatAndRun_DELEGATE.TrimNumbers(trimString: registrationArray[7]) == false {
                message = "Phone Number cannot enter characters".localized(lang: LANGUAGE!)
            }else if EatAndRun_DELEGATE.TrimNumbers(trimString: registrationArray[8]) == false {
                message = "Telephone Number cannot enter characters".localized(lang: LANGUAGE!)
            }else if registrationArray[7].characters.count < 10 {
                message = "Please enter a valid Phone Number".localized(lang: LANGUAGE!)
            }else if registrationArray[8].characters.count < 8 {
                message = "Please enter a valid Telephone Number".localized(lang: LANGUAGE!)
            }else if !self.termsAndConditionAcceptButton.isSelected {
                message = "Please accept the Terms and Condition".localized(lang: LANGUAGE!)
            }
        }
        
        if message == nil {
            postRegistration()
        }else {
            EatAndRun_DELEGATE.showAlert(title: "Message".localized(lang: LANGUAGE!), message: message, buttonTitle: "OK".localized(lang: LANGUAGE!))
        }
    }
    
    func showDropDownTable(buttonTag : Int) {
        
        if buttonTag == 0{
            UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseInOut, animations: { () -> Void in
                self.selectCountryDropDownView.frame.size.height = 0
            }) { (completed: Bool) -> Void in
                self.is_CountryDropDownShow = false
            }
        } else if buttonTag == 1 {
            if self.countryArray != nil {
                if (self.countryArray?.count)! > 0 {
                    UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseInOut, animations: { () -> Void in
                        if self.is_CountryDropDownShow == false {
                            if (self.countryArray?.count)! < 4 {
                                self.selectCountryDropDownView.frame.size.height = CGFloat((self.countryArray?.count)! * 30)
                                self.countryTableHeight.constant = self.selectCountryDropDownView.frame.size.height
                            }else {
                                self.selectCountryDropDownView.frame.size.height = CGFloat(4 * 30)
                                self.countryTableHeight.constant = self.selectCountryDropDownView.frame.size.height
                            }
                        }else {
                            self.selectCountryDropDownView.frame.size.height = 0
                        }
                    }) { (completed: Bool) -> Void in
                        self.view.setNeedsLayout()
                        self.view.layoutIfNeeded()
                        if self.is_CountryDropDownShow == false {
                            self.is_CountryDropDownShow = true
                        }else {
                            self.is_CountryDropDownShow = false
                        }
                    }
                }
            }
        }
    }
    
    // MARK: - Tableview Delegate Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if countryArray != nil {
            if (countryArray?.count)! > 0 {
                return countryArray!.count
            }else {
                return 0
            }
        }else {
            return 0
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = selectCountryTableView.dequeueReusableCell(withIdentifier: "DROPDOWN", for: indexPath as IndexPath) as! DropDownCell
        cell.valueLbl.text = self.countryArray?[indexPath.row].countryName
        return cell
    }
    
   
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        showDropDownTable(buttonTag: 0)
        print(self.registrationTextViews)
        self.registrationTextViews[6].text = self.countryArray?[indexPath.row].countryName
        let countryId = self.countryArray?[indexPath.row].countryId
        registrationArray[6] = String(describing: countryId!)
    }
    
    
    
    
    // MARK: - Web Services
    
    // MARK:  GET COUNTRY
    
    func getCountry() {
        EatAndRun_DELEGATE.showActivity(myView: self.view, myTitle: "Loading..".localized(lang: LANGUAGE!))
        if !(REACHIBILITY.statusNetworkCheck())
        {
            EatAndRun_DELEGATE.removeActivity(myView: self.view)
            EatAndRun_DELEGATE.showAlert(title: "Sorry", message: "No network available", buttonTitle: "ok")
        }
        else
        {
            let params = ["LanguageId": LANGUAGE!,"WebnaSecurityKey": "W3BN@53CUR6ITY8"]
            print(params)
            GET_COUNTRY_MANAGER.getCountryDetailsFromServer(url: BASE_URL + "General/GetAllCountry", withParameters: params as NSDictionary, completion: { (result) -> Void in
                EatAndRun_DELEGATE.removeActivity(myView: self.view)
                self.countryArray = GET_COUNTRY_MANAGER.countryArray
                print(self.countryArray!)
                self.selectCountryTableView.reloadData()
            })
        }
    }
    
    // MARK:  POST REGISTRATION
    
    func postRegistration() {
         EatAndRun_DELEGATE.showActivity(myView: self.view, myTitle: "Loading..".localized(lang: LANGUAGE!))
        if !(REACHIBILITY.statusNetworkCheck())
        {
            EatAndRun_DELEGATE.removeActivity(myView: self.view)
            EatAndRun_DELEGATE.showAlert(title: "Sorry", message: "No network available", buttonTitle: "ok")
        }
        else
        {
            let params = ["Res_Username": self.registrationArray[0], "Res_Password": self.registrationArray[1], "RestaurantName": self.registrationArray[2], "Res_CompanyName": self.registrationArray[3], "Res_PersonInCharge": self.registrationArray[4], "Res_Email": self.registrationArray[5], "CountryId": self.registrationArray[6], "Res_Mobile": self.registrationArray[7], "Res_Telephone": self.registrationArray[8],  "Res_Address": self.registrationArray[9], "CommissionPerOrder": self.registrationArray[10], "WebnaSecurityKey":"W3BN@53CUR6ITY8", "LanguageId": LANGUAGE!]
            print(params)
            RESTAURANT_REGISTRATION_MANAGER.fetchRestaurantRegistrationDetailsFromServer(url: BASE_URL + "General/AddCustomer", withParameters: params as NSDictionary, completion: { (result) -> Void in
                EatAndRun_DELEGATE.removeActivity(myView: self.view)
                self.restaurantID = RESTAURANT_REGISTRATION_MANAGER.restaurantID
                self.message = RESTAURANT_REGISTRATION_MANAGER.message
                self.success = RESTAURANT_REGISTRATION_MANAGER.success
                if self.success == "Success"{
                    //self.performSegue(withIdentifier: "HomePage", sender: self)
                    EatAndRun_DELEGATE.showAlert(title: "", message: self.message!, buttonTitle: "ok")
                } else {
                    EatAndRun_DELEGATE.showAlert(title: "", message: self.message!, buttonTitle: "ok")
                }
            })
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
