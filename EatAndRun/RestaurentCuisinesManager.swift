//
//  RestaurentCuisinesManager.swift
//  EatAndRun
//
//  Created by Ronish on 5/5/17.
//  Copyright © 2017 Mawaqaa. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
import AlamofireObjectMapper

class RestaurentCuisinesManager: NSObject {
    var restaurentCuisinesList: [RestaurentCuisinesList]?
    var message: String?
    var success: String?
    
    var manager = Alamofire.SessionManager ()
    
    func fetchRestaurentCuisinesDetailsFromServer(url: String, withParameters: NSDictionary, completion: @escaping ( Bool) -> Void)
    {
        let configuration = URLSessionConfiguration.default
        self.manager = Alamofire.SessionManager(configuration: configuration)
        manager.request(url, method: .post, parameters: withParameters as? Parameters, encoding: JSONEncoding.default).responseObject (keyPath: "") { (response: DataResponse<RestaurentCuisinesMapper>) in
            
            let responsedata = response.result.value
            //print(responsedata?.restaurentMenuList!)
            
            if(responsedata?.restaurentCuisinesList != nil)
            {
                self.restaurentCuisinesList = (responsedata?.restaurentCuisinesList)!
                for list in self.restaurentCuisinesList!
                {
                    if list.cuisineId == nil{
                        list.cuisineId = 0
                    }
                    if list.cuisineName == nil{
                        list.cuisineName = ""
                    }
                    if list.cuisineDescription == nil{
                        list.cuisineDescription = ""
                    }
                    if list.cuisineImage == nil{
                        list.cuisineImage = ""
                    }
                    if list.isActive == nil{
                        list.isActive = false
                    }
                    if list.cuisineItemCount == nil{
                        list.cuisineItemCount = 0
                    }
                }
            }else {
                self.restaurentCuisinesList = []
            }
            
            if(responsedata?.message != nil)
            {
                self.message = (responsedata?.message)!
            }
            
            if(responsedata?.success != nil)
            {
                self.success = (responsedata?.success)!
            }
            
            completion(true)
        }
    }
}

class RestaurentCuisinesMapper: Mappable {
    var restaurentCuisinesList: [RestaurentCuisinesList]?
    var message: String?
    var success: String?
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    func mapping(map: Map)
    {
        restaurentCuisinesList <- map["cuisineList"]
        message <- map["Message"]
        success <- map["Success"]
    }
}

class RestaurentCuisinesList: Mappable {
    var cuisineId: Int?
    var cuisineName: String?
    var cuisineDescription: String?
    var cuisineImage: String?
    var isActive: Bool?
    var cuisineItemCount: Int?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        cuisineId <- map["CuisineId"]
        cuisineName <- map["CuisineName"]
        cuisineDescription <- map["CuisineDescription"]
        cuisineImage <- map["CuisineImage"]
        isActive <- map["IsActive"]
        cuisineItemCount <- map["MenuItemCount"]
    }
}
