//
//  RestaurentOpenBillDetailsManager.swift
//  EatAndRun
//
//  Created by Ronish on 5/11/17.
//  Copyright © 2017 Mawaqaa. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
import AlamofireObjectMapper

class RestaurentOpenBillDetailsManager: NSObject {
    var openOrderNumber: String?
    var customerID: Int?
    var resturantID: Int?
    var resturantName: String?
    var waiterID: String?
    var waiterName: String?
    var numberOfCustomers: Int?
    var tableNumber: String?
    var totalAmount: Int?
    var message: String?
    var success: String?
    var restaurentMenuItemList: [RestaurentMenuItemList]?
    
    var manager = Alamofire.SessionManager ()
    
    func fetchRestaurentOpenBillDetailsFromServer(url: String, withParameters: NSDictionary, completion: @escaping ( Bool) -> Void)
    {
        let configuration = URLSessionConfiguration.default
        self.manager = Alamofire.SessionManager(configuration: configuration)
        manager.request(url, method: .post, parameters: withParameters as? Parameters, encoding: JSONEncoding.default).responseObject (keyPath: "") { (response: DataResponse<RestaurentOpenBillDetailsMapper>) in
            
            let responsedata = response.result.value
            //print(responsedata?.totalAmount)
            
            if(responsedata?.openOrderNumber != nil)
            {
                self.openOrderNumber = (responsedata?.openOrderNumber)!
            }
            
            if(responsedata?.customerID != nil)
            {
                self.customerID = (responsedata?.customerID)!
            }
            
            if(responsedata?.resturantID != nil)
            {
                self.resturantID = (responsedata?.resturantID)!
            }
            
            if(responsedata?.resturantName != nil)
            {
                self.resturantName = (responsedata?.resturantName)!
            }
            
            if(responsedata?.waiterID != nil)
            {
                self.waiterID = (responsedata?.waiterID)!
            }
            
            if(responsedata?.waiterName != nil)
            {
                self.waiterName = (responsedata?.waiterName)!
            }
            
            if(responsedata?.numberOfCustomers != nil)
            {
                self.numberOfCustomers = (responsedata?.numberOfCustomers)!
            }
            
            if(responsedata?.tableNumber != nil)
            {
                self.tableNumber = (responsedata?.tableNumber)!
            }
            
            if(responsedata?.resturantName != nil)
            {
                self.resturantName = (responsedata?.resturantName)!
            }
            
            if(responsedata?.totalAmount != nil)
            {
                self.totalAmount = (responsedata?.totalAmount)!
            }
            
            if(responsedata?.message != nil)
            {
                self.message = (responsedata?.message)!
            }
            
            if(responsedata?.success != nil)
            {
                self.success = (responsedata?.success)!
            }
            
            if(responsedata?.restaurentMenuItemList != nil)
            {
                self.restaurentMenuItemList = (responsedata?.restaurentMenuItemList)!
                for list in self.restaurentMenuItemList!
                {
                    if list.detailId == nil{
                        list.detailId = 0
                    }
                    if list.orderId == nil{
                        list.orderId = 0
                    }
                    if list.menuItemId == nil{
                        list.menuItemId = 0
                    }
                    if list.menuItemName == nil{
                        list.menuItemName = ""
                    }
                    if list.menuItemImage == nil{
                        list.menuItemImage = ""
                    }
                    if list.quantity == nil{
                        list.quantity = 0
                    }
                    if list.price == nil{
                        list.price = 0
                    }
                    if list.total == nil{
                        list.total = 0
                    }
                }
            }else {
                self.restaurentMenuItemList = []
            }
            
            completion(true)
        }
    }
}

class RestaurentOpenBillDetailsMapper: Mappable {
    
    var openOrderNumber: String?
    var customerID: Int?
    var resturantID: Int?
    var resturantName: String?
    var waiterID: String?
    var waiterName: String?
    var numberOfCustomers: Int?
    var tableNumber: String?
    var totalAmount: Int?
    var message: String?
    var success: String?
    var restaurentMenuItemList: [RestaurentMenuItemList]?
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    func mapping(map: Map)
    {
        openOrderNumber <- map["OpenOrderNumber"]
        customerID <- map["Cus_Id"]
        resturantID <- map["Res_Id"]
        resturantName <- map["RestaurantName"]
        waiterID <- map["WaiterId"]
        waiterName <- map["WaiterName"]
        numberOfCustomers <- map["NoOfCustomers"]
        tableNumber <- map["TableNo"]
        totalAmount <- map["TotalAmount"]
        message <- map["Message"]
        success <- map["Success"]
        restaurentMenuItemList <- map["apimainorderitemlist"]
    }
}

class RestaurentMenuItemList: Mappable {
    var detailId: Int?
    var orderId: Int?
    var menuItemId: Int?
    var menuItemName: String?
    var menuItemImage: String?
    var quantity: Int?
    var price: Float?
    var total: Int?
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        detailId <- map["DetailId"]
        orderId <- map["OrderId"]
        menuItemId <- map["MenuItemId"]
        menuItemName <- map["MenuItemName"]
        menuItemImage <- map["MenuItemImage"]
        quantity <- map["Quantity"]
        price <- map["Price"]
        total <- map["Total"]
    }
}
