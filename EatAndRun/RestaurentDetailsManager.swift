//
//  RestaurentDetailsManager.swift
//  EatAndRun
//
//  Created by Ronish on 5/4/17.
//  Copyright © 2017 Mawaqaa. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
import AlamofireObjectMapper

class RestaurentDetailsManager: NSObject {
    var resturantID: Int?
    var customerID: Int?
    var resturantName: String?
    var resturantEmail: String?
    var resturantMobile: String?
    var resturantTelephone: String?
    var resturantAddress: String?
    var resturantLattitude: String?
    var resturantLongitude: String?
    var distance: String?
    var ratingStarCount: Int?
    var ratingViewCount: Int?
    var resturantLogo: String?
    var city: String?
    var workingStatus: String?
    var openTime: String?
    var closeTime: String?
    var message: String?
    var success: String?
    
    var manager = Alamofire.SessionManager ()
    
    func fetchRestaurentDetailsFromServer(url: String, withParameters: NSDictionary, completion: @escaping ( Bool) -> Void)
    {
        let configuration = URLSessionConfiguration.default
        self.manager = Alamofire.SessionManager(configuration: configuration)
        manager.request(url, method: .post, parameters: withParameters as? Parameters, encoding: JSONEncoding.default).responseObject (keyPath: "") { (response: DataResponse<RestaurentDetailsMapper>) in
            
            let responsedata = response.result.value
            // print(responsedata?.restaurentList!)
            
            if(responsedata?.resturantID != nil)
            {
                self.resturantID = (responsedata?.resturantID)!
            }
            
            if(responsedata?.customerID != nil)
            {
                self.customerID = (responsedata?.customerID)!
            }
            
            if(responsedata?.resturantName != nil)
            {
                self.resturantName = (responsedata?.resturantName)!
            }
            
            if(responsedata?.resturantEmail != nil)
            {
                self.resturantEmail = (responsedata?.resturantEmail)!
            }
            
            if(responsedata?.resturantMobile != nil)
            {
                self.resturantMobile = (responsedata?.resturantMobile)!
            }
            
            if(responsedata?.resturantTelephone != nil)
            {
                self.resturantTelephone = (responsedata?.resturantTelephone)!
            }
            
            if(responsedata?.resturantAddress != nil)
            {
                self.resturantAddress = (responsedata?.resturantAddress)!
            }
            
            if(responsedata?.resturantLattitude != nil)
            {
                self.resturantLattitude = (responsedata?.resturantLattitude)!
            }
            
            if(responsedata?.resturantLongitude != nil)
            {
                self.resturantLongitude = (responsedata?.resturantLongitude)!
            }
            
            if(responsedata?.distance != nil)
            {
                self.distance = (responsedata?.distance)!
            }
            
            if(responsedata?.ratingStarCount != nil)
            {
                self.ratingStarCount = (responsedata?.ratingStarCount)!
            }
            
            if(responsedata?.ratingViewCount != nil)
            {
                self.ratingViewCount = (responsedata?.ratingViewCount)!
            }
            
            if(responsedata?.resturantLogo != nil)
            {
                self.resturantLogo = (responsedata?.resturantLogo)!
            }
            
            if(responsedata?.city != nil)
            {
                self.city = (responsedata?.city)!
            }
            
            if(responsedata?.workingStatus != nil)
            {
                self.workingStatus = (responsedata?.workingStatus)!
            }
            
            if(responsedata?.openTime != nil)
            {
                self.openTime = (responsedata?.openTime)!
            }
            
            if(responsedata?.closeTime != nil)
            {
                self.closeTime = (responsedata?.closeTime)!
            }
            
            if(responsedata?.message != nil)
            {
                self.message = (responsedata?.message)!
            }
            
            if(responsedata?.success != nil)
            {
                self.success = (responsedata?.success)!
            }
            
            completion(true)
        }
    }
}

class RestaurentDetailsMapper: Mappable {
    
    var resturantID: Int?
    var customerID: Int?
    var resturantName: String?
    var resturantEmail: String?
    var resturantMobile: String?
    var resturantTelephone: String?
    var resturantAddress: String?
    var resturantLattitude: String?
    var resturantLongitude: String?
    var distance: String?
    var ratingStarCount: Int?
    var ratingViewCount: Int?
    var resturantLogo: String?
    var city: String?
    var workingStatus: String?
    var openTime: String?
    var closeTime: String?
    var message: String?
    var success: String?
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    func mapping(map: Map)
    {
        resturantID <- map["Res_Id"]
        customerID <- map["Cus_Id"]
        resturantName <- map["RestaurantName"]
        resturantEmail <- map["Res_Email"]
        resturantMobile <- map["Res_Mobile"]
        resturantTelephone <- map["Res_Telephone"]
        resturantAddress <- map["Res_Address"]
        resturantLattitude <- map["Res_Latitude"]
        resturantLongitude <- map["Res_Longitude"]
        distance <- map["Distance"]
        ratingStarCount <- map["RatingStarCount"]
        ratingViewCount <- map["RatingViewCount"]
        resturantLogo <- map["RestaurantLogo"]
        city <- map["CityName"]
        workingStatus <- map["Res_WorkingStatus"]
        openTime <- map["Res_OpenTime"]
        closeTime <- map["Res_CloseTime"]
        message <- map["Message"]
        success <- map["Success"]
    }
}
