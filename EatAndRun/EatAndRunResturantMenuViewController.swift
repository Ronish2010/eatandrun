//
//  EatAndRunResturantMenuViewController.swift
//  EatAndRun
//
//  Created by Ronish on 3/17/17.
//  Copyright © 2017 Mawaqaa. All rights reserved.
//

import UIKit
import SDWebImage

class EatAndRunResturantMenuViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var restuarentMenuTable: UITableView!
    @IBOutlet weak var searchBackView: UIView!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var searchTextView: UITextField!
    @IBOutlet weak var headingLabel: UILabel!
    
    var restaurantID = ""
    var menuSectionID = ""
    var heading = ""
    var isMenu = Bool()
    var restaurentMenuList: [RestaurentMenuList]?
    var restaurentCuisinesList: [RestaurentCuisinesList]?
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
        self.restuarentMenuTable.reloadData()
    }
    
    override func viewDidLayoutSubviews() {
        self.searchBackView.layer.cornerRadius = self.searchBackView.frame.size.height/2
        self.searchBackView.clipsToBounds = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if isMenu{
            self.headingLabel.text = "Category list"
            self.getRestaurentMenuDetails()
        } else {
            self.headingLabel.text = "Cuisine list"
            self.getRestaurentCuisineList()
        }
        
        // Do any additional setup after loading the view.
    }
    
    // MARK: - TABLEVIEW DELEGATES
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isMenu{
            if restaurentMenuList != nil {
                if (restaurentMenuList?.count)! > 0 {
                    return restaurentMenuList!.count
                }else {
                    return 0
                }
            }else {
                return 0
            }
        } else {
            if restaurentCuisinesList != nil {
                if (restaurentCuisinesList?.count)! > 0 {
                    return restaurentCuisinesList!.count
                }else {
                    return 0
                }
            }else {
                return 0
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.restuarentMenuTable.dequeueReusableCell(withIdentifier: "RestaurentMenuCell", for: indexPath) as! RestaurentMenuListCell
        if isMenu{
            cell.menuItemImage.sd_setImage(with: URL(string: "http://homepluskw.com/Renderers/ShowMedia.ashx?id=MediaArchive:0b0bdc54-64a1-4f69-ad30-c37767269c9f"), placeholderImage: nil)
            cell.typeLbl.text = self.restaurentMenuList?[indexPath.row].menuSectionName!
            cell.countLbl.text = String(describing: self.restaurentMenuList![indexPath.row].menuSectionCount!)
        } else {
            cell.menuItemImage.sd_setImage(with: URL(string: "http://homepluskw.com/Renderers/ShowMedia.ashx?id=MediaArchive:0b0bdc54-64a1-4f69-ad30-c37767269c9f"), placeholderImage: nil)
            cell.typeLbl.text = self.restaurentCuisinesList?[indexPath.row].cuisineName!
            cell.countLbl.text = String(describing: self.restaurentCuisinesList![indexPath.row].cuisineItemCount!)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.restuarentMenuTable.frame.size.width * 0.458715596330275
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //self.restuarentListTableView.reloadData()
        if isMenu{
            self.menuSectionID = String(describing: self.restaurentMenuList![indexPath.row].menuSectionId!)
            self.heading = String(describing: self.restaurentMenuList![indexPath.row].menuSectionName!)
        } else {
            self.menuSectionID = String(describing: self.restaurentCuisinesList![indexPath.row].cuisineId!)
            self.heading = String(describing: self.restaurentCuisinesList![indexPath.row].cuisineName!)
        }
        performSegue(withIdentifier: "MenuCategoryItems", sender: self)
    }
    
    // MARK: - Web Services
    
    // MARK:  GET MENU DETAILS
    
    func getRestaurentMenuDetails() {
        EatAndRun_DELEGATE.showActivity(myView: self.view, myTitle: "Loading..".localized(lang: LANGUAGE!))
        if !(REACHIBILITY.statusNetworkCheck())
        {
            EatAndRun_DELEGATE.removeActivity(myView: self.view)
            EatAndRun_DELEGATE.showAlert(title: "Sorry", message: "No network available", buttonTitle: "ok")
        }
        else
        {
            let params = ["Cus_Id": EatAndRun_DEFAULTS .value(forKey: "CUSTOMER_ID")!, "LanguageId": LANGUAGE!,"WebnaSecurityKey": "W3BN@53CUR6ITY8", "Res_Id": self.restaurantID]
            print(params)
            RESTAURENT_MENU_MANAGER.fetchRestaurentMenuDetailsFromServer(url: BASE_URL + "Restaurant/GetRestaurantMenus", withParameters: params as NSDictionary, completion: { (result) -> Void in
                EatAndRun_DELEGATE.removeActivity(myView: self.view)
                self.restaurentMenuList = RESTAURENT_MENU_MANAGER.restaurentMenuList!
                self.restuarentMenuTable.reloadData()
                self.viewWillAppear(false)
            })
        }
    }
    
    // MARK:  GET CUISINE DETAILS
    
    func getRestaurentCuisineList() {
        EatAndRun_DELEGATE.showActivity(myView: self.view, myTitle: "Loading..".localized(lang: LANGUAGE!))
        if !(REACHIBILITY.statusNetworkCheck())
        {
            EatAndRun_DELEGATE.removeActivity(myView: self.view)
            EatAndRun_DELEGATE.showAlert(title: "Sorry", message: "No network available", buttonTitle: "ok")
        }
        else
        {
            let params = ["Cus_Id": EatAndRun_DEFAULTS .value(forKey: "CUSTOMER_ID")!, "LanguageId": LANGUAGE!,"WebnaSecurityKey": "W3BN@53CUR6ITY8", "Res_Id": self.restaurantID]
            print(params)
            RESTAURENT_CUISINES_MANAGER.fetchRestaurentCuisinesDetailsFromServer(url: BASE_URL + "Restaurant/GetRestaurantCuisines", withParameters: params as NSDictionary, completion: { (result) -> Void in
                EatAndRun_DELEGATE.removeActivity(myView: self.view)
                self.restaurentCuisinesList = RESTAURENT_CUISINES_MANAGER.restaurentCuisinesList!
                self.restuarentMenuTable.reloadData()
            })
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "MenuCategoryItems"{
            if isMenu{
                let restaurentMenuCategoryListViewController = segue.destination as! EatAndRunMenuCategoryListViewController
                restaurentMenuCategoryListViewController.restaurantID = self.restaurantID
                restaurentMenuCategoryListViewController.menuSectionID = self.menuSectionID
                restaurentMenuCategoryListViewController.heading = self.heading
                restaurentMenuCategoryListViewController.isMenu = true
            } else {
                let restaurentMenuCategoryListViewController = segue.destination as! EatAndRunMenuCategoryListViewController
                restaurentMenuCategoryListViewController.restaurantID = self.restaurantID
                restaurentMenuCategoryListViewController.menuSectionID = self.menuSectionID
                restaurentMenuCategoryListViewController.heading = self.heading
                restaurentMenuCategoryListViewController.isMenu = false
            }
        }
    }

}

class RestaurentMenuListCell: UITableViewCell {
    @IBOutlet weak var menuItemImage: UIImageView!
    @IBOutlet weak var typeLbl: UILabel!
    @IBOutlet weak var countLbl: UILabel!
}
