//
//  EatAndRunOpenBillInitialViewController.swift
//  EatAndRun
//
//  Created by Ronish on 5/10/17.
//  Copyright © 2017 Mawaqaa. All rights reserved.
//

import UIKit

class EatAndRunOpenBillInitialViewController: UIViewController {

    @IBOutlet weak var orderNumberLabel: UILabel!
    @IBOutlet weak var restaurantNameLabel: UILabel!
    @IBOutlet weak var waiterNameLabel: UILabel!
    @IBOutlet weak var waiterIDLabel: UILabel!
    @IBOutlet weak var numberOfCustomersLabel: UILabel!
    @IBOutlet weak var tableNumberLabel: UILabel!
    @IBOutlet weak var proceedButton: UIButton!
    
    
    var restaurantID = ""
    var openOrderNumber = ""
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
    }
    
    override func viewDidLayoutSubviews() {
        //self.searchBackView.layer.cornerRadius = self.searchBackView.frame.size.height/2
        //self.searchBackView.clipsToBounds = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getOpenBillDetails()
        // Do any additional setup after loading the view.
    }
    
    // MARK: - Web Services
    
    // MARK:  GET OPEN BILL DETAILS
    
    func getOpenBillDetails() {
        EatAndRun_DELEGATE.showActivity(myView: self.view, myTitle: "Loading..".localized(lang: LANGUAGE!))
        if !(REACHIBILITY.statusNetworkCheck())
        {
            EatAndRun_DELEGATE.removeActivity(myView: self.view)
            EatAndRun_DELEGATE.showAlert(title: "Sorry", message: "No network available", buttonTitle: "ok")
        }
        else
        {
            let params = ["Cus_Id": EatAndRun_DEFAULTS .value(forKey: "CUSTOMER_ID")!, "LanguageId": LANGUAGE!,"WebnaSecurityKey": "W3BN@53CUR6ITY8", "Res_Id": self.restaurantID]
            print(params)
            RESTAURENT_OPEN_BILL_MANAGER.fetchRestaurentOpenBillFromServer(url: BASE_URL + "OpenBill/GetOpenBill", withParameters: params as NSDictionary, completion: { (result) -> Void in
                EatAndRun_DELEGATE.removeActivity(myView: self.view)
                self.openOrderNumber = RESTAURENT_OPEN_BILL_MANAGER.openOrderNumber!
                self.orderNumberLabel.text = RESTAURENT_OPEN_BILL_MANAGER.openOrderNumber
                self.restaurantNameLabel.text = RESTAURENT_OPEN_BILL_MANAGER.resturantName
            })
        }
    }
    
    // MARK:  GET OPEN BILL PROCEED DETAILS
    
    func getOpenBillProceedDetails() {
        EatAndRun_DELEGATE.showActivity(myView: self.view, myTitle: "Loading..".localized(lang: LANGUAGE!))
        if !(REACHIBILITY.statusNetworkCheck())
        {
            EatAndRun_DELEGATE.removeActivity(myView: self.view)
            EatAndRun_DELEGATE.showAlert(title: "Sorry", message: "No network available", buttonTitle: "ok")
        }
        else
        {
            let params = ["Cus_Id": EatAndRun_DEFAULTS .value(forKey: "CUSTOMER_ID")!, "LanguageId": LANGUAGE!,"WebnaSecurityKey": "W3BN@53CUR6ITY8", "Res_Id": self.restaurantID, "OpenOrderNumber":"A4113"]
            print(params)
            RESTAURENT_OPEN_BILL_PROCEED_MANAGER.fetchRestaurentOpenBillProceedFromServer(url: BASE_URL + "OpenBill/GetWaiterConfirmationStatus", withParameters: params as NSDictionary, completion: { (result) -> Void in
                EatAndRun_DELEGATE.removeActivity(myView: self.view)
                if RESTAURENT_OPEN_BILL_PROCEED_MANAGER.success == "Success"{
                    self.performSegue(withIdentifier: "OpenBillDetailPage", sender: self)
                } else {
                    EatAndRun_DELEGATE.showAlert(title: "", message: RESTAURENT_OPEN_BILL_PROCEED_MANAGER.message!, buttonTitle: "ok")
                }
            })
        }
    }
    
    // MARK: - BUTTON ACTIONS
    
    @IBAction func proceedButtonAction(_ sender: UIButton) {
        getOpenBillProceedDetails()
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "OpenBillDetailPage"{
            let openBillDetailViewController = segue.destination as! EatAndRunOpenBillDetailViewController
            openBillDetailViewController.restaurantID = self.restaurantID
            openBillDetailViewController.openOrderNumber = self.openOrderNumber
        }
    }
}
