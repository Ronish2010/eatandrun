//
//  EatAndRunOfferDetailsViewController.swift
//  EatAndRun
//
//  Created by Ronish on 3/22/17.
//  Copyright © 2017 Mawaqaa. All rights reserved.
//

import UIKit

class EatAndRunOfferDetailsViewController: UIViewController {
    @IBOutlet weak var searchBackView: UIView!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var searchTextView: UITextField!
    @IBOutlet weak var seperatorLabel: UILabel!
    @IBOutlet weak var itemImage: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var descriptionText: UITextView!
    @IBOutlet weak var oldPriceLbl: UILabel!
    
    var menuItemId = ""
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
    }
    
    override func viewDidLayoutSubviews() {
        self.searchBackView.layer.cornerRadius = self.searchBackView.frame.size.height/2
        self.searchBackView.clipsToBounds = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.seperatorLabel.lineBreakMode = .byClipping
        self.getRestaurentOfferItemDetails()
        // Do any additional setup after loading the view.
    }
    
    // MARK: - Web Services
    
    // MARK:  GET OFFER ITEM DETAILS
    
    func getRestaurentOfferItemDetails() {
        EatAndRun_DELEGATE.showActivity(myView: self.view, myTitle: "Loading..".localized(lang: LANGUAGE!))
        if !(REACHIBILITY.statusNetworkCheck())
        {
            EatAndRun_DELEGATE.removeActivity(myView: self.view)
            EatAndRun_DELEGATE.showAlert(title: "Sorry", message: "No network available", buttonTitle: "ok")
        }
        else
        {
            let params = ["Cus_Id": EatAndRun_DEFAULTS .value(forKey: "CUSTOMER_ID")!, "LanguageId": LANGUAGE!,"WebnaSecurityKey": "W3BN@53CUR6ITY8", "MenuItemId": self.menuItemId]
            print(params)
            RESTAURENT_OFFER_ITEM_MANAGER.fetchRestaurentOfferItemDetailsFromServer(url: BASE_URL + "Restaurant/GetRestaurantOffersDetailsByMenuitemId", withParameters: params as NSDictionary, completion: { (result) -> Void in
                EatAndRun_DELEGATE.removeActivity(myView: self.view)
                self.itemImage.sd_setImage(with: URL(string: "http://homepluskw.com/Renderers/ShowMedia.ashx?id=MediaArchive:0b0bdc54-64a1-4f69-ad30-c37767269c9f"), placeholderImage: nil)
                self.titleLbl.text = RESTAURENT_OFFER_ITEM_MANAGER.menuItemName!
                self.priceLbl.text = String(RESTAURENT_OFFER_ITEM_MANAGER.menuItemPrice!)
                self.oldPriceLbl.text = String(RESTAURENT_OFFER_ITEM_MANAGER.discountPrice!)
                self.descriptionText.text = RESTAURENT_OFFER_ITEM_MANAGER.menuItemDescription!
            })
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
