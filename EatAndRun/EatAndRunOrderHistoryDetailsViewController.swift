//
//  EatAndRunOrderHistoryDetailsViewController.swift
//  EatAndRun
//
//  Created by Ronish on 5/15/17.
//  Copyright © 2017 Mawaqaa. All rights reserved.
//

import UIKit

class EatAndRunOrderHistoryDetailsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var orderIdValue: UILabel!
    @IBOutlet weak var orderHistoryDetailsTable: UITableView!
    
    var restaurantID = ""
    var orderID = ""
    var orderItemList: [RestaurentOrderItemDetailsMapper]?
    var subOrderItemList: [RestaurentOrderItemDetailsMapper]?
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
        self.orderHistoryDetailsTable.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.orderIdValue.text = self.orderID
        getOrderHistoryDetails()
        // Do any additional setup after loading the view.
    }
    
    // MARK: - TABLEVIEW DELEGATES
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if orderItemList != nil {
            if (orderItemList?.count)! > 0 {
                return (orderItemList!.count + 8)
            }else {
                return 0
            }
        }else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var identifier = ""
        if indexPath.row == 0 {
            identifier = "TopSpace"
        } else if ((indexPath.row > 0) && (indexPath.row < 6) ){
            identifier = "OrderDetails"
        } else if (indexPath.row == 6){
            identifier = "MenuOrderedButton"
        } else if ((indexPath.row > 6) && (indexPath.row < ((self.orderItemList?.count)! + 7)) ){
            identifier = "MenuDetails"
        } else {
            identifier = "TotalAmount"
        }
        let cell = self.orderHistoryDetailsTable.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! OrderHistoryDetailCell
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return self.orderHistoryDetailsTable.frame.size.width * 0.023188405797101
        } else if ((indexPath.row > 0) && (indexPath.row < 6) ){
            return self.orderHistoryDetailsTable.frame.size.width * 0.115942028985507
        } else if (indexPath.row == 6){
            return self.orderHistoryDetailsTable.frame.size.width * 0.231884057971014
        } else if ((indexPath.row > 6) && (indexPath.row < (((self.orderItemList?.count)! + 7) - 1)) ){
            return self.orderHistoryDetailsTable.frame.size.width * 0.159420289855072
        } else if (indexPath.row == ((self.orderItemList?.count)! + 7)){
            return self.orderHistoryDetailsTable.frame.size.width * 0.202898550724638
        }else {
            return 50
        }
    }
 
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //self.restuarentMenuCategoryListTable.reloadData()
        //self.menuItemId = String(describing: self.orderHistoryList![indexPath.row].menuItemId!)
        //performSegue(withIdentifier: "OfferCategorydetails", sender: self)
    }
    
    // MARK: - Web Services
    
    // MARK:  GET ORDER HISTORY DETAILS
    
    func getOrderHistoryDetails() {
        EatAndRun_DELEGATE.showActivity(myView: self.view, myTitle: "Loading..".localized(lang: LANGUAGE!))
        if !(REACHIBILITY.statusNetworkCheck())
        {
            EatAndRun_DELEGATE.removeActivity(myView: self.view)
            EatAndRun_DELEGATE.showAlert(title: "Sorry", message: "No network available", buttonTitle: "ok")
        }
        else
        {
//            let params = ["Cus_Id": EatAndRun_DEFAULTS .value(forKey: "CUSTOMER_ID")!, "LanguageId": LANGUAGE!,"WebnaSecurityKey": "W3BN@53CUR6ITY8", "Res_Id": self.restaurantID, "OpenOrderNumber": self.orderID]
            
            let params = ["Cus_Id": "23", "LanguageId": LANGUAGE!,"WebnaSecurityKey": "W3BN@53CUR6ITY8", "Res_Id": "1", "OpenOrderNumber": self.orderID]
            print(params)
            RESTAURENT_ORDER_HISTORY_DETAILS_MANAGER.fetchRestaurentOrderHistoryDetailsFromServer(url: BASE_URL + "OpenBill/GetOpenBillDetails", withParameters: params as NSDictionary, completion: { (result) -> Void in
                EatAndRun_DELEGATE.removeActivity(myView: self.view)
                self.orderItemList = RESTAURENT_ORDER_HISTORY_DETAILS_MANAGER.orderItemList!
                self.orderHistoryDetailsTable.reloadData()
            })
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

class OrderHistoryDetailCell: UITableViewCell {
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var valueLbl: UILabel!
    @IBOutlet weak var menuOrderedButton: UIButton!
    @IBOutlet weak var menuImage: UIImageView!
    @IBOutlet weak var menuTitleLbl: UILabel!
    @IBOutlet weak var menuPriceLbl: UILabel!
    @IBOutlet weak var totalBillAmountLbl: UILabel!
    @IBOutlet weak var totalValueLbl: UILabel!
    
}
