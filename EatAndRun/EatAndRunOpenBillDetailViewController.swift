//
//  EatAndRunOpenBillDetailViewController.swift
//  EatAndRun
//
//  Created by Ronish on 5/11/17.
//  Copyright © 2017 Mawaqaa. All rights reserved.
//

import UIKit
import SDWebImage

class EatAndRunOpenBillDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var orderNumberLabel: UILabel!
    @IBOutlet weak var restaurantNameLabel: UILabel!
    @IBOutlet weak var waiterNameLabel: UILabel!
    @IBOutlet weak var waiterIDLabel: UILabel!
    @IBOutlet weak var numberOfCustomersLabel: UILabel!
    @IBOutlet weak var tableNumberLabel: UILabel!
    @IBOutlet weak var splitBillButton: UIButton!
    @IBOutlet weak var orderAcceptedButton: UIButton!
    @IBOutlet weak var menuTable: UITableView!
    @IBOutlet weak var totalValueLabel: UILabel!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var orderAcceptBackView: UIView!
    @IBOutlet weak var menuOrderBackView: NSLayoutConstraint!
    @IBOutlet weak var totalAmountBackView: UIView!
    
    var restaurantID = ""
    var openOrderNumber = ""
    var isItemAvailable = false
    var menuItemsList : [RestaurentMenuItemList]?
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
        self.mainScrollView.contentSize = CGSize(width: self.mainScrollView.frame.size.width, height: self.orderAcceptBackView.frame.origin.y + self.orderAcceptBackView.frame.size.height)
        self.menuTable.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getOpenBillDetails()
        // Do any additional setup after loading the view.
    }
    
    // MARK: - Tableview Delegate Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if menuItemsList != nil {
            if (menuItemsList?.count)! > 0 {
                return menuItemsList!.count
            }else {
                return 0
            }
        }else {
            return 0
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //var identifier = "NoItemCell"//MenuCell
        let cell = self.menuTable.dequeueReusableCell(withIdentifier: "MenuCell", for: indexPath as IndexPath) as! EatAndRunMenuListCell
        cell.menuItemImage.sd_setImage(with: URL(string: "http://homepluskw.com/Renderers/ShowMedia.ashx?id=MediaArchive:0b0bdc54-64a1-4f69-ad30-c37767269c9f"), placeholderImage: nil)
        cell.menuItemName.text = self.menuItemsList?[indexPath.row].menuItemName!
        cell.menuItemPrice.text = String(describing: self.menuItemsList![indexPath.row].price!)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tabBarController?.selectedIndex = 1
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.menuTable.frame.size.width * 0.159420289855072
    }
 
    
    // MARK: - Web Services
    
    // MARK:  GET OPEN BILL DETAILS
    
    func getOpenBillDetails() {
        EatAndRun_DELEGATE.showActivity(myView: self.view, myTitle: "Loading..".localized(lang: LANGUAGE!))
        if !(REACHIBILITY.statusNetworkCheck())
        {
            EatAndRun_DELEGATE.removeActivity(myView: self.view)
            EatAndRun_DELEGATE.showAlert(title: "Sorry", message: "No network available", buttonTitle: "ok")
        }
        else
        {
//            let params = ["Cus_Id": EatAndRun_DEFAULTS .value(forKey: "CUSTOMER_ID")!, "LanguageId": LANGUAGE!,"WebnaSecurityKey": "W3BN@53CUR6ITY8", "Res_Id": self.restaurantID, "OpenOrderNumber": self.openOrderNumber]
            let params = ["Cus_Id": "23", "LanguageId": LANGUAGE!,"WebnaSecurityKey": "W3BN@53CUR6ITY8", "Res_Id": "1", "OpenOrderNumber": "A4113"]
            print(params)
            RESTAURENT_OPEN_BILL_DETAILS_MANAGER.fetchRestaurentOpenBillDetailsFromServer(url: BASE_URL + "OpenBill/GetOpenBillDetails", withParameters: params as NSDictionary, completion: { (result) -> Void in
                EatAndRun_DELEGATE.removeActivity(myView: self.view)
                self.openOrderNumber = RESTAURENT_OPEN_BILL_DETAILS_MANAGER.openOrderNumber!
                self.orderNumberLabel.text = RESTAURENT_OPEN_BILL_DETAILS_MANAGER.openOrderNumber
                self.restaurantNameLabel.text = RESTAURENT_OPEN_BILL_DETAILS_MANAGER.resturantName
                self.waiterIDLabel.text = String(describing:RESTAURENT_OPEN_BILL_DETAILS_MANAGER.waiterID!)
                self.waiterNameLabel.text = RESTAURENT_OPEN_BILL_DETAILS_MANAGER.waiterName
                self.numberOfCustomersLabel.text = String(describing:RESTAURENT_OPEN_BILL_DETAILS_MANAGER.numberOfCustomers!)
                self.tableNumberLabel.text = String(describing:RESTAURENT_OPEN_BILL_DETAILS_MANAGER.tableNumber!)
                self.totalValueLabel.text = String(describing:RESTAURENT_OPEN_BILL_DETAILS_MANAGER.totalAmount!)
                self.menuItemsList = RESTAURENT_OPEN_BILL_DETAILS_MANAGER.restaurentMenuItemList
                self.menuTable.reloadData()
                self.tableViewHeight.constant = self.menuTable.contentSize.height
                self.menuOrderBackView.constant = self.menuTable.frame.origin.y + self.menuTable.contentSize.height + self.totalAmountBackView.frame.size.height
                self.viewWillAppear(false)
            })
        }
    }
    
    // MARK:  BUTTON ACTIONS
    
    @IBAction func splitBillButtonAction(_ sender: UIButton) {
        self.performSegue(withIdentifier: "SplitBill", sender: self)
    }
    
    @IBAction func orderAcceptButtonAction(_ sender: UIButton) {
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

class EatAndRunMenuListCell: UITableViewCell {
    @IBOutlet weak var menuItemImage: UIImageView!
    @IBOutlet weak var menuItemName: UILabel!
    @IBOutlet weak var noItemLabel: UILabel!
    @IBOutlet weak var menuItemPrice: UILabel!
}
