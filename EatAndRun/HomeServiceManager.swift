//
//  HomeServiceManager.swift
//  EatAndRun
//
//  Created by Ronish on 4/28/17.
//  Copyright © 2017 Mawaqaa. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
import AlamofireObjectMapper

class HomeServiceManager: NSObject {
    var customerID: Int?
    var registeredRestaurentCount: Int?
    var registeredCustomerCount: Int?
    var customerFirstName: String?
    var customerLastName: String?
    var customerEmail: String?
    var message: String?
    var success: String?
    var manager = Alamofire.SessionManager ()
    
    func fetchHomeDetailsFromServer(url: String, withParameters: NSDictionary, completion: @escaping ( Bool) -> Void)
    {
        let configuration = URLSessionConfiguration.default
        self.manager = Alamofire.SessionManager(configuration: configuration)
        manager.request(url, method: .post, parameters: withParameters as? Parameters, encoding: JSONEncoding.default).responseObject (keyPath: "") { (response: DataResponse<HomeObjectMapper>) in
            
            let responsedata = response.result.value
            print(responsedata!)
            if(responsedata?.customerID != nil)
            {
                self.customerID = (responsedata?.customerID)!
            }
            
            if(responsedata?.registeredRestaurentCount != nil)
            {
                self.registeredRestaurentCount = (responsedata?.registeredRestaurentCount)!
            }
            
            if(responsedata?.registeredCustomerCount != nil)
            {
                self.registeredCustomerCount = (responsedata?.registeredCustomerCount)!
            }
            
            if(responsedata?.customerFirstName != nil)
            {
                self.customerFirstName = (responsedata?.customerFirstName)!
            }
            
            if(responsedata?.customerLastName != nil)
            {
                self.customerLastName = (responsedata?.customerLastName)!
            }
            
            if(responsedata?.customerEmail != nil)
            {
                self.customerEmail = (responsedata?.customerEmail)!
            }else {
                self.customerEmail = ""
            }
            
            if(responsedata?.message != nil)
            {
                self.message = (responsedata?.message)!
            }
            
            if(responsedata?.success != nil)
            {
                self.success = (responsedata?.success)!
            }
            
            completion(true)
        }
    }
}

class HomeObjectMapper: Mappable {
    var customerID: Int?
    var registeredRestaurentCount: Int?
    var registeredCustomerCount: Int?
    var customerFirstName: String?
    var customerLastName: String?
    var customerEmail: String?
    var message: String?
    var success: String?
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    func mapping(map: Map)
    {
        customerID <- map["Cus_Id"]
        registeredRestaurentCount <- map["Registered_res_count"]
        registeredCustomerCount <- map["Registered_cus_count"]
        customerFirstName <- map["Cus_FirstName"]
        customerLastName <- map["Cus_LastName"]
        customerEmail <- map["Cus_Email"]
        message <- map["Message"]
        success <- map["Success"]
    }
}
