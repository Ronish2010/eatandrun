//
//  EatAndRunHomeViewController.swift
//  EatAndRun
//
//  Created by Ronish on 3/15/17.
//  Copyright © 2017 Mawaqaa. All rights reserved.
//

import UIKit

class EatAndRunHomeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var findResturantButton: UIButton!
    @IBOutlet weak var selectCountryButton: UIButton!
    @IBOutlet weak var selectAreaButton: UIButton!
    @IBOutlet weak var selectCountryDropDownView: UIView!
    @IBOutlet weak var selectAreaDropDownView: UIView!
    @IBOutlet weak var selectCountryTableView: UITableView!
    @IBOutlet weak var selectAreaTableView: UITableView!
    @IBOutlet weak var countryTableHeight: NSLayoutConstraint!
    @IBOutlet weak var areaTableHeight: NSLayoutConstraint!
    @IBOutlet weak var countryLabel: UILabel!
    @IBOutlet weak var areaLabel: UILabel!
    @IBOutlet weak var registeredResturantsCount: UILabel!
    @IBOutlet weak var registeredCustomerCount: UILabel!
    @IBOutlet weak var helloUserLabel: UILabel!
    
    var customerID: Int?
    var countryID = ""
    var areaID = ""
    var customerFirstName: String?
    var customerLastName: String?
    var message: String?
    var success: String?
    
    var dropDownCountryArray: NSArray!
    var dropDownAreaArray: NSArray!
    var is_CountryDropDownShow = false
    var is_AreaDropDownShow = false
    var countryArray: [CountryDetails]?
    var areaArray: [AreaDetails]?
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
        postDeviceRegistration()
    }
    
    override func viewDidLayoutSubviews() {
        self.findResturantButton.layer.cornerRadius = self.findResturantButton.frame.size.height/2
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        LOCATION_MANAGER.requestWhenInUseAuthorization()
        NAVIGATION_POINTER = self.navigationController!
        self.dropDownCountryArray = ["India","USA","Dubai","Oman"]
        self.dropDownAreaArray = ["India","USA","Dubai","Oman","India","USA","Dubai","Oman"]
        // Do any additional setup after loading the view.
    }
    
    @IBAction func selectCountryAndAreaButtonActn(_ sender: UIButton) {
        if sender.tag == 0 {
            showDropDownTable(buttonTag: 1)
        } else {
            showDropDownTable(buttonTag: 2)
        }
    }
    
    func showDropDownTable(buttonTag : Int) {
        
        if buttonTag == 0{
            UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseInOut, animations: { () -> Void in
                self.selectCountryDropDownView.frame.size.height = 0
                self.selectAreaDropDownView.frame.size.height = 0
                
            }) { (completed: Bool) -> Void in
                self.is_CountryDropDownShow = false
                self.is_AreaDropDownShow = false
                
            }
        } else if buttonTag == 1 {
            if self.countryArray != nil {
                if (self.countryArray?.count)! > 0 {
                    UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseInOut, animations: { () -> Void in
                        if self.is_CountryDropDownShow == false {
                            if (self.countryArray?.count)! < 4 {
                                self.selectCountryDropDownView.frame.size.height = CGFloat((self.countryArray?.count)! * 30)
                                self.countryTableHeight.constant = self.selectCountryDropDownView.frame.size.height
                            }else {
                                self.selectCountryDropDownView.frame.size.height = CGFloat(4 * 30)
                                self.countryTableHeight.constant = self.selectCountryDropDownView.frame.size.height
                            }
                            self.selectAreaDropDownView.frame.size.height = 0
                        }else {
                            self.selectCountryDropDownView.frame.size.height = 0
                        }
                    }) { (completed: Bool) -> Void in
                        self.view.setNeedsLayout()
                        self.view.layoutIfNeeded()
                        if self.is_CountryDropDownShow == false {
                            self.is_CountryDropDownShow = true
                            self.is_AreaDropDownShow = false
                        }else {
                            self.is_CountryDropDownShow = false
                        }
                    }
                }
            }
        } else if buttonTag == 2 {
            if self.dropDownAreaArray != nil {
                if self.dropDownAreaArray.count > 0 {
                    UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseInOut, animations: { () -> Void in
                        if self.is_AreaDropDownShow == false {
                            if self.dropDownAreaArray.count < 4 {
                                self.selectAreaDropDownView.frame.size.height = CGFloat(self.dropDownAreaArray.count * 30)
                                self.areaTableHeight.constant = self.selectAreaDropDownView.frame.size.height
                            }else {
                                self.selectAreaDropDownView.frame.size.height = CGFloat(4 * 30)
                                self.areaTableHeight.constant = self.selectAreaDropDownView.frame.size.height
                            }
                            self.selectCountryDropDownView.frame.size.height = 0
                        }else {
                            self.selectAreaDropDownView.frame.size.height = 0
                        }
                    }) { (completed: Bool) -> Void in
                        if self.is_AreaDropDownShow == false {
                            self.is_AreaDropDownShow = true
                            self.is_CountryDropDownShow = false
                        }else {
                            self.is_AreaDropDownShow = false
                        }
                    }
                }
            }
        }
    }
    
    // MARK: - Tableview Delegate Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.selectCountryTableView {
            if countryArray != nil {
                if (countryArray?.count)! > 0 {
                    return countryArray!.count
                }else {
                    return 0
                }
            }else {
                return 0
            }
        } else {
            if areaArray != nil {
                if (areaArray?.count)! > 0 {
                    return areaArray!.count
                }else {
                    return 0
                }
            }else {
                return 0
            }
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == self.selectCountryTableView {
            let cell = selectCountryTableView.dequeueReusableCell(withIdentifier: "DROPDOWN", for: indexPath as IndexPath) as! DropDownCell
            cell.valueLbl.text = self.countryArray?[indexPath.row].countryName
            return cell
        } else {
            let cell = selectAreaTableView.dequeueReusableCell(withIdentifier: "DROPDOWN", for: indexPath as IndexPath) as! DropDownCell
            cell.valueLbl.text = self.areaArray?[indexPath.row].areaName
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == self.selectCountryTableView {
            showDropDownTable(buttonTag: 0)
            self.countryLabel.text = self.countryArray?[indexPath.row].countryName
            self.countryID = String(describing: self.countryArray![indexPath.row].countryId!)
            getArea(countryId: String(describing: self.countryArray![indexPath.row].countryId!))
        } else if tableView == self.selectAreaTableView{
            showDropDownTable(buttonTag: 0)
            self.areaLabel.text = self.areaArray?[indexPath.row].areaName
            self.areaID = String(describing: self.areaArray![indexPath.row].areaId!)
        }
    }
    
    // MARK: - FIND RESTAURANT BUTTON ACTION

    @IBAction func findRestaurantButtonAction(_ sender: UIButton) {
        if self.countryLabel.text == "Select Country" {
            EatAndRun_DELEGATE.showAlert(title: "Sorry", message: "Please select a Country", buttonTitle: "ok")
        } else if self.areaLabel.text == "Select Area" {
            EatAndRun_DELEGATE.showAlert(title: "Sorry", message: "Please select an Area", buttonTitle: "ok")
        } else {
            self.countryLabel.text = "Select Country"
            self.areaLabel.text = "Select Area"
            performSegue(withIdentifier: "FindResturants", sender: self)
        }
    }
    
    // MARK: - Web Services
    
    // MARK: POST DEVICE REG
    
    func postDeviceRegistration() {
        EatAndRun_DELEGATE.showActivity(myView: self.view, myTitle: "Loading..".localized(lang: LANGUAGE!))
        if !(REACHIBILITY.statusNetworkCheck())
        {
            EatAndRun_DELEGATE.removeActivity(myView: self.view)
            EatAndRun_DELEGATE.showAlert(title: "Sorry", message: "No network available", buttonTitle: "ok")
        }
        else
        {
            var params : NSDictionary
            if DEVICE_TOKEN != nil {
                params = ["Cus_Id": EatAndRun_DEFAULTS .value(forKey: "CUSTOMER_ID")!, "Device_Id": DEVICE_TOKEN!, "Device_Token": DEVICE_TOKEN!, "Device_Platform": "2", "LanguageId": LANGUAGE!,"WebnaSecurityKey": "W3BN@53CUR6ITY8"]
            }else{
                params = ["Cus_Id": EatAndRun_DEFAULTS .value(forKey: "CUSTOMER_ID")!, "Device_Id": "", "Device_Token": "", "Device_Platform": "2", "LanguageId": LANGUAGE!,"WebnaSecurityKey": "W3BN@53CUR6ITY8"]
            }

            print(params)
            DEVICE_REGISTRATION_MANAGER.fetchDeviceRegistrationDetailsFromServer(url: BASE_URL + "MobileHome/MobileHome", withParameters: params as NSDictionary, completion: { (result) -> Void in
                EatAndRun_DELEGATE.removeActivity(myView: self.view)
                if DEVICE_REGISTRATION_MANAGER.success == "Success"{
                    self.getHomeDetails()
                }
            })
        }
    }
    
    // MARK:  GET HOME DETAILS
    func getHomeDetails() {
        EatAndRun_DELEGATE.showActivity(myView: self.view, myTitle: "Loading..".localized(lang: LANGUAGE!))
        if !(REACHIBILITY.statusNetworkCheck())
        {
            EatAndRun_DELEGATE.removeActivity(myView: self.view)
            EatAndRun_DELEGATE.showAlert(title: "Sorry", message: "No network available", buttonTitle: "ok")
        }
        else
        {
            let params = ["Cus_Id": EatAndRun_DEFAULTS .value(forKey: "CUSTOMER_ID")!, "LanguageId": LANGUAGE!,"WebnaSecurityKey": "W3BN@53CUR6ITY8"]
            print(params)
            HOME_SERVICE_MANAGER.fetchHomeDetailsFromServer(url: BASE_URL + "MobileHome/MobileHome", withParameters: params as NSDictionary, completion: { (result) -> Void in
                EatAndRun_DELEGATE.removeActivity(myView: self.view)
                self.registeredCustomerCount.text = String(describing: HOME_SERVICE_MANAGER.registeredCustomerCount!)
                self.registeredResturantsCount.text = String(describing: HOME_SERVICE_MANAGER.registeredRestaurentCount!)
                self.helloUserLabel.text = "Hello \(HOME_SERVICE_MANAGER.customerFirstName!)"
                if HOME_SERVICE_MANAGER.success == "Success"{
                    self.getCountry()
                }
            })
        }
    }
    
    // MARK:  GET COUNTRY
    func getCountry() {
        EatAndRun_DELEGATE.showActivity(myView: self.view, myTitle: "Loading..".localized(lang: LANGUAGE!))
        if !(REACHIBILITY.statusNetworkCheck())
        {
            EatAndRun_DELEGATE.removeActivity(myView: self.view)
            EatAndRun_DELEGATE.showAlert(title: "Sorry", message: "No network available", buttonTitle: "ok")
        }
        else
        {
            let params = ["LanguageId": LANGUAGE!,"WebnaSecurityKey": "W3BN@53CUR6ITY8"]
            GET_COUNTRY_MANAGER.getCountryDetailsFromServer(url: BASE_URL + "General/GetAllCountry", withParameters: params as NSDictionary, completion: { (result) -> Void in
                EatAndRun_DELEGATE.removeActivity(myView: self.view)
                self.countryArray = GET_COUNTRY_MANAGER.countryArray
                print(self.countryArray!)
                self.selectCountryTableView.reloadData()
            })
        }
    }
    
    // MARK:  GET AREA
    func getArea(countryId : String) {
        EatAndRun_DELEGATE.showActivity(myView: self.view, myTitle: "Loading..".localized(lang: LANGUAGE!))
        if !(REACHIBILITY.statusNetworkCheck())
        {
            EatAndRun_DELEGATE.removeActivity(myView: self.view)
            EatAndRun_DELEGATE.showAlert(title: "Sorry", message: "No network available", buttonTitle: "ok")
        }
        else
        {
            let params = ["CountryId": countryId, "LanguageId": LANGUAGE!,"WebnaSecurityKey": "W3BN@53CUR6ITY8"]
            GET_AREA_MANAGER.getAreaDetailsFromServer(url: BASE_URL + "General/GetAllAreaByCountry", withParameters: params as NSDictionary, completion: { (result) -> Void in
                EatAndRun_DELEGATE.removeActivity(myView: self.view)
                self.areaArray = GET_AREA_MANAGER.areaArray
                print(self.areaArray!)
                self.selectAreaTableView.reloadData()
            })
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "FindResturants"{
            let restaurentListViewController = segue.destination as! EatAndRunResturantListViewController
            restaurentListViewController.countryID = self.countryID
            restaurentListViewController.areaID = self.areaID
        }
    }

}

class DropDownCell: UITableViewCell {
    @IBOutlet weak var valueLbl: UILabel!
}
