//
//  EatAndRunOrderHistoryViewController.swift
//  EatAndRun
//
//  Created by Ronish on 5/15/17.
//  Copyright © 2017 Mawaqaa. All rights reserved.
//

import UIKit

class EatAndRunOrderHistoryViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var orderHistoryListTable: UITableView!
    
    var restaurantID = ""
    var orderID = ""
    var orderHistoryList: [RestaurentOrderHistoryListMapper]?
    var message: String?
    var success: String?
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
        self.orderHistoryListTable.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getOrderHistory()
        // Do any additional setup after loading the view.
    }
    
    // MARK: - TABLEVIEW DELEGATES
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if orderHistoryList != nil {
            if (orderHistoryList?.count)! > 0 {
                return orderHistoryList!.count
            }else {
                return 0
            }
        }else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.orderHistoryListTable.dequeueReusableCell(withIdentifier: "OrderHistoryListCell", for: indexPath) as! OrderHistoryListCell
        
        cell.orderIdValue.text = self.orderHistoryList?[indexPath.row].orderNumber
        cell.orderDateValue.text = String(describing: self.orderHistoryList![indexPath.row].orderDate!)
        cell.priceValue.text = String(describing: self.orderHistoryList![indexPath.row].totalAmount!)
        cell.statusValue.text = self.orderHistoryList![indexPath.row].orderStatus
        cell.viewDetailsButton.tag = indexPath.row
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.orderHistoryListTable.frame.size.width * 0.739130434782609
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //self.restuarentMenuCategoryListTable.reloadData()
        //self.menuItemId = String(describing: self.orderHistoryList![indexPath.row].menuItemId!)
        //performSegue(withIdentifier: "OfferCategorydetails", sender: self)
    }

    // MARK: - Web Services
    
    // MARK:  GET ORDER HISTORY DETAILS
    
    func getOrderHistory() {
        EatAndRun_DELEGATE.showActivity(myView: self.view, myTitle: "Loading..".localized(lang: LANGUAGE!))
        if !(REACHIBILITY.statusNetworkCheck())
        {
            EatAndRun_DELEGATE.removeActivity(myView: self.view)
            EatAndRun_DELEGATE.showAlert(title: "Sorry", message: "No network available", buttonTitle: "ok")
        }
        else
        {
            let params = ["Cus_Id": "23", "LanguageId": LANGUAGE!,"WebnaSecurityKey": "W3BN@53CUR6ITY8"]
//            let params = ["Cus_Id": EatAndRun_DEFAULTS .value(forKey: "CUSTOMER_ID")!, "LanguageId": LANGUAGE!,"WebnaSecurityKey": "W3BN@53CUR6ITY8"]
            print(params)
            RESTAURENT_ORDER_HISTORY_MANAGER.fetchRestaurentOrderHistoryFromServer(url: BASE_URL + "ApiOrders/GetOrdersHistory", withParameters: params as NSDictionary, completion: { (result) -> Void in
                EatAndRun_DELEGATE.removeActivity(myView: self.view)
                self.orderHistoryList = RESTAURENT_ORDER_HISTORY_MANAGER.orderHistoryList!
                self.orderHistoryListTable.reloadData()
            })
        }
    }
    // MARK: - Button Actions
    
    
    @IBAction func viewDetailsButtonAction(_ sender: UIButton) {
        performSegue(withIdentifier: "OrderHistoryDetails", sender: sender)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "OrderHistoryDetails"{
            print((sender as! UIButton).tag)
            self.orderID = (self.orderHistoryList?[(sender as! UIButton).tag].orderNumber)!
            self.restaurantID = String(describing:(self.orderHistoryList?[(sender as! UIButton).tag].res_Id)!)
            let restaurentOrderHistoryDetailsViewController = segue.destination as! EatAndRunOrderHistoryDetailsViewController
            restaurentOrderHistoryDetailsViewController.orderID = self.orderID
            restaurentOrderHistoryDetailsViewController.restaurantID = self.restaurantID
        }
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }

}

class OrderHistoryListCell: UITableViewCell {
    @IBOutlet weak var orderIdValue: UILabel!
    @IBOutlet weak var orderDateValue: UILabel!
    @IBOutlet weak var priceValue: UILabel!
    @IBOutlet weak var statusValue: UILabel!
    @IBOutlet weak var viewDetailsButton: UIButton!
}
