//
//  RestaurantRegistrationManager.swift
//  EatAndRun
//
//  Created by Ronish on 4/25/17.
//  Copyright © 2017 Mawaqaa. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
import AlamofireObjectMapper

class RestaurantRegistrationManager: NSObject {
    var restaurantID: Int?
    var message: String?
    var success: String?
    var manager = Alamofire.SessionManager ()
    
    func fetchRestaurantRegistrationDetailsFromServer(url: String, withParameters: NSDictionary, completion: @escaping ( Bool) -> Void)
    {
        let configuration = URLSessionConfiguration.default
        self.manager = Alamofire.SessionManager(configuration: configuration)
        manager.request(url, method: .post, parameters: withParameters as? Parameters, encoding: JSONEncoding.default).responseObject (keyPath: "") { (response: DataResponse<RestaurantRegistrationObjectMapper>) in
            print(response.response!)
            let responsedata = response.result.value
            print(responsedata!)
            if(responsedata?.restaurantID != nil)
            {
                self.restaurantID = (responsedata?.restaurantID)!
            }
            
            if(responsedata?.message != nil)
            {
                self.message = (responsedata?.message)!
            }
            
            if(responsedata?.success != nil)
            {
                self.success = (responsedata?.success)!
            }
            
            completion(true)
        }
    }
}

class RestaurantRegistrationObjectMapper: Mappable {
    var restaurantID: Int?
    var message: String?
    var success: String?
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    func mapping(map: Map)
    {
        restaurantID <- map["Res_Id"]
        message <- map["Message"]
        success <- map["Success"]
    }
}
