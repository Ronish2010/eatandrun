//
//  EatAndRunChooseItemToSplitViewController.swift
//  EatAndRun
//
//  Created by Sreeraj VR on 18/05/2017.
//  Copyright © 2017 Mawaqaa. All rights reserved.
//

import UIKit

protocol checkBoxDelegate
{
    func tapCheckBox(sender:SplititemCell)
}

class EatAndRunChooseItemToSplitViewController: UIViewController ,UITableViewDelegate,UITableViewDataSource{
    
    
    @IBOutlet weak var itemsSplitTable: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var identifier : String
        
        if (indexPath.row % 2 == 0)
        {
            identifier = "SplititemCell"
        }
        else
        {
            identifier = "SplititemCellOdd"
        }
        
        let cell = self.itemsSplitTable.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! SplititemCell
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.itemsSplitTable.frame.size.width * 0.173913043478261
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //self.restuarentMenuCategoryListTable.reloadData()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

class SplititemCell : UITableViewCell
{
   
    @IBAction func tapCheckBox(_ sender: Any) {
       
        
    }
}
