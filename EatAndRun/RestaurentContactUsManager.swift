//
//  RestaurentContactUsManager.swift
//  EatAndRun
//
//  Created by Ronish on 5/15/17.
//  Copyright © 2017 Mawaqaa. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
import AlamofireObjectMapper

class RestaurentContactUsManager: NSObject {
    var address: String?
    var phoneNumber: String?
    var email: String?
    var website: String?
    var instagram: String?
    var facebook: String?
    var googlePlus: String?
    var cus_Id: Int?
    var message: String?
    var success: String?
    
    var manager = Alamofire.SessionManager ()
    
    func fetchRestaurentContactUsFromServer(url: String, withParameters: NSDictionary, completion: @escaping ( Bool) -> Void)
    {
        let configuration = URLSessionConfiguration.default
        self.manager = Alamofire.SessionManager(configuration: configuration)
        manager.request(url, method: .post, parameters: withParameters as? Parameters, encoding: JSONEncoding.default).responseObject (keyPath: "") { (response: DataResponse<RestaurentContactUsMapper>) in
            
            let responsedata = response.result.value
            //print(responsedata?.phoneNumber!)
            
            if(responsedata?.address != nil)
            {
                self.address = (responsedata?.address)!
            }
            
            if(responsedata?.phoneNumber != nil)
            {
                self.phoneNumber = (responsedata?.phoneNumber)!
            }
            
            if(responsedata?.email != nil)
            {
                self.email = (responsedata?.email)!
            }
            
            if(responsedata?.website != nil)
            {
                self.website = (responsedata?.website)!
            }
            
            if(responsedata?.instagram != nil)
            {
                self.instagram = (responsedata?.instagram)!
            }
            
            if(responsedata?.facebook != nil)
            {
                self.facebook = (responsedata?.facebook)!
            }
            
            if(responsedata?.googlePlus != nil)
            {
                self.googlePlus = (responsedata?.googlePlus)!
            }
            
            if(responsedata?.cus_Id != nil)
            {
                self.cus_Id = (responsedata?.cus_Id)!
            }
            
            if(responsedata?.message != nil)
            {
                self.message = (responsedata?.message)!
            }
            
            if(responsedata?.success != nil)
            {
                self.success = (responsedata?.success)!
            }
            
            completion(true)
        }
    }
}

class RestaurentContactUsMapper: Mappable {
    
    var address: String?
    var phoneNumber: String?
    var email: String?
    var website: String?
    var instagram: String?
    var facebook: String?
    var googlePlus: String?
    var cus_Id: Int?
    var message: String?
    var success: String?
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    func mapping(map: Map)
    {
        address <- map["Address"]
        phoneNumber <- map["PhoneNumber"]
        email <- map["Email"]
        website <- map["Website"]
        instagram <- map["Instagram"]
        facebook <- map["Facebook"]
        googlePlus <- map["GooglePlus"]
        cus_Id <- map["Cus_Id"]
        message <- map["Message"]
        success <- map["Success"]
    }
}
