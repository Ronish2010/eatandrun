//
//  RestaurentOpenBillProceedManager.swift
//  EatAndRun
//
//  Created by Ronish on 5/11/17.
//  Copyright © 2017 Mawaqaa. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
import AlamofireObjectMapper

class RestaurentOpenBillProceedManager: NSObject {
    var openOrderNumber: String?
    var customerID: Int?
    var resturantID: Int?
    var resturantName: String?
    var message: String?
    var success: String?
    
    var manager = Alamofire.SessionManager ()
    
    func fetchRestaurentOpenBillProceedFromServer(url: String, withParameters: NSDictionary, completion: @escaping ( Bool) -> Void)
    {
        let configuration = URLSessionConfiguration.default
        self.manager = Alamofire.SessionManager(configuration: configuration)
        manager.request(url, method: .post, parameters: withParameters as? Parameters, encoding: JSONEncoding.default).responseObject (keyPath: "") { (response: DataResponse<RestaurentOpenBillProceedMapper>) in
            
            let responsedata = response.result.value
            // print(responsedata?.restaurentList!)
            
            if(responsedata?.openOrderNumber != nil)
            {
                self.openOrderNumber = (responsedata?.openOrderNumber)!
            }
            
            if(responsedata?.customerID != nil)
            {
                self.customerID = (responsedata?.customerID)!
            }
            
            if(responsedata?.resturantID != nil)
            {
                self.resturantID = (responsedata?.resturantID)!
            }
            
            if(responsedata?.resturantName != nil)
            {
                self.resturantName = (responsedata?.resturantName)!
            }
            
            if(responsedata?.message != nil)
            {
                self.message = (responsedata?.message)!
            }
            
            if(responsedata?.success != nil)
            {
                self.success = (responsedata?.success)!
            }
            
            completion(true)
        }
    }
}

class RestaurentOpenBillProceedMapper: Mappable {
    
    var openOrderNumber: String?
    var customerID: Int?
    var resturantID: Int?
    var resturantName: String?
    var message: String?
    var success: String?
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    func mapping(map: Map)
    {
        openOrderNumber <- map["OpenOrderNumber"]
        customerID <- map["Cus_Id"]
        resturantID <- map["Res_Id"]
        resturantName <- map["RestaurantName"]
        message <- map["Message"]
        success <- map["Success"]
    }
}
