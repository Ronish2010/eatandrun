//
//  EatAndRun_CustomDelegates.swift
//  EatAndRun
//
//  Created by Ronish on 4/10/17.
//  Copyright © 2017 Mawaqaa. All rights reserved.
//

import UIKit

class EatAndRun_CustomDelegates: NSObject {
    // MARK: - Variables
    var loadingView: UIView = UIView()
    var actView: UIView = UIView()
    var titleLabel: UILabel = UILabel()
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    func showActivity(myView: UIView, myTitle: String) {
        myView.isUserInteractionEnabled = false
        myView.window?.isUserInteractionEnabled = false
        myView.endEditing(true)
        actView.frame = CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: myView.frame.width, height: myView.frame.height))
        actView.center = myView.center
        actView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3)
        
        loadingView.frame = CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: 80, height: 80))
        loadingView.center = myView.center
        loadingView.backgroundColor = THEME_COLOUR
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 15
        
        activityIndicator.frame = CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: 40, height: 40))
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.whiteLarge
        activityIndicator.center = CGPoint(x: loadingView.frame.size.width / 2,y :loadingView.frame.size.height / 2)
        
        titleLabel.frame = CGRect(origin: CGPoint(x: 5,y :loadingView.frame.height-20), size: CGSize(width: loadingView.frame.width-10, height: 20))
        titleLabel.textColor = UIColor.white
        titleLabel.adjustsFontSizeToFitWidth = true
        titleLabel.textAlignment = NSTextAlignment.center
        titleLabel.text = myTitle
        //titleLabel.font = UIFont(name: FONT_NAME_R, size: 10)
        
        loadingView.addSubview(activityIndicator)
        actView.addSubview(loadingView)
        loadingView.addSubview(titleLabel)
        myView.addSubview(actView)
        activityIndicator.startAnimating()
    }
    func removeActivity(myView: UIView) {
        myView.isUserInteractionEnabled = true
        myView.window?.isUserInteractionEnabled = true
        activityIndicator.stopAnimating()
        actView.removeFromSuperview()
    }
    func showAlert(title: String, message: String, buttonTitle: String) {
        let AM_Alert = UIAlertController(title: title.localized(lang: LANGUAGE!), message: message, preferredStyle: UIAlertControllerStyle.alert)
        let okAction = UIAlertAction(title: buttonTitle.localized(lang: LANGUAGE!), style: UIAlertActionStyle.default) {
            (result : UIAlertAction) -> Void in
        }
        AM_Alert.addAction(okAction)
        AM_Alert.message = message
        if let window = (UIApplication.shared.delegate?.window)! as UIWindow! {
            window.rootViewController?.present(AM_Alert, animated: true, completion: { 
                
            })
        }
    }
    func TrimString(trimString: String)-> String {
        let whitespace: NSCharacterSet = NSCharacterSet.whitespacesAndNewlines as NSCharacterSet
        let trimmed = trimString.trimmingCharacters(in: whitespace as CharacterSet)
        return trimmed
    }
    func TrimNumbers(trimString: String)-> Bool {
        let whitespace: NSCharacterSet = NSCharacterSet.whitespacesAndNewlines as NSCharacterSet
        let disallowedCharacterSet = NSCharacterSet(charactersIn: "0123456789").inverted
        let trimmed = trimString.trimmingCharacters(in: whitespace as CharacterSet)
//        let replacementStringIsLegal = trimmed.rangeOfCharacterFromSet(disallowedCharacterSet) == nil
        let replacementStringIsLegal = trimmed.rangeOfCharacter(from: disallowedCharacterSet) == nil
        if replacementStringIsLegal == true {
            return true
        }else {
            return false
        }
    }
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    func checkNullToNil(value : AnyObject?) -> AnyObject? {
        if value is NSNull {
            return nil
        } else {
            return value
        }
    }
    func attributed(attrString: NSAttributedString, isBold: Bool, fontsize: CGFloat, fontColor: UIColor) -> NSMutableAttributedString {
        let mutableAttrString: NSMutableAttributedString = NSMutableAttributedString(attributedString: attrString)
        let headerStart: Int = 0
        let headerEnd: Int = attrString.length
        
        if isBold == true {
            mutableAttrString.addAttribute(NSFontAttributeName, value: UIFont.boldSystemFont(ofSize: fontsize), range: NSMakeRange(headerStart, headerEnd))
        }else {
            mutableAttrString.addAttribute(NSFontAttributeName, value: UIFont.systemFont(ofSize: fontsize), range: NSMakeRange(headerStart, headerEnd))
        }
        mutableAttrString.addAttribute(NSForegroundColorAttributeName, value: fontColor, range: NSMakeRange(headerStart, headerEnd))
        
        return mutableAttrString
    }
    /*
    func ShakeAnimation(animateView: UIView) {
        //        if VIBRATE == true {
        //            AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
        //        }
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.07
        animation.repeatCount = 2
        animation.autoreverses = true
        animation.fromValue = NSValue(cgPoint: CGPointMake(animateView.center.x - 10, animateView.center.y))
        animation.toValue = NSValue(cgPoint: CGPointMake(animateView.center.x + 10, animateView.center.y))
        animateView.layer.add(animation, forKey: "position")
    }
 */
}
