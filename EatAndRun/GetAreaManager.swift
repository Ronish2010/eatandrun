//
//  GetAreaManager.swift
//  EatAndRun
//
//  Created by Ronish on 4/28/17.
//  Copyright © 2017 Mawaqaa. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
import AlamofireObjectMapper

class GetAreaManager: NSObject {
    var areaArray: [AreaDetails]?
    var manager = Alamofire.SessionManager ()
    
    func getAreaDetailsFromServer(url: String, withParameters: NSDictionary, completion: @escaping ( Bool) -> Void)
    {
        let configuration = URLSessionConfiguration.default
        self.manager = Alamofire.SessionManager(configuration: configuration)
        manager.request(url, method: .post, parameters: withParameters as? Parameters, encoding: JSONEncoding.default).responseArray (keyPath: "areaList") { (response: DataResponse<[AreaDetails]>) in
            let responsedata = response.result.value
            if(responsedata != nil)
            {
                self.areaArray = responsedata
            }
            completion(true)
        }
    }
}

class AreaDetails: Mappable {
    var areaId: Int?
    var areaName: String?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        areaId <- map["AreaId"]
        areaName <- map["AreaName"]
    }
}
