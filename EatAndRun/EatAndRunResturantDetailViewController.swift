//
//  EatAndRunResturantDetailViewController.swift
//  EatAndRun
//
//  Created by Ronish on 3/17/17.
//  Copyright © 2017 Mawaqaa. All rights reserved.
//

import UIKit
import CoreLocation
import SDWebImage

class EatAndRunResturantDetailViewController: UIViewController, CLLocationManagerDelegate {
    
    @IBOutlet weak var searchBackView: UIView!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var searchTextView: UITextField!
    @IBOutlet weak var restaurantNameLbl: UILabel!
    @IBOutlet weak var restaurantAddressText: UITextView!
    @IBOutlet weak var distanceLbl: UILabel!
    @IBOutlet weak var currentStatusLbl: UILabel!
    
    var restaurantID = ""
    var userLattitude : Double = 0.0
    var userLongitude : Double = 0.0
    var locationManager = CLLocationManager()
    var statusCount = 1
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
    }
    
    override func viewDidLayoutSubviews() {
        self.searchBackView.layer.cornerRadius = self.searchBackView.frame.size.height/2
        self.searchBackView.clipsToBounds = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.locationManager.requestWhenInUseAuthorization()
        print(restaurantID)
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        // Do any additional setup after loading the view.
    }
    
    // MARK: - Web Services
    
    // MARK:  GET RESTAURENT DETAILS
    
    func getRestaurentDetails() {
        locationManager.stopUpdatingLocation()
        EatAndRun_DELEGATE.showActivity(myView: self.view, myTitle: "Loading..".localized(lang: LANGUAGE!))
        if !(REACHIBILITY.statusNetworkCheck())
        {
            EatAndRun_DELEGATE.removeActivity(myView: self.view)
            EatAndRun_DELEGATE.showAlert(title: "Sorry", message: "No network available", buttonTitle: "ok")
        }
        else
        {
            let params = ["Cus_Id": EatAndRun_DEFAULTS .value(forKey: "CUSTOMER_ID")!, "LanguageId": LANGUAGE!,"WebnaSecurityKey": "W3BN@53CUR6ITY8", "Res_Id": self.restaurantID, "Cus_Latitude": self.userLattitude, "Cus_Longitude": self.userLongitude]
            print(params)
            RESTAURENT_DETAILS_MANAGER.fetchRestaurentDetailsFromServer(url: BASE_URL + "Restaurant/GetRestaurantDetails", withParameters: params as NSDictionary, completion: { (result) -> Void in
                EatAndRun_DELEGATE.removeActivity(myView: self.view)
                self.restaurantNameLbl.text = RESTAURENT_DETAILS_MANAGER.resturantName
                self.restaurantAddressText.text = RESTAURENT_DETAILS_MANAGER.resturantAddress
                self.distanceLbl.text = RESTAURENT_DETAILS_MANAGER.distance
                self.currentStatusLbl.text = RESTAURENT_DETAILS_MANAGER.workingStatus
            })
        }
    }
    
    // MARK: - BUTTON ACTION
    
    @IBAction func menuButtonAction(_ sender: UIButton) {
        performSegue(withIdentifier: "MenuDetails", sender: self)
    }
    
    @IBAction func cuisineButtonAction(_ sender: UIButton) {
        performSegue(withIdentifier: "cuisineDetails", sender: self)
    }
    
    @IBAction func openBillButtonAction(_ sender: UIButton) {
        performSegue(withIdentifier: "OpenBill", sender: self)
    }
    
    @IBAction func joinBillButtonAction(_ sender: UIButton) {
        performSegue(withIdentifier: "JoinBill", sender: self)
    }
    
    // MARK: - LOCATION DELEGATE
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        self.userLattitude = Double(locValue.latitude)
        self.userLongitude = Double(locValue.longitude)
        if statusCount > 0 {
            statusCount = statusCount - 1
        } else if statusCount == 0{
            statusCount = -1
            getRestaurentDetails()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "MenuDetails"{
            let restaurentMenuViewController = segue.destination as! EatAndRunResturantMenuViewController
            restaurentMenuViewController.restaurantID = self.restaurantID
            restaurentMenuViewController.isMenu = true
        }else if segue.identifier == "cuisineDetails"{
            let restaurentMenuViewController = segue.destination as! EatAndRunResturantMenuViewController
            restaurentMenuViewController.restaurantID = self.restaurantID
            restaurentMenuViewController.isMenu = false
        }else if segue.identifier == "OpenBill"{
            let openBillInitialViewController = segue.destination as! EatAndRunOpenBillInitialViewController
            openBillInitialViewController.restaurantID = self.restaurantID
        }
    }
}
