//
//  RestaurentListManager.swift
//  EatAndRun
//
//  Created by Ronish on 5/2/17.
//  Copyright © 2017 Mawaqaa. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
import AlamofireObjectMapper

class RestaurentListManager: NSObject {
    var restaurentList: [RestaurentList]?
    var message: String?
    var success: String?
    var manager = Alamofire.SessionManager ()
    
    func fetchRestaurentListFromServer(url: String, withParameters: NSDictionary, completion: @escaping ( Bool) -> Void)
    {
        let configuration = URLSessionConfiguration.default
        self.manager = Alamofire.SessionManager(configuration: configuration)
        manager.request(url, method: .post, parameters: withParameters as? Parameters, encoding: JSONEncoding.default).responseObject (keyPath: "") { (response: DataResponse<RestaurentListMapper>) in
            
            let responsedata = response.result.value
           // print(responsedata?.restaurentList!)
            
            if(responsedata?.restaurentList != nil)
            {
                self.restaurentList = (responsedata?.restaurentList)!
                for list in self.restaurentList!
                {
                    if list.resturantId == nil{
                        list.resturantId = 0
                    }
                    if list.customerId == nil{
                        list.customerId = 0
                    }
                    if list.restaurentName == nil{
                        list.restaurentName = ""
                    }
                    if list.restaurentAddress == nil{
                        list.restaurentAddress = ""
                    }
                    if list.distance == nil{
                        list.distance = ""
                    }
                    if list.ratingStarCount == nil{
                        list.ratingStarCount = 0
                    }
                    if list.ratingViewCount == nil{
                        list.ratingViewCount = 0
                    }
                    if list.restaurentLogo == nil{
                        list.restaurentLogo = ""
                    }
                    if list.streetName == nil{
                        list.streetName = ""
                    }
                    if list.BiuldingName == nil{
                        list.BiuldingName = ""
                    }
                    if list.BiuldingNo == nil{
                        list.BiuldingNo = ""
                    }
                    if list.cityName == nil{
                        list.cityName = ""
                    }
                }
            }else {
                self.restaurentList = []
            }
            
            if(responsedata?.message != nil)
            {
                self.message = (responsedata?.message)!
            }
            
            if(responsedata?.success != nil)
            {
                self.success = (responsedata?.success)!
            }
            
            completion(true)
        }
    }
}


class RestaurentListMapper: Mappable {
    var restaurentList: [RestaurentList]?
    var message: String?
    var success: String?
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    func mapping(map: Map)
    {
        restaurentList <- map["apirestaurantmodelList"]
        message <- map["Message"]
        success <- map["Success"]
    }
}

class RestaurentList: Mappable {
    var resturantId: Int?
    var customerId: Int?
    var restaurentName: String?
    var restaurentAddress: String?
    var distance: String?
    var ratingStarCount: Float?
    var ratingViewCount: Int?
    var restaurentLogo: String?
    var streetName: String?
    var BiuldingName: String?
    var BiuldingNo: String?
    var cityName: String?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        resturantId <- map["Res_Id"]
        customerId <- map["Cus_Id"]
        restaurentName <- map["RestaurantName"]
        restaurentAddress <- map["Res_Address"]
        distance <- map["Distance"]
        ratingStarCount <- map["RatingStarCount"]
        ratingViewCount <- map["RatingViewCount"]
        restaurentLogo <- map["RestaurantLogo"]
        streetName <- map["StreetName"]
        BiuldingName <- map["BuildingName"]
        BiuldingNo <- map["BuildingNo"]
        cityName <- map["CityName"]
    }
}
