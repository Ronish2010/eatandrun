//
//  EatAndRunRootViewController.swift
//  EatAndRun
//
//  Created by Ronish on 3/13/17.
//  Copyright © 2017 Mawaqaa. All rights reserved.
//

import UIKit

class EatAndRunRootViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var menuBackView: UIView!
    @IBOutlet weak var menuTableView: UITableView!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var menuBackViewLeadingSpace: NSLayoutConstraint!
    @IBOutlet weak var languageSelectionPopUpBackView: UIView!
    @IBOutlet weak var languageSelectionPopUpView: UIView!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var englishButton: UIButton!
    @IBOutlet weak var arabicButton: UIButton!
    @IBOutlet weak var frenchButton: UIButton!
    
    let moreMenuTitleArray = ["Customer Account","Language","Join Bill","Current Order Status","Order History","Notification","Contact Us","Logout","Terms & Conditions"]
    
    let moreMenuImageArray = ["CustomerAccount","Language","JoinBill","CurrentOrderStatus","OrderHistory","Notification","ContactUs","Logout","TermsAndConditions"]
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
        self.languageSelectionPopUpBackView.isHidden = true
    }
    
    override func viewDidLayoutSubviews() {
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.menuButton.isSelected = false
        self.menuTableView.delegate = self
        self.menuTableView.dataSource = self
        self.menuTableView.reloadData()
        
        self.languageSelectionPopUpView.layer.borderWidth = 4
        self.languageSelectionPopUpView.layer.borderColor = THEME_ORANGE_COLOUR.cgColor
        self.languageSelectionPopUpView.layer.cornerRadius = 10
        self.closeButton.layer.cornerRadius = self.closeButton.frame.size.height/2
        self.closeButton.layer.borderColor = UIColor.white.cgColor
        self.closeButton.layer.borderWidth = 2
        self.englishButton.backgroundColor = THEME_ORANGE_COLOUR
        self.englishButton.layer.cornerRadius = 4
        self.englishButton.layer.borderColor =  UIColor.white.cgColor
        self.englishButton.layer.borderWidth = 1
        self.arabicButton.backgroundColor = THEME_BLUE_COLOUR
        self.arabicButton.layer.cornerRadius = 4
        self.arabicButton.layer.borderColor =  UIColor.white.cgColor
        self.arabicButton.layer.borderWidth = 1
        self.frenchButton.backgroundColor = THEME_BLUE_COLOUR
        self.frenchButton.layer.cornerRadius = 4
        self.frenchButton.layer.borderColor =  UIColor.white.cgColor
        self.frenchButton.layer.borderWidth = 1
        // Do any additional setup after loading the view.
    }
    
    @IBAction func menuButtonAction(_ sender: UIButton) {
        if menuButton.isSelected{
            UIView.animate(withDuration: 0.5, animations: {
                self.menuBackViewLeadingSpace.constant = 0
                self.view.layoutIfNeeded()
            }, completion: { Void in
                self.menuButton.isSelected = false
            })
        } else {
            UIView.animate(withDuration: 0.6, animations: {
                self.menuBackViewLeadingSpace.constant = -self.menuBackView.frame.size.width
                self.view.layoutIfNeeded()
            }, completion: { Void in
                self.menuButton.isSelected = true
            })
        }
    }
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        print(NAVIGATION_POINTER)
        _ = NAVIGATION_POINTER.popViewController(animated: true)
    }
    
    // MARK: - PopUp Close Button Action
    
    @IBAction func popUpCloseButtonAction(_ sender: UIButton) {
        self.languageSelectionPopUpBackView.isHidden = true
    }
    
    // MARK: - TABLEVIEW DELEGATES
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.moreMenuTitleArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.menuTableView.dequeueReusableCell(withIdentifier: "Menu_Cell", for: indexPath) as! MenuCell
        cell.ItemImage.image = UIImage(named: self.moreMenuImageArray[indexPath.row])
        cell.itemTitle.text = self.moreMenuTitleArray[indexPath.row] 
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //performSegue(withIdentifier: "Notification", sender: self)
        self.menuButton.sendActions(for: .touchUpInside)
        if indexPath.row == 0{
            NAVIGATION_POINTER.performSegue(withIdentifier: "MyAccount", sender: self)
        }else if indexPath.row == 1{
            self.languageSelectionPopUpBackView.isHidden = false
        }else if indexPath.row == 2{
            NAVIGATION_POINTER.performSegue(withIdentifier: "JoinBill", sender: self)
        }else if indexPath.row == 3{
            NAVIGATION_POINTER.performSegue(withIdentifier: "CurrentOrderStatus", sender: self)
        }else if indexPath.row == 4{
            NAVIGATION_POINTER.performSegue(withIdentifier: "OrderHistory", sender: self)
        }else if indexPath.row == 5{
            NAVIGATION_POINTER.performSegue(withIdentifier: "Notification", sender: self)
        }else if indexPath.row == 6{
            NAVIGATION_POINTER.performSegue(withIdentifier: "ContactUs", sender: self)
        }else if indexPath.row == 7{
            let alert = UIAlertController(title: "", message: "Do you want to logout?".localized(lang: LANGUAGE!), preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Continue".localized(lang: LANGUAGE!), style: UIAlertActionStyle.default, handler: { action in
                EatAndRun_DEFAULTS.setValue("", forKey: "CUSTOMER_ID")
                EatAndRun_DEFAULTS.set(false, forKey: "KEEP_ME_SIGNED_IN")
                let storage = HTTPCookieStorage.shared
                if let cookies = storage.cookies
                {
                    for cookie in cookies
                    {
                        storage.deleteCookie(cookie)
                    }
                }
                //let mainView: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let viewcontroller : EatAndRunLoginViewController = self.storyboard!.instantiateViewController(withIdentifier: "EatAndRunLoginViewController") as! EatAndRunLoginViewController
                self.present(viewcontroller, animated: true, completion: nil)
            }))
            alert.addAction(UIAlertAction(title: "Cancel".localized(lang: LANGUAGE!), style: UIAlertActionStyle.cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }else if indexPath.row == 8{
            NAVIGATION_POINTER.performSegue(withIdentifier: "TermsAndCondition", sender: self)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

class MenuCell: UITableViewCell {
    @IBOutlet weak var ItemImage: UIImageView!
    @IBOutlet weak var itemTitle: UILabel!
}
